package com.softserveinc.ita.kaiji.web.controller.async;

import org.junit.Test;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CountDownLatch;

import static org.junit.Assert.assertEquals;

/**
 * Created by Andrey on 06.03.2015.
 */
public class GameSyncroTest {
    @Test
    public void getGameWaiterTest(){
        ConcurrentMap<Integer, CountDownLatch> gameWaiter = new ConcurrentHashMap<>();
        GameSyncro gs = new GameSyncro();
        gs.setGameWaiter(gameWaiter);
        ConcurrentMap<Integer, CountDownLatch> gw = gs.getGameWaiter();
        assertEquals(gameWaiter, gw);
    }
}
