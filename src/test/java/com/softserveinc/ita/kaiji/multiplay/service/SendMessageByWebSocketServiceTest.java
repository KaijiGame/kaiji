package com.softserveinc.ita.kaiji.multiplay.service;

import com.softserveinc.ita.kaiji.multiplay.domain.config.ShipConfigForGame;
import com.softserveinc.ita.kaiji.multiplay.domain.player.PlayerOnlineInfo;
import com.softserveinc.ita.kaiji.multiplay.domain.room.Ship;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;


@RunWith(BlockJUnit4ClassRunner.class)
public class SendMessageByWebSocketServiceTest {
    @Mock
    private SimpMessagingTemplate template;
    @InjectMocks
    private SendMessageByWebSocketService service;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSendMessageToPlayerOnShip() {
        service.sendMessageToPlayerOnShip(new PlayerOnlineInfo("", "", new Ship(new ShipConfigForGame())), new Object());
        verify(template).convertAndSendToUser(anyString(), anyString(), anyObject());
    }
}
