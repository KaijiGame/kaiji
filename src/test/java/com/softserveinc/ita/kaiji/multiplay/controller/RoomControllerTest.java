package com.softserveinc.ita.kaiji.multiplay.controller;

import com.softserveinc.ita.kaiji.model.User;
import com.softserveinc.ita.kaiji.multiplay.dao.RoomDAO;
import com.softserveinc.ita.kaiji.multiplay.domain.config.ShipConfigForGame;
import com.softserveinc.ita.kaiji.multiplay.domain.room.Ship;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(BlockJUnit4ClassRunner.class)
public class RoomControllerTest {

    @Mock
    private RoomDAO roomDAO;
    @InjectMocks
    private RoomController roomController;

    private MockMvc mockMvc;

    @Before
    public void prepareData() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(roomController).build();
    }

    @Test
    public void testGetRoomPage() throws Exception {
        Authentication auth = new UsernamePasswordAuthenticationToken(new User(),null);
        SecurityContextHolder.getContext().setAuthentication(auth);
        when(roomDAO.findOne(anyString())).thenReturn(new Ship(new ShipConfigForGame()));
        mockMvc.perform(get("/game/room/shipName").principal(auth))
                .andExpect(status().isOk())
                .andExpect(view().name("room"))
                .andExpect(model().attributeExists("messages"))
                .andExpect(model().attributeExists("shipName"))
                .andExpect(model().attributeExists("numberOfTables"));
    }
}