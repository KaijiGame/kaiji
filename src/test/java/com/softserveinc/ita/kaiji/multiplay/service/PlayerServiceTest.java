package com.softserveinc.ita.kaiji.multiplay.service;

import com.softserveinc.ita.kaiji.multiplay.dao.PlayerOnlineInfoDAO;
import com.softserveinc.ita.kaiji.multiplay.dao.RoomDAO;
import com.softserveinc.ita.kaiji.multiplay.domain.config.ShipConfigForGame;
import com.softserveinc.ita.kaiji.multiplay.domain.player.PlayerOnlineInfo;
import com.softserveinc.ita.kaiji.multiplay.domain.room.Ship;
import com.softserveinc.ita.kaiji.multiplay.dto.client.DtoClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import sun.security.acl.PrincipalImpl;

import java.security.Principal;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(BlockJUnit4ClassRunner.class)
public class PlayerServiceTest {
    @Mock
    private PlayerOnlineInfoDAO playerOnlineInfoDAO;
    @Mock
    private RoomDAO roomDAO;
    @Mock
    private ApplicationContext appContext;
    @Mock
    private PlayerOnlineInfo playerOnlineInfo;
    @Mock
    private SendMessageByWebSocketService sendMessageByWebSocketService;
    @Mock
    private Ship ship;
    @InjectMocks
    private PlayerService playerService;

    @Before
    public void prepareData() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddPlayer() {
        final Principal principal = new PrincipalImpl("name");
        final String simpSessionId = "0";
        final String shipName = "shipName";
        final Ship ship = new Ship(new ShipConfigForGame());
        final PlayerOnlineInfo player = new PlayerOnlineInfo(simpSessionId, shipName, ship);
        player.setSendMessageByWebSocketService(sendMessageByWebSocketService);
        when(roomDAO.findOne(shipName)).thenReturn(ship);
        when(appContext.getBean(PlayerOnlineInfo.class, simpSessionId,
                principal.getName(), ship)).thenReturn(player);
        playerService.addPlayer(principal, simpSessionId, shipName);
        verify(playerOnlineInfoDAO).save(player);

    }

    @Test
    public void testRemovePlayer() {
        final String simpSessionId = "0";
        final String shipName = "shipName";
        final Ship ship = new Ship(new ShipConfigForGame());
        final PlayerOnlineInfo player = new PlayerOnlineInfo(simpSessionId, shipName, ship);
        player.setSendMessageByWebSocketService(sendMessageByWebSocketService);
        when(playerOnlineInfoDAO.findOne(simpSessionId)).thenReturn(player);
        playerService.remove(simpSessionId);
        verify(playerOnlineInfoDAO).delete(simpSessionId);
    }

    @Test
    public void testIncomeMessage() {
        final Principal principal = new PrincipalImpl("name");
        final String simpSessionId = "0";
        DtoClient dtoClient = new DtoClient();
        when(ship.getName()).thenReturn("name");
        when(playerOnlineInfo.getIdSession()).thenReturn(simpSessionId);
        when(playerOnlineInfoDAO.findOne(simpSessionId)).thenReturn(playerOnlineInfo);
        when(playerOnlineInfo.getPlayRoom()).thenReturn(ship);
        playerService.inComeMessage(principal, simpSessionId, dtoClient);
        verify(ship).inComeMessage(playerOnlineInfo, dtoClient);
    }

}
