package com.softserveinc.ita.kaiji.multiplay.websocket.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;


@RunWith(BlockJUnit4ClassRunner.class)
public class DisconnectListenerTest {
    @Mock
    private SocketGameController socketGameController;
    @Mock
    private SessionDisconnectEvent sessionDisconnectEvent;
    @InjectMocks
    private DisconnectListener listener;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testOnApplicationEvent() {
        listener.onApplicationEvent(sessionDisconnectEvent);
        verify(socketGameController).disconnectHandler(anyString());
    }
}
