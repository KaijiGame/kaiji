package com.softserveinc.ita.kaiji.multiplay.websocket.controller;

import com.softserveinc.ita.kaiji.multiplay.dto.client.DtoClient;
import com.softserveinc.ita.kaiji.multiplay.service.PlayerService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import sun.security.acl.PrincipalImpl;

import java.security.Principal;

import static org.mockito.Mockito.verify;

@RunWith(BlockJUnit4ClassRunner.class)
public class SocketGameControllerTest {
    @Mock
    private PlayerService playerService;
    @InjectMocks
    private SocketGameController socketGameController;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testInComeMessageHandler() {
        final Principal principal = new PrincipalImpl("name");
        final String simpSessionId = "0";
        final DtoClient dtoClient = new DtoClient();
        socketGameController.inComeMessageHandler(principal, simpSessionId, dtoClient);
        verify(playerService).inComeMessage(principal, simpSessionId, dtoClient);
    }

    @Test
    public void testConnectHandler() {
        final Principal principal = new PrincipalImpl("name");
        final String simpSessionId = "0";
        final String shipName = "name";
        socketGameController.connectHandler(principal, simpSessionId, shipName);
        verify(playerService).addPlayer(principal, simpSessionId, shipName);
    }

    @Test
    public void testDisconnectHandler() {
        final String simpSessionId = "0";
        socketGameController.disconnectHandler(simpSessionId);
        verify(playerService).remove(simpSessionId);
    }
}
