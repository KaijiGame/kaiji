package com.softserveinc.ita.kaiji.database;

import com.softserveinc.ita.kaiji.DBConfiguration;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import static org.junit.Assert.assertNotNull;

@Ignore
public class DataBaseConnectionTest {

	@Test
	public void connectionSetUpTest() throws IOException {

		Properties connectionProperties = new DBConfiguration().getConnectionProperties();

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(
					connectionProperties.getProperty("jdbc.url")+"/"+connectionProperties.getProperty("jdbc.dbname"),
					connectionProperties.getProperty("jdbc.username"),
					connectionProperties.getProperty("jdbc.password"));

		} catch (SQLException e) {
		}
		assertNotNull(conn);
	}

}
