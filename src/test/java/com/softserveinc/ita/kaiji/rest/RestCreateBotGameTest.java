package com.softserveinc.ita.kaiji.rest;

import com.softserveinc.ita.kaiji.TestConfiguration;
import com.softserveinc.ita.kaiji.dao.UserDAO;
import com.softserveinc.ita.kaiji.dto.SystemConfiguration;
import com.softserveinc.ita.kaiji.model.User;
import com.softserveinc.ita.kaiji.service.GameService;
import com.softserveinc.ita.kaiji.service.SystemConfigurationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
@WebAppConfiguration
public class RestCreateBotGameTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Mock
    private GameService gameService;

    @Mock
    private UserDAO userDAO;

    @Mock
    private SystemConfigurationService systemConfigurationService;
    @InjectMocks
    private RestCreateGameWithBotController controller;

	private MockMvc mockMvc;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

	@Test
	public void createBotGame() throws Exception {
        SystemConfiguration systemConfiguration = new SystemConfiguration();
        systemConfiguration.setNumberOfCards(3);
        when(userDAO.findByNickname(anyString())).thenReturn(new User());
        when(systemConfigurationService.getSystemConfiguration()).thenReturn(systemConfiguration);
		mockMvc.perform(post("/rest/botgame/create")
				.param("name", "petya")
				.param("gamename", "Game")
				.param("cards", "2")
				.param("bot", "EASY"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.gameName", is("Game")))
				.andExpect(jsonPath("$.playerName", is("petya")))
				.andExpect(jsonPath("$.botGame", is(true)))
				.andExpect(jsonPath("$.numberOfCards", is(2)))
				.andExpect(jsonPath("$.numberOfStars", is(0)))
				.andExpect(jsonPath("$.botType", is("EASY")))
				.andExpect(jsonPath("$.gameType", is("BOT_GAME")))
				.andExpect(jsonPath("$.playerId", is(0)));
	}

	@Test
	public void createBotGameWithWrongParameters() throws Exception {

		mockMvc.perform(post("/rest/botgame/create")
				.param("name", "yuti")
				.param("gamename", "Game")
				.param("cards", "2")
				.param("bot", "EASY_GAME"))
				.andExpect(status().isBadRequest());
	}

}
