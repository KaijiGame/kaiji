package com.softserveinc.ita.kaiji.rest;

import com.softserveinc.ita.kaiji.TestServiceConfiguration;
import com.softserveinc.ita.kaiji.dao.UserDAO;
import com.softserveinc.ita.kaiji.rest.dto.ConvertToRestDto;
import com.softserveinc.ita.kaiji.service.GameService;
import com.softserveinc.ita.kaiji.service.SystemConfigurationService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestServiceConfiguration.class)
@WebAppConfiguration
@Ignore
public class RestCreatePlayerGameTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Mock
    private GameService gameService;

    @Mock
    private UserDAO userDAO;

    @Mock
    private SystemConfigurationService systemConfigurationService;
    @Mock
    private ConvertToRestDto convertToRestDto;
    @InjectMocks
    private RestCreateGameWithPlayerController controller;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

	@Test
	public void createPlayerGameWithWrongParameters() throws Exception {

		mockMvc.perform(post("/rest/playergame/create")
				.param("name", "someName")
				.param("gamename", "Game")
				.param("cards", "2")
				.param("stars", "STARS"))
				.andExpect(status().isBadRequest());
	}

}
