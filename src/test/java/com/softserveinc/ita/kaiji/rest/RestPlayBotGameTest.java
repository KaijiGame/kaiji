package com.softserveinc.ita.kaiji.rest;

import com.softserveinc.ita.kaiji.TestConfiguration;
import com.softserveinc.ita.kaiji.model.Card;
import com.softserveinc.ita.kaiji.model.User;
import com.softserveinc.ita.kaiji.model.game.*;
import com.softserveinc.ita.kaiji.model.player.HumanPlayer;
import com.softserveinc.ita.kaiji.model.player.Player;
import com.softserveinc.ita.kaiji.rest.dto.ConvertToRestDto;
import com.softserveinc.ita.kaiji.rest.dto.CurrentGameRestInfoDto;
import com.softserveinc.ita.kaiji.service.GameService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
@WebAppConfiguration
public class RestPlayBotGameTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Mock
    private ConvertToRestDto convertToRestDto;

    @Mock
    private GameService gameService;

    @Mock
    private RestUtils restUtils;

    @InjectMocks
    private RestPlayGameWithBotController controller;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

	@Test
	public void makeTurnBotGame() throws Exception {
        Player player = new HumanPlayer(new User(), 1 , 1);
        player.setId(1);
        final GameInfo gameInfo = new GameInfoImpl();
        gameInfo.getPlayers().add(player);
        GameHistory gameHistory = getGameHistory(gameInfo);
        when(convertToRestDto.currentGameInfoToDto(0, 0, Card.PAPER, gameHistory)).thenReturn(getGameInfoDto());
        when(gameService.getGameInfo(anyInt())).thenReturn(gameInfo);
        when(gameService.getGameHistory(anyInt())).thenReturn(gameHistory);
		mockMvc.perform(get("/rest/botgame/play/" + gameService.getGameId("MyGame") + "/PAPER"))
				.andExpect(status().isOk())
    			.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.playerName", is("petya")))
				.andExpect(jsonPath("$.gameName", is("MyGame")))
				.andExpect(jsonPath("$.gameState", is("GAME_PLAYING")))
				.andExpect(jsonPath("$.cardPaperLeft", is(1)));
	}

	@Test
	public void makeTurnBotGameWithWrongParameters() throws Exception {

		mockMvc.perform(get("/rest/botgame/play/" + gameService.getGameId("MyGameTest") + "/PAPIR"))
				.andExpect(status().isBadRequest());
	}

    private CurrentGameRestInfoDto getGameInfoDto() {
        CurrentGameRestInfoDto currentGameRestInfoDto = new CurrentGameRestInfoDto();
        currentGameRestInfoDto.setPlayerName("petya");
        currentGameRestInfoDto.setGameName("MyGame");
        currentGameRestInfoDto.setGameState(Game.State.GAME_PLAYING);
        currentGameRestInfoDto.setCardPaperLeft(1);
        return currentGameRestInfoDto;
    }

    private GameHistory getGameHistory(final GameInfo gameInfo) {
        GameHistory gameHistory = new GameHistory() {
            @Override
            public GameInfo getGameInfo() { return gameInfo; }
            @Override
            public List<RoundResult> getRoundResults() { return null; }
            @Override
            public RoundResult getLastRoundResultFor(Player player) {  return null; }
            @Override
            public Set<Player> getWinners() {return null;}
            @Override
            public Game.State getGameStatus() {  return Game.State.GAME_FINISHED;}
            @Override
            public Integer getId() { return null; }
            @Override
            public void setId(Integer id) {}
        };
        return gameHistory;
    }
}
