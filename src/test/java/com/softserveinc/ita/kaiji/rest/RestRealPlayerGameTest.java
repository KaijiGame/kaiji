package com.softserveinc.ita.kaiji.rest;

import com.softserveinc.ita.kaiji.TestServiceConfiguration;
import com.softserveinc.ita.kaiji.rest.dto.ConvertToRestDto;
import com.softserveinc.ita.kaiji.service.GameService;
import com.softserveinc.ita.kaiji.service.SystemConfigurationService;
import com.softserveinc.ita.kaiji.service.UserService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestServiceConfiguration.class)
@WebAppConfiguration
@Ignore
public class RestRealPlayerGameTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Mock
    private ConvertToRestDto convertToRestDto;

    @Mock
    private GameService gameService;

    @Mock
    private UserService userService;

    @Mock
    private SystemConfigurationService systemConfigurationService;

    @Mock
    private RestUtils restUtils;
    @InjectMocks
    private RestPlayGameWithPlayerController controller;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }
	@Test
	public void playerMakeTurnWithWrongParameters() throws Exception {

		mockMvc.perform(get("/rest/playergame/play")
				.param("playerId", "0")
				.param("gameId", "0")
				.param("chosenCard", "CARD"))
				.andExpect(status().isBadRequest());
	}
}
