package com.softserveinc.ita.kaiji.dao;

import com.softserveinc.ita.kaiji.dto.game.StatisticsDTO;
import com.softserveinc.ita.kaiji.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Alexandra on 05.03.2015.
 */
@RunWith(Parameterized.class)
public class UserDAOTest extends UserDAOImpl {
    private Object [] queryResult;
    public UserDAOTest(Object[] list){

        queryResult = /*new Object[]{  new BigInteger("18"),new BigInteger("9"),new BigInteger("9"),new BigInteger("1"),new BigInteger("3")};//*/list;
    }
    @Parameterized.Parameters
    public static Collection<Object[]> getParameters(){
        Object [][] params = {
                    {  setParameters("19","9","10","1","3")   },
                    {   setParameters("5","6","3","0","2")   },
                    {   setParameters("20","10","8","2","3") },
                    {   setParameters("8","9","2","0","1")   },
                    {   setParameters("4","2","1","0","1")   },
                    {   setParameters("30","20","10","2","4")}    };
        return Arrays.asList(params);
    }

    private static Object[]setParameters(String ... a){
       return new Object[]{ new BigInteger(a[0]),new BigInteger(a[1]),new BigInteger(a[2]),new BigInteger(a[3]),new BigInteger(a[4])};
    }
    @Override
    protected List<Object[]> executeStatisticsQuery(User user) {
        List<Object[]> imitateResultList = new ArrayList<>();
        imitateResultList.add(queryResult);
        return imitateResultList;
    }

    @Test
    public void createStatisticsDTOParamTest(){
        User testUser = new User("test","test@mail.ru","test");
        testUser.setId(999);
        StatisticsDTO statisticsDTO = super.findStatistics(testUser);
        assertEquals(((BigInteger)queryResult[0]).longValue(), statisticsDTO.getWin().longValue());
        assertEquals(((BigInteger)queryResult[1]).longValue(), statisticsDTO.getLose().longValue());
        assertEquals(((BigInteger)queryResult[2]).longValue(), statisticsDTO.getDraw().longValue());
        assertEquals(((BigInteger)queryResult[3]).longValue(), statisticsDTO.getGameWins().longValue());
        assertEquals(((BigInteger)queryResult[4]).longValue(), statisticsDTO.getTotalGames().longValue());
    }
}
