package com.softserveinc.ita.kaiji.ajax;

import com.softserveinc.ita.kaiji.model.game.GameInfo;
import com.softserveinc.ita.kaiji.model.game.GameInfoImpl;
import com.softserveinc.ita.kaiji.model.player.Player;
import com.softserveinc.ita.kaiji.model.player.bot.Bot;
import com.softserveinc.ita.kaiji.service.GameService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(BlockJUnit4ClassRunner.class)
public class SearchCreatedGamesTest {

    @Mock
    private GameService gameService;

    @InjectMocks
    private SearchCreatedGames searchCreatedGames;

    private MockMvc mockMvc;

    @Before
    public void prepareData() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(searchCreatedGames).build();
    }

    @Test
    public void testAllCreatedGames() throws Exception {
        GameInfoImpl gameInfo = new GameInfoImpl("gameName", "gameOwner", 0, 0,
                Bot.Types.EASY, new HashSet<Player>(), 0);
        Set<GameInfo> gameInfos = new HashSet<>();
        gameInfos.add(gameInfo);
        when(gameService.getAllGameInfos()).thenReturn(gameInfos);
        mockMvc.perform(get("/game/createdgames").param("term", "gameName"))
                .andExpect(status().isOk())
                .andExpect(content().string("[\"gameName\"]"));
    }
}