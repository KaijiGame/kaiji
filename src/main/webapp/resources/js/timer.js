/**
 * Created by mikeldpl on 02.04.2015.
 */

function timerSetTime(mlsec) {
    var countdown = $('#countdown');
    countdown.countdown({until: new Date(mlsec + new Date().getTime()), format: 'HMS'});
    countdown.countdown('pause');
}

function timerStart() {
    $('#countdown').countdown('resume');
}

function timerStop() {
    $('#countdown').countdown('pause');
}