/**
 * Created by Anton Tulskih on 17.09.2015.
 */

function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var img = document.getElementById('avatarImg');

        reader.onload = function (e) {
            img.setAttribute('src', e.target.result);

            // Create an empty canvas element
            var canvas = document.createElement("canvas");
            canvas.width = img.width;
            canvas.height = img.height;

            // Copy the image contents to the canvas
            var ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0, img.width, img.height);

            var base64 = canvas.toDataURL("image/jpeg", 1.0);
            var newImg = new Image();
            newImg.src = base64;
            img.src = newImg.src;
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function uploadImg() {
    document.getElementById('imgInp').click();
}