/**
 * Created by Bukatin Vlad on 29.08.2015.
 */

//ForCopy
var messageForCopy;
var userForCopy;
var tabForCopy;
var newMessageFromForCopy;
var newChatTabForCopy;
var chatForm;

//Global
var infoAboutNewMessages;
var infoAboutTabsForChat;
var chatHolder;
var messageText;

var currentTab;
var currentNickName;
var currentChat;
var allTabs = [];
var usersForNewChat = [];


var UUIDGenerator = createUUID();

//FOR WEBSOCKETS

var port = ":8000";
var serviceLocationForChat = "http://" + document.location.host + "/chat";
var subscribePathForChat = "/user/queue/chat/sub";
var answerPathForChat = "/queue/chat/mess";

var socketChat = new SockJS(serviceLocationForChat);
var stompClientChat = Stomp.over(socketChat);
stompClientChat.connect({}, function (frame) {
    console.log('Connected: ' + frame);
    stompClientChat.subscribe(subscribePathForChat, function (msg) {//on income msg
        parseJsonForChat(msg.body);//code
    });
    socketChat.onclose = function () {
    };
});

function sendAnswerForChat(msg) {
    stompClientChat.send(answerPathForChat, {}, msg);
}

//Start

window.onunload = closeIt;
window.onload = chatOnLoad;

function chatOnLoad() {
    infoAboutNewMessages = document.getElementById("infoAboutNewMessages");
    infoAboutTabsForChat = document.getElementById("infoAboutTabsForChat");
    chatHolder = document.getElementById("chatHolder");
    messageText = document.getElementById("message");
    chatForm = document.getElementById("objectChatForm");

    messageForCopy = document.getElementById("messageForCopy");
    userForCopy = document.getElementById("userForCopy");
    tabForCopy = document.getElementById("response_forCopy");
    newMessageFromForCopy = document.getElementById("newMessageFromForCopy");
    newChatTabForCopy = document.getElementById("newTabForChat");

}

function addElementsForAll() {
    if (!arrayInclude(allTabs, currentTab)) {
        allTabs.push(currentTab);
    }
}

function removeElementsFromAll(tabId) {

    for (var i = 0; i < allTabs.length; i++) {
        if (allTabs[i].getTabId() == tabId) {
            allTabs.remove(allTabs[i]);
            break;
        }
    }
}

function setCurrentElementsForChat() {

    for (var i = 0; i < allTabs.length; i++) {
        if (equalsChatIds(allTabs[i].getChat().getId(),currentChat.getId())) {
            currentTab = allTabs[i];
            break;
        }
    }
    if (currentTab === undefined
        || !(equalsChatIds(currentTab.getChat().getId(),currentChat.getId()))) {
        currentTab = createNewTab();
    }
    currentTab.setChatWindow(currentTab.getTab().getElementsByClassName('MyChat')[0]);
    currentTab.setActiveUsers(currentTab.getTab().getElementsByClassName('activeUsers')[0]);
    addElementsForAll();
    activateCurrentTab();
}

function closeIt() {
    socketChat.close();
}

function createUUID() {
    var self = {};
    var lut = [];
    for (var i = 0; i < 256; i++) {
        lut[i] = (i < 16 ? '0' : '') + (i).toString(16);
    }
    self.generate = function () {
        var d0 = Math.random() * 0xffffffff | 0;
        var d1 = Math.random() * 0xffffffff | 0;
        var d2 = Math.random() * 0xffffffff | 0;
        var d3 = Math.random() * 0xffffffff | 0;
        return lut[d0 & 0xff] + lut[d0 >> 8 & 0xff] + lut[d0 >> 16 & 0xff] + lut[d0 >> 24 & 0xff] + '-' +
            lut[d1 & 0xff] + lut[d1 >> 8 & 0xff] + '-' + lut[d1 >> 16 & 0x0f | 0x40] + lut[d1 >> 24 & 0xff] + '-' +
            lut[d2 & 0x3f | 0x80] + lut[d2 >> 8 & 0xff] + '-' + lut[d2 >> 16 & 0xff] + lut[d2 >> 24 & 0xff] +
            lut[d3 & 0xff] + lut[d3 >> 8 & 0xff] + lut[d3 >> 16 & 0xff] + lut[d3 >> 24 & 0xff];
    }
    return self;
}

function send_message() {
    if (messageText.value != "") {
        var messageForSend = createSendingMessage();
        var jsonStr = JSON.stringify(messageForSend);
        sendAnswerForChat(jsonStr);
        messageText.value = "";
        messageText.focus();
    }
}

function createSendingMessage() {
    var sendingMessage = new createMessageObject();
    sendingMessage.setChatId(currentChat.getId());
    sendingMessage.setDateMessage(new Date());
    sendingMessage.setUserFrom(currentNickName);
    sendingMessage.setTextMessage(messageText.value);
    sendingMessage.generateUuidFromBrowser();
    var usersTo = getUsersFromCurrentChatWithoutCurrent();
    for (var i = 0; i < usersTo.length; i++) {
        var userTo = new createUserToObject();
        userTo.setUserTo(usersTo[i]);
        userTo.setItDeleted(false);
        userTo.setItNewMessage(true);
        sendingMessage.addUserTo(userTo);
    }
    return sendingMessage;
}

// JS OBJECTS

function createTabForChat() {
    this.tab;
    this.activeUsers;
    this.chatWindow;
    this.tabId;
    this.chat;
    this.buttonForChange;

    this.setTab = function (tab) {
        this.tab = tab;
    }
    this.getTab = function () {
        return this.tab;
    }

    this.setActiveUsers = function (activeUsers) {
        this.activeUsers = activeUsers;
    }
    this.getActiveUsers = function () {
        return this.activeUsers;
    }

    this.setChatWindow = function (chatWindow) {
        this.chatWindow = chatWindow;
    }
    this.getChatWindow = function () {
        return this.chatWindow;
    }

    this.setTabId = function (tabId) {
        this.tabId = tabId;
    }
    this.getTabId = function () {
        return this.tabId;
    }

    this.setChat = function (chat) {
        this.chat = chat;
    }
    this.getChat = function () {
        return this.chat;
    }

    this.setButtonForChange = function (buttonForChange) {
        this.buttonForChange = buttonForChange;
    }
    this.getButtonForChange = function () {
        return this.buttonForChange;
    }

}

function createUserToObject() {
    this.userTo;
    this.itNewMessage;
    this.itDeleted;

    this.setUserTo = function (userTo) {
        this.userTo = userTo;
    }
    this.getUserTo = function () {
        return this.userTo;
    }

    this.setItNewMessage = function (itNewMessage) {
        this.itNewMessage = itNewMessage;
    }
    this.getItNewMessage = function () {
        return this.itNewMessage;
    }

    this.setItDeleted = function (itDeleted) {
        this.itDeleted = itDeleted;
    }
    this.getItDeleted = function () {
        return this.itDeleted;
    }
}

function createMessageObject() {
    this.type = 'message';
    this.id;
    this.chatId;
    this.userFrom;
    this.textMessage;
    this.dateMessage;
    this.usersTo = [];
    this.uuidFromBrowser;
    this.chatName;

    this.setId = function (id) {
        this.id = id;
    }
    this.getId = function () {
        return this.id;
    }

    this.setChatId = function (chatId) {
        this.chatId = chatId;
    }
    this.getChatId = function () {
        return this.chatId;
    }

    this.setUserFrom = function (userFrom) {
        this.userFrom = userFrom;
    }
    this.getUserFrom = function () {
        return this.userFrom;
    }

    this.setTextMessage = function (textMessage) {
        this.textMessage = textMessage;
    }
    this.getTextMessage = function () {
        return this.textMessage;
    }

    this.setDateMessage = function (dateMessage) {
        this.dateMessage = dateMessage;
    }
    this.getDateMessage = function () {
        return this.dateMessage;
    }

    this.addUserTo = function (userTo) {
        this.usersTo.push(userTo);
    }
    this.getUsersTo = function () {
        return this.usersTo;
    }

    this.setUuidFromBrowser = function (UUIDFromBrowser) {
        this.uuidFromBrowser = UUIDFromBrowser;
    }
    this.generateUuidFromBrowser = function () {
        this.uuidFromBrowser = UUIDGenerator.generate();
    }
    this.getUuidFromBrowser = function () {
        return this.uuidFromBrowser;
    }

    this.isItNewMessage = function () {
        var itIsNew = false;
        for (var i = 0; i < this.usersTo.length; i++) {
            if (this.usersTo[i].itNewMessage) {
                itIsNew = true;
                break;
            }
        }
        return itIsNew;
    }
    this.getUsersWhichDontRead = function () {
        var users = [];
        for (var i = 0; i < this.usersTo.length; i++) {
            if (this.usersTo[i].itNewMessage
                && this.usersTo[i].userTo != currentNickName) {
                users.push(this.usersTo[i].userTo);
            }
        }
        return users;
    }

    this.setChatName = function (chatName) {
        this.chatName = chatName;
    }

    this.getChatName = function () {
        return this.chatName;
    }
}

function createMessageObjectFromJson(jsonMessage) {
    var message = new createMessageObject();
    message.setId(jsonMessage.id);
    message.setChatId(jsonMessage.chatId);
    message.setDateMessage(new Date(jsonMessage.dateMessage));
    message.setUserFrom(jsonMessage.userFrom);
    message.setTextMessage(jsonMessage.textMessage);
    message.setUuidFromBrowser(jsonMessage.uuidFromBrowser);
    for (var i = 0; i < jsonMessage.usersTo.length; i++) {
        var userTo = new createUserToObject();
        userTo.setUserTo(jsonMessage.usersTo[i].userTo);
        userTo.setItDeleted(jsonMessage.usersTo[i].itDeleted);
        userTo.setItNewMessage(jsonMessage.usersTo[i].itNewMessage);
        message.addUserTo(userTo);
    }
    return message;
}

function createChatObject() {
    this.type = 'chat';
    this.id;
    this.name;
    this.messageForChatList = [];
    this.userList = [];

    this.setId = function (id) {
        this.id = id;
    }

    this.getId = function () {
        return this.id;
    }

    this.setName = function (name) {
        this.name = name;
    }

    this.getName = function () {
        return this.name;
    }

    this.addMessage = function (message) {
        this.messageForChatList.push(message);
    }

    this.getMessages = function () {
        return this.messageForChatList;
    }

    this.addUser = function (user) {
        this.userList.push(user);
    }

    this.getUsers = function () {
        return this.userList;
    }

    this.isUserListContains = function (user) {
        return arrayInclude(this.userList, user);
    }

}

function createChatFromJson(json) {
    var chat = new createChatObject();
    chat.setId(json.id);
    chat.setName(json.name);
    for (var i = 0; i < json.userList.length; i++) {
        chat.addUser(json.userList[i]);
    }
    for (var j = 0; j < json.messageForChatList.length; j++) {
        chat.addMessage(createMessageObjectFromJson(json.messageForChatList[j]));
    }
    return chat;
}

// OTHER FUNCTIONS

function equalsChatIds(id1, id2) {
    if (id1 == id2) return true;
    if (id2 == null
        || id1 == null
        || id1.length != id2.length) return false;

    var currentChatUsers = id1.split("_");
    var checkedChatUsers = id2.split("_");

    if (currentChatUsers.length != checkedChatUsers.length) {
        return false;
    }
    var countEquals = 0;
    for (var i = 0; i < currentChatUsers.length; i++) {
        if (arrayInclude(checkedChatUsers, currentChatUsers[i])) {
            countEquals++;
        }
    }
    if (countEquals == currentChatUsers.length) {
        return true;
    } else {
        return false;
    }
}

function parseJsonForChat(msg) {
    var json = JSON.parse(msg);
    if (json.type === 'user' && json.online) {
        if (json.currentUser) {
            currentNickName = json.nickName;
        }
        writeUserToScreenAboutUser(json.nickName);
    }
    else if (json.type === 'user' && !json.online) {
        removeUserFromOnline(json.nickName);
    }
    else if (json.type == 'chat') {
        var chat = createChatFromJson(json);
        writeAboutChat(chat);
    }
    else if (json.type == 'message' || json.type == 'messageDTO') {
        var jsonMessage = (json.type == 'message') ? json : json.messageForChat;
        if (isThereSomeTabWithChat(jsonMessage.chatId)) {
            var messageToScreen = createMessageObjectFromJson(jsonMessage);
            writeMessageToScreen(messageToScreen);
        }
        if (currentNickName != jsonMessage.userFrom
            && currentChat.getId() != jsonMessage.chatId) {
            var chatName = (json.type == 'messageDTO') ?
                ((json.chatName == currentNickName) ?
                    jsonMessage.userFrom
                    : json.chatName)
                : jsonMessage.userFrom;
            addInfoAboutNewMessage(jsonMessage.chatId, chatName);
        }
    }
    else if(json.type == 'tabs'){
        createTabsForGroupChat(json.holderList);
    }
}

function getUsersFromCurrentChatWithoutCurrent() {
    var otherUsers = [];
    for (var i = 0; i < currentChat.getUsers().length; i++) {
        if (currentChat.getUsers()[i] != currentNickName) {
            otherUsers.push(currentChat.getUsers()[i]);
        }
    }
    return otherUsers;
}

function arrayInclude(arr, obj) {
    return (arr.indexOf(obj) != -1);

}

function isThereSomeTabWithChat(chatId) {
    for (var i = 0; i < allTabs.length; i++) {
        if (equalsChatIds(chatId, allTabs[i].getChat().getId())) {
            return true;
        }
    }
    return false;
}

function getFormatDate(date) {
    var options = {
        day: "2-digit", month: "2-digit", hour12: false,
        hour: "2-digit", minute: "2-digit", second: "2-digit"
    };
    return date.toLocaleTimeString("en-US", options);
}

function findTabWithChat(chatId) {
    for (var i = 0; i < allTabs.length; i++) {
        if (equalsChatIds(allTabs[i].getChat().getId(), chatId)) {
            return allTabs[i];
        }
    }
    return false;
}
function changeTabByMessageFrom(chatId, buttonId) {
    changeTabOnClientServer(chatId);
    var elementForDel = document.getElementById(buttonId);
    elementForDel.parentNode.removeChild(elementForDel);
}
function changeTabOnClientServer(chatId, chatName) {
    if (isThereSomeTabWithChat(chatId)) {
        changeTabOnClient(chatId);
    }
    else {
        changeTabOnServer(chatId, chatName);
    }

}


function changeTabOnServer(chatId, chatName) {
    var chat = new createChatObject();
    chat.setId(chatId);
    if (chatName != null && chatName !== undefined) {
        chat.setName(chatName);
    }
    var arrUsers = chatId.split('_');
    for (var i = 0; i < arrUsers.length; i++) {
        chat.addUser(arrUsers[i]);
    }
    var jsonStr = JSON.stringify(chat);
    sendAnswerForChat(jsonStr);

}

function changeTabOnClient(chatId) {
    for (var i = 0; i < allTabs.length; i++) {
        if (equalsChatIds(allTabs[i].getChat().getId(), chatId)) {
            currentTab = allTabs[i];
            currentChat = allTabs[i].getChat();
            //currentTab.setChatWindow(currentTab.getTab().getElementsByClassName('MyChat')[0]);
            //currentTab.setActiveUsers(currentTab.getTab().getElementsByClassName('activeUsers')[0]);
            activateCurrentTab();
            break;
        }
    }
}

//Functions with DOM elements

function clearClassListCopyElement(elementAfterCopy) {
    if (elementAfterCopy.classList.contains("hide")) {
        elementAfterCopy.classList.remove("hide");
    }
    if (elementAfterCopy.classList.contains("ForCopy")) {
        elementAfterCopy.classList.remove("ForCopy");
    }
}

function writeUserToScreenAboutUser(user) {
    var userElement = userForCopy.cloneNode(true);
    clearClassListCopyElement(userElement);
    userElement.removeAttribute('id');
    userElement.classList.add(user);
    userElement.setAttribute('name', user);
    userElement.innerHTML = user + ";";
    if (user != currentNickName) {
        userElement.onclick = function () {
            changeTabOnClientServer(currentNickName + "_" + user);
        }
        userElement.oncontextmenu = function () {
            if (chatForm.classList.contains("visible")
                && chatForm.getElementsByClassName(user).length == 0) {
                addUserToNewChat(user);
            }
            return false;
        }
        userElement.draggable = true;
    }
    for (var i = 0; i < allTabs.length; i++) {
        if ((allTabs[i].getChat().getId() == 'global')
            || allTabs[i].getChat().isUserListContains(user)) {
            if (allTabs[i].getActiveUsers().getElementsByClassName(user).length == 0) {
                allTabs[i].getActiveUsers().appendChild(userElement);
            }
        }
    }

}

function removeUserFromOnline(user) {
    var elementsForRemove = document.getElementsByClassName(user);
    for (var i = 0; i < elementsForRemove.length; i++) {
        elementsForRemove[i].parentNode.removeChild(elementsForRemove[i]);
    }
}

function writeAboutChat(chat) {
    currentChat = chat;
    setCurrentElementsForChat();
    for (var i = 0; i < chat.getUsers().length; i++) {
        writeUserToScreenAboutUser(chat.getUsers()[i]);
    }
    for (var j = 0; j < chat.getMessages().length; j++) {
        writeMessageToScreen(chat.getMessages()[j]);
    }
}

function createNewTab() {
    var tabId = 'responce_' + currentChat.getId();
    var newButtonForTab = document.getElementById('buttonTab_' + currentChat.getId());
    if ( newButtonForTab== null) {
        newButtonForTab = newChatTabForCopy.cloneNode(true);
        var chatId = currentChat.getId();
        newButtonForTab.id = 'buttonTab_' + chatId;
        clearClassListCopyElement(newButtonForTab);
        newButtonForTab.onclick = function () {
            changeTabOnClient(chatId);
        };
        newButtonForTab.innerHTML = currentChat.getName();
        infoAboutTabsForChat.appendChild(newButtonForTab);
    }
    var tabForAppend = tabForCopy.cloneNode(true);
    tabForAppend.id = tabId;
    clearClassListCopyElement(tabForAppend);
    var elementsForRemoveAfterCopy = tabForAppend.getElementsByClassName('ForCopy');
    for (var i = 0; i < elementsForRemoveAfterCopy.length; i++) {
        elementsForRemoveAfterCopy[i].parentNode.removeChild(elementsForRemoveAfterCopy[i]);
    }
    chatHolder.appendChild(tabForAppend);
    var tabForReturn = new createTabForChat();
    tabForReturn.setChat(currentChat);
    tabForReturn.setTab(tabForAppend);
    tabForReturn.setTabId(tabForAppend.id);
    tabForReturn.setButtonForChange(newButtonForTab);
    return tabForReturn;
}

function createTabsForGroupChat(listTabs){
    for(var i = 0; i < listTabs.length; i++){
        createButtonForTab(listTabs[i].name, listTabs[i].id);
    }
}

function createButtonForTab(chatName, chatId) {
    var newButtonForTab = newChatTabForCopy.cloneNode(true);
     newButtonForTab.id = 'buttonTab_' + chatId;
    clearClassListCopyElement(newButtonForTab);
    newButtonForTab.onclick = function () {
        changeTabOnClientServer(chatId, chatName);
    };
    newButtonForTab.innerHTML = chatName;
    infoAboutTabsForChat.appendChild(newButtonForTab)
}

function activateCurrentTab() {
    var activeTabElements = document.getElementsByClassName('activeTab');
    var activeButton = document.getElementsByClassName('activeButton');
    for (var i = 0; i < activeTabElements.length; i++) {
        activeTabElements[i].classList.add('hide');
        activeTabElements[i].classList.remove('activeTab');
    }
    for (var i = 0; i < activeButton.length; i++) {
        activeButton[i].classList.remove('activeButton');
    }
    currentTab.getTab().classList.add('activeTab');
    currentTab.getButtonForChange().classList.add('activeButton');
    currentTab.getTab().classList.remove('hide');
}

function writeMessageToScreen(msg) {
    var messageLine = createMessageLine(msg);
    var tabForAppend = findTabWithChat(msg.getChatId());
    if (!tabForAppend) {/*NOP*/
    }
    else {
        tabForAppend.getChatWindow().appendChild(messageLine);
        if (tabForAppend.getTabId() == currentTab.getTabId()) {
            scrollDownChat();
        }
    }
}

function createMessageLine(msg) {
    var messageLine = messageForCopy.cloneNode(true);
    messageLine.id = msg.getId();
    clearClassListCopyElement(messageLine);
    var messageTime = messageLine.getElementsByClassName("received")[0];
    if (messageTime !== undefined && messageTime != null) {
        messageTime.innerHTML = getFormatDate(msg.getDateMessage());
    }

    var messageFrom = messageLine.getElementsByClassName("user")[0];
    if (messageFrom !== undefined && messageTime != null) {
        messageFrom.innerHTML = msg.getUserFrom();
    }

    var messageStr = messageLine.getElementsByClassName("message")[0];
    if (messageStr !== undefined && messageTime != null) {
        messageStr.innerHTML = msg.getTextMessage();
    }
    return messageLine;
}

function scrollDownChat() {
    var checkBoxAutoScroll = document.getElementById("AutoScroll");
    if (checkBoxAutoScroll.checked) {
        currentTab.getChatWindow().parentNode.scrollTop = currentTab.getChatWindow().parentNode.scrollHeight;
    }
}

function addInfoAboutNewMessage(chatId, chatName) {
    var elementForChange = document.getElementById('button_' + chatId);
    if (elementForChange === undefined
        || elementForChange == null) {
        createNewMessageFrom(chatId, chatName);
    }
}

function createNewMessageFrom(chatId, chatName) {
    var newMessageFromLine = newMessageFromForCopy.cloneNode(true);
    clearClassListCopyElement(newMessageFromLine);
    newMessageFromLine.id = 'button_' + chatId;
    newMessageFromLine.innerHTML = chatName;
    newMessageFromLine.onclick = function () {
        changeTabByMessageFrom(chatId, newMessageFromLine.id);
    }
    infoAboutNewMessages.appendChild(newMessageFromLine);

}

function openChatForm() {

    changeVisible(chatForm, true);
}

function closeChatForm() {

    changeVisible(chatForm, false);
    var elementsForClean = chatForm.getElementsByClassName("objectRow");
    for (var i = 0; i < elementsForClean.length; i++) {
        elementsForClean[i].value = '';
    }
    var elementsForDelete = chatForm.getElementsByClassName("onlineUser");
    for (var j = 0; j < elementsForDelete.length; j++) {
        elementsForDelete[j].parentNode.removeChild(elementsForDelete[j]);
    }
    usersForNewChat = [];
}

function changeVisible(elementForVisible, needVisualisation) {
    var needClass, removeClass;
    if (needVisualisation) {
        needClass = 'visible';
        removeClass = 'invisible';

    }
    else {
        needClass = 'invisible';
        removeClass = 'visible';
    }
    if (elementForVisible.classList.contains(removeClass)) {
        elementForVisible.classList.remove(removeClass);
    }
    if (!elementForVisible.classList.contains(needClass)) {
        elementForVisible.classList.add(needClass);
    }

}

function addUserToNewChat(user) {
    var userElement = userForCopy.cloneNode(true);
    clearClassListCopyElement(userElement);
    userElement.removeAttribute('id');
    userElement.classList.add(user);
    userElement.innerHTML = user;
    userElement.onclick = function () {
        removeUserFromNewChat(user);
    }
    var elementForAppend = chatForm.getElementsByClassName("userHolderForNewChat")[0];
    elementForAppend.appendChild(userElement);
    usersForNewChat.push(user);

}

function removeUserFromNewChat(user) {
    var elementForDelete = chatForm.getElementsByClassName(user)[0];
    elementForDelete.parentNode.removeChild(elementForDelete);
    var indexInAray = usersForNewChat.indexOf(user);
    if (indexInAray > -1) {
        usersForNewChat.splice(indexInAray, 1);
    }
}

function createChatFromForm() {
    var chatName = document.getElementById("chatName").value;
    if (chatName == '' || usersForNewChat.length <= 1) {
        alert("need to fill name and users (more than one)!");
        return;
    }
    var chatId = currentNickName;
    for (var i = 0; i < usersForNewChat.length; i++) {
        chatId = chatId + "_" + usersForNewChat[i];
    }
    changeTabOnClientServer(chatId, chatName);
    closeChatForm();
}


function dragStart(ev) {
    ev.dataTransfer.effectAllowed='move';
    ev.dataTransfer.setData("Text", ev.target.getAttribute('name'));
    ev.dataTransfer.setDragImage(ev.target,5,5);
    return true;
}

function dragEnter(ev) {
    ev.preventDefault();
    return true;
}
function dragOver(ev) {
    ev.preventDefault();
}

function dragDrop(ev) {
    var data = ev.dataTransfer.getData("Text");
   addUserToNewChat(data);
    return false;
}

