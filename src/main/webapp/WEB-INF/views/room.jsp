<%--
  Created by IntelliJ IDEA.
  User: mikeldpl
  Date: 02.04.2015
  Time: 22:18
--%>
<%@ page language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Room</title>


	<script src="${pageContext.request.contextPath}/resources/js/roomspage.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>

	<script src="${pageContext.request.contextPath}/resources/js/sockjs.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/stomp.js"></script>

	<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/timer.js"></script>

	<!-- LIBRARY FOR MODAL -->
	<script src="${pageContext.request.contextPath}/resources/js/modal_window.js"></script>
	<!-- FOR DUEL PAGE-->
	<script src="${pageContext.request.contextPath}/resources/js/jquery.plugin.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/jquery.countdown.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/kaiji-room-script.js"></script>


	<link href="${pageContext.request.contextPath}/resources/css/roomPage/jquery.countdown.css" rel="stylesheet"
		  type="text/css"/>
	<link href="${pageContext.request.contextPath}/resources/css/roomPage/room.css" rel="stylesheet" type="text/css"/>
	<link href="${pageContext.request.contextPath}/resources/css/roomPage/modal_window.css" rel="stylesheet"
		  type="text/css">
	<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/resources/css/duelPage/duel.css" rel="stylesheet" type="text/css"/>


	<script>
		$(document).ready(function(){
				setResourcePath("${pageContext.request.contextPath}");
				kaijiBeginGame("/game/websocket", "/user/queue/game/${shipName}", "/queue/game/ship");
		});
	</script>
</head>
<body>

<div class="navbar-brand" id="notificationMessage" style="padding: 15px 0px;margin-top: 5px; top: 0px;"></div>

<div id="countdown"></div>

<%--timerSetTime(mlsec)--%>
<%--timerStart()--%>
<%--timerStop()--%>

<%--open_duel()--%>
<%--close_duel()--%>

<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Result</h4>
			</div>
			<div class="modal-body">
				<h2 align="center">GAME OVER</h2>
			</div>
			<div class="modal-footer">
				<a href="<spring:url value="/game/lobby-page"/>"
				   class="btn btn-primary col-md-6 col-md-offset-3">Finish
					game</a>			</div>
		</div>

	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<!--Header-->
		<div class="col-lg-12 head-block">
			<button class="cancelBtn" type="submit" id="cancelGameBtn">Cancel</button>
			<table style="float: right" id="leftCards">
				<tr>
					<td>Remaining cards:</td>
					<td><img src="${pageContext.request.contextPath}/resources/img/room_page/stone.png"></td>
					<td id="leftStone">4</td>
					<td><img src="${pageContext.request.contextPath}/resources/img/room_page/scissors.png"></td>
					<td id="leftScissors">4</td>
					<td><img src="${pageContext.request.contextPath}/resources/img/room_page/paper.png"></td>
					<td id="leftPaper">4</td>
				</tr>
			</table>
		</div>
		<%--<div><h1 id="timer">Game finishes in </h1></div>--%>
		<%--<div><h1 id="leftCards">Left cards </h1></div>--%>

	</div>

	<div class="row">
		<div class="col-lg-3 players-block">
			<div class="playerCardsStars">
				<p id="gamerName">You have:</p>
				<table>

					<tr>
						<td><img src="${pageContext.request.contextPath}/resources/img/room_page/starSmall.png"></td>
						<td id="starCounter"></td>

					</tr>
					<tr id="cardPanel">
						<td><img src="${pageContext.request.contextPath}/resources/img/room_page/stone.png"></td>
						<td>4</td>
						<td><img src="${pageContext.request.contextPath}/resources/img/room_page/scissors.png"></td>
						<td>4</td>
						<td><img src="${pageContext.request.contextPath}/resources/img/room_page/paper.png"></td>
						<td>4</td>
					</tr>
				</table>
			</div>

			<div class="row">
				<div class="col-lg-12">
					<div class="playersInRoom">

						<p><img src="${pageContext.request.contextPath}/resources/img/room_page/playersImg.png"></p>

						<table class="playerState" id="playerStateTemplate">
							<tr>
								<td>
									<table class="playerStars">
										<tr>
											<!-- here to be stars -->

										</tr>
									</table>
								</td>
								<td ROWSPAN=2><!-- <button id="onDuel" class="duelBtn">Duel</button> --></td>
							</tr>
							<tr>
								<td style="font-size: 14px;font-weight: bold"><span
										id="playerName">Nick Washington</span></td>
							</tr>
						</table>


					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-5">
			<div class="desksInRoom" id="desks">

				<p>DESKS</p>
				<table>
					<tr>
						<td>
							<table id="table0">
								<tr>
									<td><img src="${pageContext.request.contextPath}/resources/img/room_page/face.png">

										<div class="leftSide"></div>
									</td>

									<td><div style="background-image: url(${pageContext.request.contextPath}/resources/img/room_page/desk.png); height: 125px; width: 161px;">
										<div class="leftSideCard" style="float:left; margin-top:25px; margin-left:25px">

										</div>

										<div class="rightSideCard" style="float:right; margin-top:25px; margin-right:25px">

										</div>
									</div></td>


									<td><img src="${pageContext.request.contextPath}/resources/img/room_page/face.png">

										<div class="rightSide"></div>
									</td>
								</tr>
							</table>
						</td>
						<td>
							<table id="table1">
								<tr>
									<td><img src="${pageContext.request.contextPath}/resources/img/room_page/face.png">

										<div class="leftSide"></div>
									</td>

									<td><div style="background-image: url(${pageContext.request.contextPath}/resources/img/room_page/desk.png); height: 125px; width: 161px;">
										<div class="leftSideCard" style="float:left; margin-top:25px; margin-left:25px">

										</div>

										<div class="rightSideCard" style="float:right; margin-top:25px; margin-right:25px">

										</div>
									</div></td>

									<td><img src="${pageContext.request.contextPath}/resources/img/room_page/face.png">

										<div class="rightSide"></div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<%--<table id="table2">--%>
								<%--<tr>--%>
									<%--<td><img src="${pageContext.request.contextPath}/resources/img/room_page/face.png">--%>

										<%--<div class="leftSide"></div>--%>
									<%--</td>--%>
									<%--<td><img src="${pageContext.request.contextPath}/resources/img/room_page/desk.png">--%>
									<%--</td>--%>
									<%--<td><img src="${pageContext.request.contextPath}/resources/img/room_page/face.png">--%>

										<%--<div class="rightSide"></div>--%>
									<%--</td>--%>
								<%--</tr>--%>
							<%--</table>--%>
						</td>
						<td>
							<%--<table id="table3">--%>
								<%--<tr>--%>
									<%--<td><img src="${pageContext.request.contextPath}/resources/img/room_page/face.png">--%>

										<%--<div class="leftSide"></div>--%>
									<%--</td>--%>
									<%--<td><img src="${pageContext.request.contextPath}/resources/img/room_page/desk.png">--%>
									<%--</td>--%>
									<%--<td><img src="${pageContext.request.contextPath}/resources/img/room_page/face.png">--%>

										<%--<div class="rightSide"></div>--%>
									<%--</td>--%>
								<%--</tr>--%>
							<%--</table>--%>
						</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="col-lg-4">
			<jsp:include page="chat.jsp"/>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12 footer-block">
			<h1>Powered by dp-081</h1>
		</div>
	</div>
</div>


<!--DUEL -->

<div class="popup__overlay">
	<div class="popup">
		<div class="container">
			<!--Header-->
			<div class="row">
				<div class="col-xs-12 head-block">
					<%--<div id="countdown"></div>--%>
					<!--В хедере у нас только таймер-->
				</div>
			</div>

			<!--Content-->
			<div class="row">     <!-- Этот блок для 1го игрока (фото, Звезды, карты)-->
				<div class="col-xs-3">
					<div class="row">
						<div class="col-xs-12">
							<div class="mySide">
								<P>YOU</P>

								<P><img src="${pageContext.request.contextPath}/resources/img/duel_page/you.png"></P>
							</div>

							<div class="row">
								<div class="col-xs-12">
									<div class="myStars">
										<p>Stars</p>
										<table>
											<tr>
												<td><img
														src="${pageContext.request.contextPath}/resources/img/duel_page/starSmall.png">
												</td>
												<td id="duelStarCounter">3</td>
											</tr>
										</table>
									</div>

									<div class="row">
										<div class="col-xs-12">
											<div class="myCards">
												<p>Cards</p>
												<table id="duelCardPanel">
													<tr>
														<td><img
																src="${pageContext.request.contextPath}/resources/img/duel_page/stone.png">
														</td>
														<td>4</td>
													</tr>
													<tr>
														<td><img
																src="${pageContext.request.contextPath}/resources/img/duel_page/scissors.png">
														</td>
														<td>4</td>
													</tr>
													<tr>
														<td><img
																src="${pageContext.request.contextPath}/resources/img/duel_page/paper.png">
														</td>
														<td>4</td>
													</tr>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Это центральный блок - с ставками и столом-->
				<div class="col-xs-6">
					<div class="duelStake">
						<P>STAKE</P>
						<table>   <!--Общая таблица для ставок-->
							<tr>   <!--В ней одна строка в которой 2 столбца.-->
								<td>   <!--Этот столбец для одного игрока-->
									<table>
										<!--В каждом столбце есть таблица - это для разделения где чьи Звезды.-->
										<tr>
											<th>YOU</th>
										</tr>
										<tr>
											<td><img
													src="${pageContext.request.contextPath}/resources/img/duel_page/starSmall.png">
											</td>
											<td></td>
										</tr>
										<tr>
											<td></td>
											<td></td>
										</tr>
									</table>
								</td>

								<td> <!--Этот столбец с таблицей для соперника-->
									<table>
										<tr>
											<th>ENEMY</th>
										</tr>
										<tr>
											<td><img
													src="${pageContext.request.contextPath}/resources/img/duel_page/starSmall.png">
											</td>
											<td><img
													src="${pageContext.request.contextPath}/resources/img/duel_page/starSmall.png">
											</td>
										</tr>
										<tr>
											<td></td>
											<td></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>

					<!--Сам стол будет здесь-->
					<div class="row">
						<div class="col-xs-12">
							<div class="duelDesk">
								<table>
									<tr>
										<td id="mySideCardPlace"></td>
										<!--Здесь идет карта 1го игрока-->
										<td id="enemySideCardPlace"><img
												src="${pageContext.request.contextPath}/resources/img/duel_page/paper2.gif">
										</td>
										<!--Здесь идет карта 2го игрока-->
									</tr>
								</table>
							</div>
						</div>
					</div>

				</div>

				<!--Блок второго игрока аналогичен первому..-->
				<div class="col-xs-3">
					<div class="row">
						<div class="col-xs-12">
							<div class="enemySide">
								<P id="enemyName">ENEMY</P>

								<P><img src="${pageContext.request.contextPath}/resources/img/duel_page/you.png"></P>
							</div>

							<div class="row">
								<div class="col-xs-12">
									<div class="enemyStars">
										<p>Stars</p>
										<table>
											<tr>
												<td><img
														src="${pageContext.request.contextPath}/resources/img/duel_page/starSmall.png">
												</td>
												<td id="duelEnemyStarCounter"></td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>


</body>
</html>