<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:message code="footer.aboutus" var="footerAboutUs"/>
<spring:message code="aboutus.goalTitle" var="footerGoalTitle"/>
<spring:message code="aboutus.goal" var="footerGoal"/>
<spring:message code="aboutus.whoWeAreTitle" var="footerWhoWeAreTitle"/>
<spring:message code="aboutus.whoWeAre" var="footerWhoWeAre"/>
<spring:message code="aboutus.thanksTitle" var="footerThanksTitle"/>
<spring:message code="aboutus.close" var="footerClose"/>
<spring:message code="gameStatistic.button" var="button1"/>
<spring:message code="aboutus.button" var="button2"/>
<spring:message code="aboutus.devs" var="devs"/>



  <div class ="col-lg-12 head-block" style=" bottom: 0; position: fixed;">
    <div style="margin-top:80px">
      <a  class="lang-change" data-toggle="modal" data-target="#about">
        <a href="?lang=en">English</a> | <a href="?lang=ru">Russian</a> | <a href="?lang=ua">Ukrainian</a>
      </a>
      <a  class="about-develop-btn" data-toggle="modal" data-target="#about">
        ${button2}
      </a>
      <%--Artur Yakubenko 14.09.15--%>
      <a class="statistic-btn" href="/gameStatistic">${button1}</a>
      <%--Artur Yakubenko 14.09.15--%>
      <div class="modal fade" data-backdrop="false" id="about" tabindex="-1" role="dialog"
           aria-labelledby="myModalLabel"
           aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header"
                 style="background-image: url(${pageContext.servletContext.contextPath}/resources/img/background.jpg)">
              <button type="button" class="close" data-dismiss="modal"><span
                      aria-hidden="true">&times;</span><span class="sr-only">${footerClose}</span></button>
            </div>
            <div class="modal-body" align="left">
              <b>${footerGoalTitle}</b><br>
              ${footerGoal}<br><br>
              <b>${footerWhoWeAreTitle}</b><br>
              ${footerWhoWeAre}<br><br>
              <b>${footerThanksTitle}</b><br>
             <p align="center">${devs}</p>
            </div>
            <div class="modal-footer"
                 style="background-image: url(${pageContext.servletContext.contextPath}/resources/img/background.jpg)">
              <button type="button" class="btn btn-default" data-dismiss="modal">${footerClose}</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

