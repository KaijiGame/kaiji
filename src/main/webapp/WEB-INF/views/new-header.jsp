<%--
  Created by IntelliJ IDEA.
  User: Alexandra
  Date: 30.03.2015
  Time: 22:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<c:set var="userName" value="${sessionScope.nickname}"></c:set>



<div class="col-lg-12 head-block">
	<!-- Icon MainPage Button -->
	<a class="u-btn-main" href='/start-page'>
		<img src="${pageContext.request.contextPath}/resources/img/logo.jpg"/>
	</a>

	<div class="navbar-brand" id="notificationMessage" style="padding: 15px 0px;margin-top: 5px; top: 0px;"></div>

	<sec:authorize access="!hasAnyRole('USER_ROLE','ADMIN_ROLE')">
		<!-- Sign Up Button -->
		<button class="top-menu-button u-btn-signup" onclick="location.href='<spring:url value='/registration'/>'">
			Sign Up
		</button>
	</sec:authorize>
	<sec:authorize access="hasAnyRole('USER_ROLE')">
		<!-- Training Button -->
		<button class="top-menu-button u-btn-train" onclick="location.href='<spring:url value='/game/new'/>'">
			Training
		</button>
		<!-- Lobby Button -->
		<div>
			<button class="top-menu-button u-btn-signup"
					onclick="location.href='<spring:url value="/game/lobby-page"/>'">
				Lobby
			</button>
		</div>
	</sec:authorize>
	<sec:authorize access="hasAnyRole('USER_ROLE','ADMIN_ROLE')">
		<!-- User nickname -->
		<div class="username-text"><a
				href="/userProfile">${userName}</a></div>
		<!-- Log out Button -->
		<button class="top-menu-button u-btn-logout" style="width: 11%;"
				onclick="location.href='<spring:url value='/logout'/>'">
			Log out
			<img src="${pageContext.request.contextPath}/resources/img/lobby_page/u68.png"/>
		</button>
	</sec:authorize>
	<!-- MANUAL Button -->
	<div>
		<button class="top-menu-button u-btn-MANUAL" onclick="location.href='<spring:url value="/manual"/>'">
			Manual
		</button>
	</div>
</div>


