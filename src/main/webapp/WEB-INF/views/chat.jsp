<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link href="${pageContext.servletContext.contextPath}/resources/css/bootstrap_chat.css" rel="stylesheet">
<script src="${pageContext.request.contextPath}/resources/js/chat.js"></script>

<form id="do-chat">
    <div class ="someAction">
        <label class="someAction"><input id="AutoScroll" type="checkbox" checked="checked"/>AutoScroll</label>
        <div class=" someAction createChat" onclick="openChatForm()">create group chat</div>
    </div>
    <div class="infoAboutNewMessages" id='infoAboutNewMessages'>You have new messages from:<div class="hide newMessagesFrom ForCopy" id="newMessageFromForCopy"></div>
    </div>
    <div class="infoAboutTabsForChat" id='infoAboutTabsForChat'>Your tabs:<div class="hide newTabForChat ForCopy" id="newTabForChat"></div>
    </div>
    <div id="chatHolder">
        <div id="response_forCopy" class="hide tabForChat ForCopy">
            <div class="activeUsers"> Online users :
                <div class="hide onlineUser ForCopy " ondragstart="return dragStart(event)" id="userForCopy"></div>
            </div>
            <div class="MyDivForChat">
                <table class="table table-bordered MyChat">
                    <tr id="messageForCopy" class="hide messageLine ForCopy">
                        <td class="received"></td><td class="user label label-info"></td><td class="message badge"></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <fieldset>
        <div class="controls">
            <input type="text" class="input-block-level" placeholder="Print your message..." id="message"
                   style="height:60px"/>
            <input type ="button" onclick="send_message()" class="btn btn-large btn-block btn-primary"
                   value="Send message"/>
        </div>
    </fieldset>



        <div class="visibleBlock invisible" id="objectChatForm" ondragenter="return dragEnter(event)" ondrop="return dragDrop(event)" ondragover="return dragOver(event)">
            <input type="text" class="objectRow name" placeholder="name"
                   name="name" id ="chatName"/>
            <div class="userHolderForNewChat" id="userHolderForNewChat"></div>
            <div class="buttonsBox">
                <div class="buttonOK" onclick="createChatFromForm()">ok</div><div class="buttonCancel" onclick="closeChatForm()">cancel</div>
            </div>
        </div>

</form>


