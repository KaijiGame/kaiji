<%--
  Created by Anton Tulskih
  Date: 27.08.2015
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script src="/resources/js/userRegistrationAndChangeProfile.js"></script>

<spring:message code="registration.updateButton" var="updButton"/>
<spring:message code="registration.textName" var="textName"/>
<spring:message code="registration.textNickName" var="textNickName"/>
<spring:message code="registration.textEmail" var="textEmail"/>
<spring:message code="registration.textPassword" var="textPassword"/>
<spring:message code="registration.textPassword2" var="textPassword2"/>
<spring:message code="registration.nicknameAlreadyExistsErr" var="nicknameErr"/>
<spring:message code="registration.emailAlreadyExistsErr" var="emailErr"/>
<!--Login Form + Center Image -->
    <div id="signUp-box" style="margin-left: 15%;margin: auto; margin-top:2%;">

        <h1>${regtitle}</h1>

        <spring:url value="/changePersonalData" var="url"/>
        <form:form name='registrationForm' action="${url}" method='POST'
                   modelAttribute="userDto" enctype="multipart/form-data">

            <div class="form-group has-error has-feedback">
                <form:errors path="*" class="control-label" for="inputError"/>
            </div>
            <c:if test="${nicknameExistsErr eq 'ERROR'}">
                <div class="error">
                    ${nicknameErr}
                </div>
            </c:if>
            <c:if test="${emailExistsErr eq 'ERROR'}">
                <div class="error">
                    ${emailErr}
                </div>
            </c:if>
            <br>
            <center>
                <table>
                    <tr>
                        <td>${textName}</td>
                    </tr>

                    <tr>
                        <td><form:input path="name" id="inputError"
                                        class="form-control"
                                        placeholder="${userDto.name}"
                                        size="25"/></td>

                    </tr>


                    <tr>
                        <td>${textNickName}</td>
                    </tr>

                    <tr>
                        <td><form:input path="nickname" id="inputError"
                                        class="form-control"
                                        placeholder="${userDto.nickname}"
                                        size="25"/></td>

                    </tr>

                    <tr>
                        <td>${textEmail}</td>
                    </tr>

                    <tr>
                        <td><form:input path="email" id="inputError"
                                        class="form-control"
                                        placeholder="${userDto.email}"
                                        size="25"/></td>

                    </tr>

                    <tr>
                        <td>${textPassword}</td>
                    </tr>

                    <tr>
                        <td><form:password path="password" id="inputError"
                                           class="form-control"
                                           placeholder="${def_pass}" size="25"/></td>

                    </tr>

                    <tr>
                        <td>${textPassword2}</td>
                    </tr>

                    <tr>
                        <td><form:password path="confirmPassword" id="inputError"
                                           class="form-control" placeholder="${def_pass}" size="25"/></td>

                    </tr>

                    <input type="file"
                           onchange="readURL(this)"
                           id="imgInp"
                           name="avatarInput"
                           accept="image/gif, image/jpeg, image/jpg, image/png"/>

                    <tr>
                        <td colspan='2'><input name="submit" type="submit"
                                               class="btn btn-primary"
                                               value="${updButton}"/></td>
                    </tr>
                </table>
            </center>

        </form:form>
    </div>
<img id="avatarImg"
     src="${encodedString}"
     onclick="uploadImg();"
     alt="your avatar" />