<%--
  Created by IntelliJ IDEA.
  User: Alexandra
  Date: 30.03.2015
  Time: 23:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<link href="<%=request.getContextPath()%>/resources/css/startpage.css" type="text/css" rel="stylesheet"/>
  <!--Left Image -->
  <div class="col-lg-4">
    <img class ="side-img" alt="" src="<%=request.getContextPath()%>/resources/img/kaiji-startPage1-2.png"/>
  </div>
  <div class="col-lg-4">
    <sec:authorize access="hasAnyRole('USER_ROLE','ADMIN_ROLE')">
      <jsp:include page="rules-page.jsp"></jsp:include>
    </sec:authorize>
    <sec:authorize access="!hasAnyRole('USER_ROLE','ADMIN_ROLE')">
      <jsp:include page="login.jsp"></jsp:include>
    </sec:authorize>
  </div>


  <!--Right Image -->
  <div class="col-lg-4">
    <img class ="side-img" alt="" src="<%=request.getContextPath()%>/resources/img/kaiji-startPage3-4.png"/>

  </div>