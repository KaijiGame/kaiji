<%--
  Created by IntelliJ IDEA.
  User: Jenson Harvey
  Date: 27.08.2015
  Time: 23:00
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:message code="manual_cabinet.greetings" var="greetings"/>
<spring:message code="manual_cabinet.about" var="about"/>
<spring:message code="manual_cabinet.change" var="change"/>
<spring:message code="manual-page.HINTS" var="HINTS"/>


<link href="<%=request.getContextPath()%>/resources/css/startpage.css" type="text/css" rel="stylesheet"/>
<!--Left Image -->
<div class="col-lg-4">
  <img class ="side-img" alt="" src="<%=request.getContextPath()%>/resources/img/kaiji-startPage1-2.png"/>
</div>
<div class="col-lg-4">
  <sec:authorize access="hasAnyRole('USER_ROLE','ADMIN_ROLE')">
    <jsp:include page="profile.jsp"></jsp:include>
  </sec:authorize>
  <sec:authorize access="!hasAnyRole('USER_ROLE','ADMIN_ROLE')">
    <jsp:include page="login.jsp"></jsp:include>
  </sec:authorize>
</div>

<!--Right Image -->
<div class="col-lg-4">
  <div style="font-size: 20px">
    <p>${about}</p>
    <p>${change}</p>
  </div>

</div>

<div class="row">
  <title>margin</title>
  <style>
    body {
      margin: 0;
    }
    .hints {
      padding: 28px;
    }
  </style>
  <div class="hints">
    <div class="col-lg-12 footer-block">
      <h1>
        <p align="center">${HINTS}</p>
      </h1>
    </div>
  </div>
</div>

