<%--
  Created by IntelliJ IDEA.
  User: Jenson Harvey
  Date: 27.08.2015
  Time: 23:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<spring:message code="FAQ.body_title" var="body_title"/>
<spring:message code="FAQ.description" var="description"/>
<spring:message code="FAQ.title" var="title"/>

<spring:message code="FAQ.q1" var="q1"/>
<spring:message code="FAQ.u1" var="u1"/>

<spring:message code="FAQ.q2" var="q2"/>
<spring:message code="FAQ.u2" var="u2"/>

<spring:message code="FAQ.q3" var="q3"/>
<spring:message code="FAQ.u3" var="u3"/>
<spring:message code="FAQ.u31" var="u31"/>
<spring:message code="FAQ.u32" var="u32"/>
<spring:message code="FAQ.u33" var="u33"/>
<spring:message code="FAQ.u34" var="u34"/>
<spring:message code="FAQ.u35" var="u35"/>
<spring:message code="FAQ.u36" var="u36"/>

<spring:message code="FAQ.q4" var="q4"/>
<spring:message code="FAQ.u4" var="u4"/>

<spring:message code="FAQ.q5" var="q5"/>
<spring:message code="FAQ.u5" var="u5"/>

<spring:message code="FAQ.q6" var="q6"/>
<spring:message code="FAQ.u6" var="u6"/>

<link href="<%=request.getContextPath()%>/resources/css/startpage.css" type="text/css" rel="stylesheet"/>
<!--Left Image -->
<div class="col-lg-4">
  <img class ="side-img" alt="" src="<%=request.getContextPath()%>/resources/img/manual_page/FAQleft.jpg"/>
</div>

<div class="col-lg-4">
  <td align="center">

  <div>
    <a onclick="hidetxt('div1'); return false;" href="#" rel="nofollow"><b>${q1}</b><br></a>
    <div style="display:none;" id="div1">
      <b>${u1}</b>
    </div>
  </div>

  <div>
    <a onclick="hidetxt('div2'); return false;" href="#" rel="nofollow"><b>${q2}</b><br></a>
    <div style="display:none;" id="div2">
      <b>${u2}</b>
    </div>
  </div>

  <div>
    <a onclick="hidetxt('div3'); return false;" href="#" rel="nofollow"><b>${q3}</b><br></a>
    <div style="display:none;" id="div3">
      <b>${u3}</b><br>
      <b>${u31}</b><br>
      <b>${u32}</b><br>
      <b>${u33}</b><br>
      <b>${u34}</b><br>
      <b>${u35}</b><br>
      <b>${u36}</b><br>
    </div>
  </div>
  <div>
    <a onclick="hidetxt('div4'); return false;" href="#" rel="nofollow"><b>${q4}</b><br></a>
    <div style="display:none;" id="div4">
      <b>${u4}</b>
    </div>
  </div>

  <div>
    <a onclick="hidetxt('div5'); return false;" href="#" rel="nofollow"><b>${q5}</b><br></a>
    <div style="display:none;" id="div5">
      <b>${u5}</b>
    </div>
  </div>

  <div>
    <a onclick="hidetxt('div6'); return false;" href="#" rel="nofollow"><b>${q6}</b><br></a>
    <div style="display:none;" id="div6">
      <b>${u6}</b>
    </div>
  </div>
</td>
</div>

<!--Right Image -->
<div class="col-lg-4">
  <img class ="side-img" alt="" src="<%=request.getContextPath()%>/resources/img/manual_page/FAQright.jpg"/>

</div>



<script>
  var show;
  function hidetxt(type){
    param=document.getElementById(type);
    if(param.style.display == "none") {
      if(show) show.style.display = "none";
      param.style.display = "block";
      show = param;
    }else param.style.display = "none"
  }
</script>