<%--
  Created by IntelliJ IDEA.
  User: artur_000
  Date: 14.09.2015
  Time: 10:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:message code="admin-page.backButton" var="backButton"/>
<spring:message code="gameStatistic.player" var="player"/>
<spring:message code="gameStatistic.cards" var="cards"/>
<spring:message code="gameStatistic.stars" var="stars"/>
<spring:message code="gameStatistic.date" var="date"/>
<spring:message code="gameStatistic.start" var="start"/>
<spring:message code="gameStatistic.finish" var="finish"/>
<spring:message code="gameStatistic.duration" var="duration"/>
<spring:message code="gameStatistic.wins" var="wins"/>
<spring:message code="gameStatistic.draws" var="draws"/>
<spring:message code="gameStatistic.loses" var="loses"/>
<spring:message code="gameStatistic.score" var="score"/>

<div align="center"><h2><b>Game history</b></h2></div>

<table class="table table-striped">
  <tr>
    <th>${player}</th>
    <th>${cards}</th>
    <th>${stars}</th>
    <th>${date}</th>
    <th>${start}</th>
    <th>${finish}</th>
    <th>${duration}</th>
    <th>${wins}</th>
    <th>${draws}</th>
    <th>${loses}</th>
    <th>${score}</th>
  </tr>

  <c:forEach var="gameStat" items="${statList}">
    <tr>
      <td>${gameStat.playerName}</td>
      <td>${gameStat.cardNumber}</td>
      <td>${gameStat.starNumber}</td>
      <td>${gameStat.gameDate}</td>
      <td>${gameStat.startTime}</td>
      <td>${gameStat.finishTime}</td>
      <td>${gameStat.gameDuration}</td>
      <td>${gameStat.wins}</td>
      <td>${gameStat.draws}</td>
      <td>${gameStat.loses}</td>
      <td>${gameStat.score}</td>
    </tr>
  </c:forEach>
</table>
