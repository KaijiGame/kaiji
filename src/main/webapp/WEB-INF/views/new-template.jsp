<%--
  Created by IntelliJ IDEA.
  User: Alexandra
  Date: 30.03.2015
  Time: 22:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>


<tiles:importAttribute name="title"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link
          href="${pageContext.servletContext.contextPath}/resources/css/bootstrap.min.css"
          rel="stylesheet">
  <link
          href="${pageContext.servletContext.contextPath}/resources/css/bootstrap-responsive.css"
          rel="stylesheet">
  <link
          href="${pageContext.servletContext.contextPath}/resources/css/bootstrap-responsive.min.css"
          rel="stylesheet">
  <link
          href="${pageContext.servletContext.contextPath}/resources/css/bootstrap-theme.css"
          rel="stylesheet">
  <link
          href="${pageContext.servletContext.contextPath}/resources/css/bootstrap-theme.min.css"
          rel="stylesheet">
  <link
          href="${pageContext.servletContext.contextPath}/resources/css/bootstrap.css"
          rel="stylesheet">
  <link
          href="${pageContext.servletContext.contextPath}/resources/css/styles.css"
          rel="stylesheet">
  <link
          href="${pageContext.servletContext.contextPath}/resources/css/custom.css"
          rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/lobbyPage/style.css"
          rel="stylesheet" type="text/css"/> <!--Это наш css-->

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/sockjs.min.js"></script> <!--Этот для Вебсокетов-->
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/stomp.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>

  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <link href="<%=request.getContextPath()%>/resources/css/login_page/styles.css" type="text/css" rel="stylesheet"/>
  <title><spring:message code="${title}"/></title>
</head>
<body>
  <div class="container-fluid" >
    <div class="">
      <div class= "row">
          <tiles:insertAttribute name="header"/>
      </div>
    </div>

    <div class="">
      <div class= "row">
        <tiles:insertAttribute name="content"/>
      </div>
    </div>

    <div class="footer">
      <div class= "row">
        <tiles:insertAttribute name="footer"/>
      </div>
    </div>
  </div>
</body>
</html>
