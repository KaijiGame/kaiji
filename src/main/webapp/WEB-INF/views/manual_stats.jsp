<%--
  Created by IntelliJ IDEA.
  User: Jenson Harvey
  Date: 27.08.2015
  Time: 23:00
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:message code="admin-page.backButton" var="backButton"/>
<spring:message code="gameStatistic.player" var="player"/>
<spring:message code="gameStatistic.cards" var="cards"/>
<spring:message code="gameStatistic.stars" var="stars"/>
<spring:message code="gameStatistic.date" var="date"/>
<spring:message code="gameStatistic.start" var="start"/>
<spring:message code="gameStatistic.finish" var="finish"/>
<spring:message code="gameStatistic.duration" var="duration"/>
<spring:message code="gameStatistic.wins" var="wins"/>
<spring:message code="gameStatistic.draws" var="draws"/>
<spring:message code="gameStatistic.loses" var="loses"/>
<spring:message code="gameStatistic.score" var="score"/>
<spring:message code="manual_statistics.about" var="about"/>
<spring:message code="manual-page.HINTS" var="HINTS"/>

<div align="center"><h2><b>Game history</b></h2></div>

<table class="table table-striped">
  <tr>
    <th>${player}</th>
    <th>${cards}</th>
    <th>${stars}</th>
    <th>${date}</th>
    <th>${start}</th>
    <th>${finish}</th>
    <th>${duration}</th>
    <th>${wins}</th>
    <th>${draws}</th>
    <th>${loses}</th>
    <th>${score}</th>
  </tr>

  <c:forEach begin="0" end="5" var="gameStat" items="${statList}">
    <tr>
      <td>${gameStat.playerName}</td>
      <td>${gameStat.cardNumber}</td>
      <td>${gameStat.starNumber}</td>
      <td>${gameStat.gameDate}</td>
      <td>${gameStat.startTime}</td>
      <td>${gameStat.finishTime}</td>
      <td>${gameStat.gameDuration}</td>
      <td>${gameStat.wins}</td>
      <td>${gameStat.draws}</td>
      <td>${gameStat.loses}</td>
      <td>${gameStat.score}</td>
    </tr>
  </c:forEach>
</table>
<div class="row">
  <title>margin</title>
  <style>
    body {
      margin: 0;
    }
    .hints {
      padding: 28px;
    }
  </style>
  <div class="hints">
    <div class="col-lg-12 footer-block">
      <h1>
        <p align="center">${HINTS}</p>
      </h1>
    </div>
    <div style="font-size: 20px">
      <b>${about}</b><br>
  </div>
</div>