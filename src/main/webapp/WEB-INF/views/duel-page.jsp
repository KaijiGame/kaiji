<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <link href="${pageContext.request.contextPath}/resources/css/duelPage/duel.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.countdown.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/script.js"></script>
    <link href="${pageContext.request.contextPath}/resources/css/roomPage/jquery.countdown.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="container">
    <!--Header-->
    <div class="row">
        <div class="col-xs-12 head-block">
            <div id="countdown"></div>     <!--В хедере у нас только таймер-->
        </div>
    </div>

    <!--Content-->
    <div class="row" >     <!-- Этот блок для 1го игрока (фото, Звезды, карты)-->
        <div class="col-xs-3">
            <div class="row">
                <div class="col-xs-12">
                    <div class="mySide">
                        <P>YOU</P>
                        <P><img src="${pageContext.request.contextPath}/resources/img/duel_page/you.png"></P>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="myStars">
                                <p>Stars</p>
                                <table>
                                    <tr>
                                        <td><img src="${pageContext.request.contextPath}/resources/img/duel_page/starSmall.png"></td>
                                        <td><img src="${pageContext.request.contextPath}/resources/img/duel_page/starSmall.png"></td>
                                        <td><img src="${pageContext.request.contextPath}/resources/img/duel_page/starSmall.png"></td>
                                    </tr>
                                </table>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="myCards">
                                        <p>Cards</p>
                                        <table>
                                            <tr>
                                                <td><img src="${pageContext.request.contextPath}/resources/img/duel_page/stone.png"></td>
                                                <td>4</td>
                                            </tr>
                                            <tr>
                                                <td><img src="${pageContext.request.contextPath}/resources/img/duel_page/scissors.png"></td>
                                                <td>4</td>
                                            </tr>
                                            <tr>
                                                <td><img src="${pageContext.request.contextPath}/resources/img/duel_page/paper.png"></td>
                                                <td>4</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Это центральный блок - с ставками и столом-->
        <div class="col-xs-6">
            <div class="duelStake">
                <P>STAKE</P>
                <table>   <!--Общая таблица для ставок-->
                    <tr>   <!--В ней одна строка в которой 2 столбца.-->
                        <td>   <!--Этот столбец для одного игрока-->
                            <table>      <!--В каждом столбце есть таблица - это для разделения где чьи Звезды.-->
                                <tr>
                                    <th>YOU</th>
                                </tr>
                                <tr>
                                    <td><img src="${pageContext.request.contextPath}/resources/img/duel_page/starSmall.png"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </td>

                        <td> <!--Этот столбец с таблицей для соперника-->
                            <table>
                                <tr>
                                    <th>ENEMY</th>
                                </tr>
                                <tr>
                                    <td><img src="${pageContext.request.contextPath}/resources/img/duel_page/starSmall.png"></td>
                                    <td><img src="${pageContext.request.contextPath}/resources/img/duel_page/starSmall.png"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>

            <!--Сам стол будет здесь-->
            <div class="row">
                <div class="col-xs-12">
                    <div class="duelDesk">
                        <table>
                            <tr>
                                <td></td>  <!--Здесь идет карта 1го игрока-->
                                <td><img src="${pageContext.request.contextPath}/resources/img/duel_page/paper2.gif"></td>  <!--Здесь идет карта 2го игрока-->
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

        </div>

        <!--Блок второго игрока аналогичен первому..-->
        <div class="col-xs-3">
            <div class="row">
                <div class="col-xs-12">
                    <div class="enemySide">
                        <P>ENEMY</P>
                        <P><img src="${pageContext.request.contextPath}/resources/img/duel_page/you.png"></P>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="enemyStars">
                                <p>Stars</p>
                                <table>
                                    <tr>
                                        <td><img src="${pageContext.request.contextPath}/resources/img/duel_page/starSmall.png"></td>
                                        <td><img src="${pageContext.request.contextPath}/resources/img/duel_page/starSmall.png"></td>
                                        <td><img src="${pageContext.request.contextPath}/resources/img/duel_page/starSmall.png"></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

</div>
</body>
</html>

