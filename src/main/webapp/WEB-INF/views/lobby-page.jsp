<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<div>
	<div class="col-lg-5 ships-block">
		<!--Первая часть займет 5 столбц (col-lg-5). А следующая - 8(не больше 12 должно быть)-->
		<div class="ships">
			<table>
				<!--Ну здесь начинается круговерть таблиц в таблице. Все для того чтобы разместить корабли :)-->
				<p>ROOMS</p>
				<tr>
					<td>
						<table>
							<tr>
								<th>${ship.name}</th>
							</tr>
							<tr>
								<td>
									<img src="${pageContext.request.contextPath}/resources/img/lobby_page/ship_img.png">

									<p>${ship.players.players.size()}/${ship.players.maxNumberOfPlayersConfig}</p>
									<a href="<c:url value="/game/room/${ship.name}"/>">
										<button class="joinSubmit" type="submit" id="joinRoomBtn">Join room</button>
									</a>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<table>
							<tr>
								<th><%--Fuiji--%></th>
							</tr>
							<tr>
								<td>
									<%--<img src="${pageContext.request.contextPath}/resources/img/lobby_page/ship_img.png">--%>

									<%--<form action="" method="post">--%>
									<%--<button class="joinSubmit" type="submit">Join room</button>--%>
									<%--</form>--%>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<table>
							<tr>
								<th><%--Harakiri--%></th>
							</tr>
							<tr>
								<td>
									<%--<img src="${pageContext.request.contextPath}/resources/img/lobby_page/ship_img.png">--%>

									<%--<form action="" method="post">--%>
									<%--<button class="joinSubmit" type="submit">Join room</button>--%>
									<%--</form>--%>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<!--col-lg-8 - значит 8 столбцов мы отдаем под чат(из 12ти). + я еще свой класс тут обьявляю, чтобы использ его в сss-->
	<div class="col-lg-7 chat-block">
		<!--Это вторая часть моей строки. Первые 4 столбца ушли на Корабли. Оставшиеся 8 столбцов - под чат.-->
		<div class="chat">
			<jsp:include page="chat.jsp"/>
		</div>
	</div>
</div>
