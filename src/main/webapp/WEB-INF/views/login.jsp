<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<spring:message code="login.pageTitle" var="page_title"/>
<spring:message code="login.loginError" var="login_error"/>
<spring:message code="login.usernameLabel" var="username_label"/>
<spring:message code="login.passwordLabel" var="password_label"/>
<spring:message code="login.submit" var="submit"/>
<spring:message code="login.defNickName" var="def_nickname"/>
<spring:message code="login.defPass" var="def_pass"/>
<spring:message code="login.checkEmailNotification" var="checkEmailNotification"/>
<spring:message code="login.emailActivatedNotification"
                var="emailActivatedNotification"/>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/social-buttons-3.css">
<!--Login Form + Center Image -->
<div class="col-lg-4">
    <div id="login-box" class="form-container" style="margin-left:35%;">

        <h1 style="text-align: center;">${page_title}</h1>
        <c:if test="${not empty error}">
            <div class="error">${login_error}</div>
        </c:if>
        <c:if test="${checkEmailNotif eq 'SENT'}">
            <div class="checkEmailNotification">${checkEmailNotification}</div>
        </c:if>
        <c:if test="${checkEmailNotif eq 'ACTIVATED'}">
            <div
                    class="checkEmailNotification">${emailActivatedNotification}</div>
        </c:if>
        <div class="input-group " style="margin: auto;">
            <form name='loginForm' action="<c:url value='/login' />" method='POST'>

                <table>
                    <tr>
                        <td>${username_label}:</td>
                        <td><input type='text' name='username' value=''
                                    placeholder="${def_nickname}"
                                    autofocus="true"/></td>
                    </tr>
                    <tr>
                        <td>${password_label}:</td>
                        <td><input type='password' name='password'
                                    placeholder="${def_pass}"/></td>
                    </tr>
                    <tr>
                        <td colspan='2'><input name="submit" type="submit"
                                               class="btn btn-default" value="${submit}"/></td>
                    </tr>
                </table>


            </form>
        </div>
        <%--<div class="panel panel-default">--%>
            <%--<div class="panel-body">--%>
                <%--<div class="row social-button-row">--%>
                    <%--<div class="col-lg-4">--%>
                        <%--<!-- Add Facebook sign in button -->--%>
                        <%--<a href="${pageContext.request.contextPath}/auth/facebook">--%>
                            <%--<button class="btn btn-facebook"><i class="icon-facebook"></i><spring:message code="login.facebook"/> </button>--%>
                        <%--</a>--%>
                    <%--</div>--%>
                <%--</div>--%>
            <%--</div>--%>
        <%--</div>--%>
    </div>
</div>
