<%--
  Created by IntelliJ IDEA.
  User: Constantine los
  Date: 27.08.2015
  Time: 15:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<link href="<%=request.getContextPath()%>/resources/css/startpage.css" type="text/css" rel="stylesheet"/>
<!--Left Image -->
<div class="col-lg-4">
  <img class ="side-img" alt="" src="<%=request.getContextPath()%>/resources/img/manual_page/FAQleft.jpg"/>
</div>

<div class="col-lg-4">
  <td align="center">
    <div style="font-size: 25px"> <b>${title}</b></div>
  <a href="/manual_FAQ">FAQ</a>

  <div style="font-size: 25px"> <b>${title}</b></div>
  <a href="/manual_cabinet">Cabinet</a>

  <div style="font-size: 25px"> <b>${title}</b></div>
  <a href="/manual_stats">Stats</a>

  <div style="font-size: 25px"> <b>${title}</b></div>
  <a href="/manual_about_lobby">Lobby</a>
<!--
  <div style="font-size: 25px"> <b>${title}</b></div>
  <a href="/manual_gameroom">About</a>-->

</div>
</td>


<!--Right Image -->
<div class="col-lg-4">
  <img class ="side-img" alt="" src="<%=request.getContextPath()%>/resources/img/manual_page/FAQright.jpg"/>

</div>

