<%--
  Created by IntelliJ IDEA.
  User: Jenson Harvey
  Date: 15.09.2015
  Time: 23:00
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page language="java" pageEncoding="UTF-8" %>

<spring:message code="manual_about_lobby.greetings" var="greetings"/>
<spring:message code="manual_about_lobby.overal" var="overal"/>
<spring:message code="manual_about_lobby.chat" var="chat"/>
<spring:message code="manual_about_lobby.gamestatus" var="gamestatus"/>
<spring:message code="manual_about_lobby.other" var="other"/>
<spring:message code="manual-page.HINTS" var="HINTS"/>




<div>
	<div class="col-lg-5 ships-block">
		<!--Первая часть займет 5 столбц (col-lg-5). А следующая - 8(не больше 12 должно быть)-->
		<div class="ships">
			<table>
				<!--Ну здесь начинается круговерть таблиц в таблице. Все для того чтобы разместить корабли :)-->
				<p>ROOMS</p>
				<tr>
					<td>
						<table>
							<tr>
								<th>ESPIOR</th>
							</tr>
							<tr>
								<td>
									<img src="${pageContext.request.contextPath}/resources/img/lobby_page/ship_img.png">

									<p>0/45</p>
									<a href="/manual_gameroom">
										<button class="joinSubmit" type="submit" id="joinRoomBtn">Join room</button>
									</a>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<table>
							<tr>
								<th><%--Fuiji--%></th>
							</tr>
							<tr>
								<td>
									<%--<img src="${pageContext.request.contextPath}/resources/img/lobby_page/ship_img.png">--%>

									<%--<form action="" method="post">--%>
									<%--<button class="joinSubmit" type="submit">Join room</button>--%>
									<%--</form>--%>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<table>
							<tr>
								<th><%--Harakiri--%></th>
							</tr>
							<tr>
								<td>
									<%--<img src="${pageContext.request.contextPath}/resources/img/lobby_page/ship_img.png">--%>

									<%--<form action="" method="post">--%>
									<%--<button class="joinSubmit" type="submit">Join room</button>--%>
									<%--</form>--%>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<!--col-lg-8 - значит 8 столбцов мы отдаем под чат(из 12ти). + я еще свой класс тут обьявляю, чтобы использ его в сss-->
	<div class="col-lg-7 chat-block">
		<!--Это вторая часть моей строки. Первые 4 столбца ушли на Корабли. Оставшиеся 8 столбцов - под чат.-->
		<div class="chat">
			<jsp:include page="chat.jsp"/>
		</div>
	</div>

	<div class="row">
		<title>margin</title>
		<style>
			body {
				margin: 0;
			}
			.hints {
				padding: 28px;
			}
		</style>
			<div class="hints">
				<div class="col-lg-12 footer-block">
					<h1>
					<p align="center">${HINTS}</p>
					</h1>
				</div>
			<div style="font-size: 15px">
				<p>${overal}</p>
				<p>${chat}</p>
				<p>${gamestatus}</p>
				<p>${other}</p>
			</div>
		</div>
	</div>
</div>
