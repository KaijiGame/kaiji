<%--
  Created by Anton Tulskih
  Date: 27.08.2015
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<spring:message code="profile.personalDataTitle" var="personalDataTitle"/>
<spring:message code="profile.changeLink" var="changeLink"/>
<spring:message code="profile.avatarImgAlt" var="avatarImgAlt"/>
<spring:message code="profile.userNameTitle" var="userNameTitle"/>
<spring:message code="profile.userNicknameTitle" var="userNicknameTitle"/>
<spring:message code="profile.userEmailTitle" var="userEmailTitle"/>
<spring:message code="profile.personalStatisticsTitle" var="personalStatisticsTitle"/>
<spring:message code="profile.gamesPlayedTitle" var="gamesPlayedTitle"/>
<spring:message code="profile.gamesWonTitle" var="gamesWonTitle"/>
<spring:message code="profile.roundsPlayedTitle" var="roundsPlayedTitle"/>
<spring:message code="profile.roundsWonTitle" var="roundsWonTitle"/>
<spring:message code="profile.roundsDrawTitle" var="roundsDrawTitle"/>
<spring:message code="profile.roundsLostTitle" var="roundsLostTitle"/>
<spring:url value="/changePersonalData" var="url"/>
<div>
    <div id="personalDataLblDiv">
        <table>
            <tr>
                <td>
                    <h3>${personalDataTitle}:</h3>
                </td>
                <td>
                    <sup><a href="${url}">${changeLink}</a></sup>
                </td>
            </tr>
        </table>
    </div>
    <div id="userProfileAvatarDiv">
        <img id="userProfileAvatarImg"
             src="${encodedString}"
             alt="${avatarImgAlt}"/>
    </div>
    <div>
        <b>${userNameTitle}: </b>${user.name} <br>
        <b>${userNicknameTitle}: </b>${user.nickname} <br>
        <b>${userEmailTitle}: </b>${user.email} <br>
    </div>

    <h3>${personalStatisticsTitle}:</h3>
        <b>${gamesPlayedTitle}: </b>${statisticsDTO.totalGames} <br>
        <b>${gamesWonTitle}: </b>${statisticsDTO.gameWins}
        (${statisticsDTO.percentOfGamesWon}%)<br>
        <b>${roundsPlayedTitle}: </b>${statisticsDTO.totalRounds} <br>
        <b>${roundsWonTitle}: </b>${statisticsDTO.win}
        (${statisticsDTO.percentOfRoundsWon}%)<br>
        <b>${roundsDrawTitle}: </b>${statisticsDTO.draw}
        (${statisticsDTO.percentOfRoundsDraw}%)<br>
        <b>${roundsLostTitle}: </b>${statisticsDTO.lose}
        (${statisticsDTO.percentOfRoundsLose}%)<br>
</div>