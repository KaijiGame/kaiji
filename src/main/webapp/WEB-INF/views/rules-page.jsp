<%--
  Created by IntelliJ IDEA.
  User: Alexandra
  Date: 28.03.2015
  Time: 23:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<spring:message code="start-page.body_title" var="body_title"/>
<spring:message code="start-page.description" var="description"/>
<spring:message code="start-page.rules_title" var="rules_title"/>
<spring:message code="start-page.rule1" var="rule1"/>
<spring:message code="start-page.rule2" var="rule2"/>
<spring:message code="start-page.rule3" var="rule3"/>
<spring:message code="start-page.rule4" var="rule4"/>
<spring:message code="start-page.rule5" var="rule5"/>
<spring:message code="start-page.rule6" var="rule6"/>
<spring:message code="start-page.rule7" var="rule7"/>
<spring:message code="start-page.game_process_title"
                var="game_process_title"/>
<spring:message code="start-page.step1" var="step1"/>
<spring:message code="start-page.step2" var="step2"/>
<spring:message code="start-page.step3" var="step3"/>
<spring:message code="start-page.condition1" var="condition1"/>
<spring:message code="start-page.condition2" var="condition2"/>
<spring:message code="start-page.create" var="create"/>
<spring:message code="start-page.join" var="join"/>
<spring:message code="start-page.greetings" var="greetings"/>

<div>
    <b>${greetings}</b><br>
</div>