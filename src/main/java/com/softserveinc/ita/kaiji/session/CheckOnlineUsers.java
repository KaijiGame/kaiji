package com.softserveinc.ita.kaiji.session;


import org.apache.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * @author Konstantin Shevchuk
 * @version 1.3
 * @since 01.08.14.
 */

public class CheckOnlineUsers implements HttpSessionListener {

    private static final Logger LOG = Logger.getLogger(CheckOnlineUsers.class);
    private static final String KEY = CheckOnlineUsers.class.getName();

    public void sessionCreated(HttpSessionEvent se) {
        registerInServletContext(se.getSession().getServletContext());
    }

    public void sessionDestroyed(HttpSessionEvent se) {

        String nickname = (String) se.getSession().getAttribute("nickname");
        LOG.trace("Delete Session");
        SessionUtils.getUserSession().get(nickname).getTimer().cancel();
        SessionUtils.getUserSession().remove(nickname);

    }

    private void registerInServletContext(ServletContext servletContext) {

        if (servletContext.getAttribute(KEY) == null) {
            servletContext.setAttribute(KEY, this);
        }
    }

}
