package com.softserveinc.ita.kaiji.dto.game;

/**
 * @author Ievgen Sukhov
 * @version 1.0
 * @since 21.04.14
 */

public class StatisticsDTO {

    private Long win;
    private Long lose;
    private Long draw;
    private Long gameWins;
    private Long totalGames;
    private Double percentOfGamesWon;
    private Long totalRounds;
    private Double percentOfRoundsWon;
    private Double percentOfRoundsDraw;
    private Double percentOfRoundsLose;

    public StatisticsDTO() {
    }

    public Long getTotalGames() {
        return totalGames;
    }

    public void setTotalGames(Long totalGames) {
        this.totalGames = totalGames;
    }

    public Long getGameWins() {
        return gameWins;
    }

    public void setGameWins(Long gameWins) {
        this.gameWins = gameWins;
    }

    public Long getDraw() {
        return draw;
    }

    public void setDraw(Long draw) {
        this.draw = draw;
    }

    public Long getLose() {
        return lose;
    }

    public void setLose(Long lose) {
        this.lose = lose;
    }

    public Long getWin() {
        return win;
    }

    public void setWin(Long win) {
        this.win = win;
    }

    public Double getPercentOfGamesWon() {
        Double percent = getGameWins().doubleValue() / getTotalGames().doubleValue
                () * 10000.0;
        percent = Math.round(percent) / 100.0;
        return percent;
    }

    public Long getTotalRounds() {
        return getWin() + getDraw() + getLose();
    }

    public Double getPercentOfRoundsWon() {
        Double percent = getWin().doubleValue() / getTotalRounds().doubleValue
                () * 10000.0;
        percent = Math.round(percent) / 100.0;
        return percent;
    }

    public Double getPercentOfRoundsDraw() {
        Double percent = getDraw().doubleValue() / getTotalRounds().doubleValue
                () * 10000.0;
        percent = Math.round(percent) / 100.0;
        return percent;
    }

    public Double getPercentOfRoundsLose() {
        if (getTotalRounds() == 0) {
            return 0.0;
        } else {
            Double percent = (100.00 - getPercentOfRoundsWon() -
                    getPercentOfRoundsDraw()) * 100.0;
            percent = Math.round(percent) / 100.0;
            return percent;
        }
    }
}

