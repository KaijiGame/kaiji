package com.softserveinc.ita.kaiji.dto.game;

import org.springframework.stereotype.Component;

import java.sql.Date;
import java.sql.Time;
import java.util.Comparator;

/**
 * Created by Artur Yakubenko
 * on 05.09.2015.
 */
@Component
public class GameStatisticDto {
    private String playerName;
    private String enemyName;
    private Byte cardNumber;
    private Byte starNumber;
    private Date gameDate;
    private Time startTime;
    private Time finishTime;
    private Time gameDuration;
    private Long wins;
    private Long draws;
    private Long loses;
    private Long score;

    /* Artur Yakubenko 05.09.15
    * It was planned to implement score counting withing separate
    * class that would consider many parameters such as duration,
    * bot type, number of games etc. to count game score. This is
    * just a stub.
    * */
    private void initScore() {
        score = wins * 2 + draws;
    }

    private void initDuration() {
        long start = startTime.getTime();
        long finish = finishTime.getTime();
        int secs = (int)((finish - start) / 1000);
        int mins = (int)((finish - start) / 1000 / 60);
        int hours = (int)((finish - start) / 1000 / (60*60));
        gameDuration = new Time(hours, mins, secs);
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public Byte getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(Byte cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Byte getStarNumber() {
        return starNumber;
    }

    public void setStarNumber(Byte starNumber) {
        this.starNumber = starNumber;
    }

    public Date getGameDate() {
        return gameDate;
    }

    public void setGameDate(Date gameDate) {
        this.gameDate = gameDate;
    }

    public Time getStartTime() {
        return startTime;
    }

    public void setStartTime(Time startTime) {
        this.startTime = startTime;
    }

    public Time getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Time finishTime) {
        this.finishTime = finishTime;
    }

    public Time getGameDuration() {
        initDuration();
        return gameDuration;
    }

    public void setGameDuration(Time gameDuration) {
        this.gameDuration = gameDuration;
    }

    public Long getWins() {
        return wins;
    }

    public void setWins(Long wins) {
        this.wins = wins;
    }

    public Long getDraws() {
        return draws;
    }

    public void setDraws(Long draws) {
        this.draws = draws;
    }

    public Long getLoses() {
        return loses;
    }

    public void setLoses(Long loses) {
        this.loses = loses;
    }

    public Long getScore() {
        initScore();
        return score;
    }

    public String getEnemyName() {
        return enemyName;
    }

    public void setEnemyName(String enemyName) {
        this.enemyName = enemyName;
    }

    public void setScore(Long score) {
        this.score = score;
    }

    @Override
    public String toString() {
        initDuration();
        initScore();
        return "GameStatisticsDto{" +
                "playerName='" + playerName + '\'' +
                ", cardNumber=" + cardNumber +
                ", starNumber=" + starNumber +
                ", gameDate=" + gameDate  +
                ", startTime=" + startTime +
                ", finishTime=" + finishTime +
                ", gameDuration=" + gameDuration +
                ", wins=" + wins +
                ", draws=" + draws +
                ", loses=" + loses +
                ", score=" + score +
                '}';
    }

    public static class GameStatCompareScore implements Comparator<GameStatisticDto> {
        @Override
        public int compare(GameStatisticDto o1, GameStatisticDto o2) {
            o1.initScore();
            o2.initScore();

            return o2.score.compareTo(o1.score);
        }
    }

    public static class GameStatCompareDate implements Comparator<GameStatisticDto> {
        @Override
        public int compare(GameStatisticDto o1, GameStatisticDto o2) {
            if (!(o1.gameDate.equals(o2.gameDate)))
                return o2.gameDate.compareTo(o1.gameDate);
            else
                return o2.startTime.compareTo(o1.startTime);
        }
    }
}
