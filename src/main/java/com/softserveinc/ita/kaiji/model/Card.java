package com.softserveinc.ita.kaiji.model;

/**
 * @author Ievgen Sukhov
 *         Created on March 15, 2014
 * @version 1.2
 */

public enum Card {
    ROCK, PAPER, SCISSORS;//, SPOK, LIZARD;

    /**
     * Compares two cards
     *
     * @param opponent Another Card entity to compare with
     * @return Outcome enum: if cards are equals - DRAW,
     * if first player won - WIN
     * and LOSE if opponent won
     */
    public DuelResult match(Card opponent) {
        if (ordinal() == opponent.ordinal()) {
            return DuelResult.DRAW;
        }
        return (((ordinal() - opponent.ordinal() + values().length) % values().length) & 1) == 0
                ? DuelResult.LOSE : DuelResult.WIN;
    }

    public enum DuelResult {
        WIN, LOSE, DRAW
    }
}




