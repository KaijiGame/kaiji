package com.softserveinc.ita.kaiji.model;

public enum SocialMediaService {
    FACEBOOK,
    TWITTER
}