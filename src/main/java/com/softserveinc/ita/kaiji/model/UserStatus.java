package com.softserveinc.ita.kaiji.model;


public enum UserStatus {
    ACTIVE, INACTIVE
}
