package com.softserveinc.ita.kaiji.model;

import com.softserveinc.ita.kaiji.dto.game.GameInfoEntity;
import com.softserveinc.ita.kaiji.model.util.Identifiable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.social.security.SocialUserDetails;

import javax.persistence.*;
import java.sql.Blob;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Represents basic User that can be introduced to system
 *
 * @author Ievgen Sukhov
 * @author Paziy Evgeniy
 * @version 3.4
 * @since 30.03.14
 */
@Entity
@Table(name = "user")
@Inheritance(strategy = InheritanceType.JOINED)
public class User implements Identifiable, UserDetails, SocialUserDetails {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    @Column(name = "name", nullable = false, length = 40)
    private String name;

    @Column(name = "nickname", nullable = false, unique = true, length = 30)
    private String nickname;

    @Column(name = "email", nullable = false, unique = true, length = 40)
    private String email;

    @Column(name = "avatar", nullable = false)
    private Blob avatar;

    @Lob @Basic(fetch = FetchType.EAGER)
    @Column(name = "password", nullable = false, length = 60)
    private String password;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "registration_date", nullable = true)
    private Date registrationDate = new Date();

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "user_role", joinColumns = {@JoinColumn(name = "user_id")})
    @Column(name = "role", nullable = false,
            columnDefinition = "enum('USER_ROLE', 'ADMIN_ROLE')")
    @Enumerated(EnumType.STRING)
    private Set<RoleType> roles = getDefaultRoles();

    @ManyToMany(mappedBy = "users")
    private Set<GameInfoEntity> gameInfoEntities;
    @Enumerated(EnumType.STRING)
    private UserStatus userStatus = UserStatus.INACTIVE;
    @Enumerated(EnumType.STRING)
    private SocialMediaService socialSignInProvider;


    public User() {
    }

    public User(String nickname, String email, String password) {
        this.nickname = nickname;
        this.email = email;
        this.password = password;
    }

    public static Set<RoleType> getDefaultRoles() {
        Set<RoleType> defaultRoles = new HashSet<>();
        defaultRoles.add(RoleType.USER_ROLE);
        return defaultRoles;
    }

    public SocialMediaService getSocialSignInProvider() {
        return socialSignInProvider;
    }

    public void setSocialSignInProvider(SocialMediaService socialSignInProvider) {
        this.socialSignInProvider = socialSignInProvider;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Returns real user name under which current user is registered in system
     *
     * @return {@link java.lang.String} user name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name of user.
     *
     * @param name {@link java.lang.String} new user name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns user's nickname (login)
     *
     * @return {@link java.lang.String} user's nickname
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * Sets user's nickname (login)
     *
     * @param nickname {@link java.lang.String} new user's nickname
     */
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return getRoles();
    }

    /**
     * Returns encrypted user's password
     *
     * @return encrypted user's password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets new encrypted password for user
     *
     * @param password encrypted password for user
     */
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getUsername() {
        return getNickname();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    /**
     * Returns date of user's registration
     *
     * @return {@link java.util.Date} date of user's registration
     */
    public Date getRegistrationDate() {
        return registrationDate;
    }

    /**
     * Sets user registration date.
     *
     * @param registrationDate {@link java.util.Date} date of user's registration
     */
    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    /**
     * Returns user's email
     *
     * @return {@link java.lang.String} user's email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets user's mail.
     *
     * @param email {@link java.lang.String} new user's email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Returns user's avatar
     *
     * @return {@link com.mysql.jdbc.Blob} user's avatar
     */
    public Blob getAvatar() {
        return avatar;
    }

    /**
     * Sets user's avatar.
     *
     * @param avatar {@link com.mysql.jdbc.Blob} new user's avatar
     */
    public void setAvatar(Blob avatar) {
        this.avatar = avatar;
    }

    public Set<RoleType> getRoles() {
        return roles;
    }

    public void setRoles(Set<RoleType> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", nickname='" + nickname + '\'' +
                ", email='" + email + '\'' +
                ", roles=" + roles +
                '}';
    }

    public boolean hasRole(final RoleType role) {
        for (final RoleType roleType : this.getRoles()) {
            if (roleType.equals(role)) {
                return true;
            }
        }
        return false;
    }

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    public Set<GameInfoEntity> getGameInfoEntities() {
        return gameInfoEntities;
    }

    public void setGameInfoEntities(Set<GameInfoEntity> gameInfoEntities) {
        this.gameInfoEntities = gameInfoEntities;
    }

    @Override
    public String getUserId() {
        return getUsername();
    }
}
