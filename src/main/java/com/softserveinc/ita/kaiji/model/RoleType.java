package com.softserveinc.ita.kaiji.model;

import org.springframework.security.core.GrantedAuthority;

/**
 * User's roles
 *
 * @author Paziy Evgeniy
 * @version 1.0
 * @since 10.04.14
 */
public enum RoleType implements GrantedAuthority {
    USER_ROLE("User"),
    ADMIN_ROLE("Admin");

    private final String roleViewName;

    RoleType(final String s) {
        roleViewName = s;
    }

    public String getRoleViewName() {
        return roleViewName;
    }

    @Override
    public String getAuthority() {
        return name();
    }
}
