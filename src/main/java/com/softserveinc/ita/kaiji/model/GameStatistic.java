package com.softserveinc.ita.kaiji.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Artur Yakubenko
 * on 27.08.2015.
 * This class is not used and can be removed
 * or used in implementing statistic collection
 * with Spring data. Currently statistic collection
 * implemented only with GameStatisticDaoJdbc Using JDBCTemplate
 */

@Entity
public class GameStatistic {
    @Id
    @GeneratedValue
    @Column(name = "Id")
    private Integer id;

    @Column(name = "Player_name")
    private String playerName;

    @Column(name = "Enemy_name")
    private String enemyName;

    @Column(name = "Game_result")
    @Enumerated(EnumType.STRING)
//    @Column(name = "Game_result", columnDefinition = "enum('WIN','LOSE', 'DRAW')")
    private Card.DuelResult gameResult;

    @Column(name = "Start_time")
    private Date startTime;

    @Column(name = "Finish_time")
    private Date finishTime;

    @Column(name = "Card_number")
    private Integer cardNumber;

    public GameStatistic() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getEnemyName() {
        return enemyName;
    }

    public void setEnemyName(String enemyName) {
        this.enemyName = enemyName;
    }

    public Card.DuelResult getGameResult() {
        return gameResult;
    }

    public void setGameResult(Card.DuelResult gameResult) {
        this.gameResult = gameResult;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date gameDate) {
        this.startTime = gameDate;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public Integer getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(Integer cardNumber) {
        this.cardNumber = cardNumber;
    }
}
