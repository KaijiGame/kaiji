package com.softserveinc.ita.kaiji.model.util;

import com.softserveinc.ita.kaiji.model.Card;
import com.softserveinc.ita.kaiji.model.GameStatistic;
import com.softserveinc.ita.kaiji.model.game.GameHistory;
import com.softserveinc.ita.kaiji.model.game.GameInfo;
import com.softserveinc.ita.kaiji.model.player.Player;

import java.util.Date;
import java.util.Set;

/**
 * Created by Artur Yakubenko
 * on 28.08.2015.
 * This class is not used and can be removed
 * or used in implementing statistic collection
 * with Spring data. Currently statistic collection
 * implemented only with GameStatisticDaoJdbc Using JDBCTemplate
 *
 */
public class GameStatisticCollector {
    public static GameStatistic collectData(Player player,
                Player enemy, GameHistory gameHistory) {

        GameInfo gameInfo = gameHistory.getGameInfo();
        GameStatistic gameStatistic = new GameStatistic();
        Card.DuelResult gameResult;

        Set<Player> winners = gameHistory.getWinners();
        if (winners.isEmpty()) {
            gameResult = Card.DuelResult.DRAW;
        } else if (winners.contains(player)) {
            gameResult = Card.DuelResult.WIN;
        } else {
            gameResult = Card.DuelResult.LOSE;
        }

        String playerName = player.getName();
        String enemyName = enemy.getName();
        Date startTime = gameInfo.getGameStartTime();
        Date finishTime = new Date();
        Integer cardNumber = gameInfo.getNumberOfCards();

        gameStatistic.setPlayerName(playerName);
        gameStatistic.setEnemyName(enemyName);
        gameStatistic.setStartTime(startTime);
        gameStatistic.setFinishTime(finishTime);
        gameStatistic.setGameResult(gameResult);
        gameStatistic.setCardNumber(cardNumber);

        return gameStatistic;
    }
}
