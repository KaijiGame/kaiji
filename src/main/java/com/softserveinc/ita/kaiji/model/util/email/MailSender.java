package com.softserveinc.ita.kaiji.model.util.email;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

/**
 * @author Sydorenko Oleksandra
 * @version 1.0
 * @since 21.08.14
 */

@Component
public class MailSender {

    private static final Logger LOG = Logger.getLogger(MailSender.class);

    @Autowired
    Environment env;

    @Autowired
    JavaMailSender javaMailSender;

    /**
     * Sends message with specified subject and text to recipient
     *
     * @param to           email of recipient person
     * @param link         activation link
     * @return true if message was successfully send, otherwise - false
     */
    public boolean send(String to, String link) {

        if ("".equals(to)) {
            LOG.error("Empty recipient email address ");
            return false;
        }

        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(env.getProperty("message.subject"));
        message.setText(env.getProperty("message.body") + link);
        javaMailSender.send(message);
        LOG.trace("Sent message successfully....");
        return true;
    }

}
