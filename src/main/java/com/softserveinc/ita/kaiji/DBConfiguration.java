package com.softserveinc.ita.kaiji;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * DB configuration
 *
 * @author Sydorenko Oleksandra
 * @version 1.0
 * @since 20.08.14
 */
@Configuration
@EnableJpaRepositories(basePackages = "com.softserveinc.ita.kaiji.dao")
@EnableTransactionManagement
public class DBConfiguration {

    private Properties connectionProperties;

    public DBConfiguration() {
        connectionProperties = new Properties();
        try {
            connectionProperties.load(new FileInputStream(getClass().getResource("/jdbc.properties").getFile()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (System.getenv("OPENSHIFT_MYSQL_DB_HOST") != null) {
            connectionProperties.put("jdbc.url",
                    "jdbc:mysql://" + System.getenv("OPENSHIFT_MYSQL_DB_HOST") + ":" +
                            System.getenv("OPENSHIFT_MYSQL_DB_PORT"));
            connectionProperties.put("jdbc.username", System.getenv("OPENSHIFT_MYSQL_DB_USERNAME"));
            connectionProperties.put("jdbc.password", System.getenv("OPENSHIFT_MYSQL_DB_PASSWORD"));
        }
    }

    public Properties getConnectionProperties() {
        return connectionProperties;
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(connectionProperties.getProperty("jdbc.driverClassName"));
        dataSource.setUrl(connectionProperties.getProperty("jdbc.url") + "/" +
                connectionProperties.getProperty("jdbc.dbname"));
        dataSource.setUsername(connectionProperties.getProperty("jdbc.username"));
        dataSource.setPassword(connectionProperties.getProperty("jdbc.password"));
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(true);
        vendorAdapter.setDatabasePlatform(connectionProperties.getProperty("hibernate.dialect"));
        vendorAdapter.setShowSql(true);

        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setDataSource(dataSource());
        factory.setPackagesToScan("com.softserveinc.ita.kaiji.model", "com.softserveinc.ita.kaiji.dto");
        factory.setPersistenceUnitName("myPersistenceUnit");
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setJpaProperties(connectionProperties);

        return factory;
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());

        return transactionManager;
    }

}
