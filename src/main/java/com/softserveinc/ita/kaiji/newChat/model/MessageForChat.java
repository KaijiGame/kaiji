package com.softserveinc.ita.kaiji.newChat.model;

import java.io.Serializable;
import java.util.*;

/**
 * Created by Vlad on 29.08.2015.
 */
public class MessageForChat implements Serializable {
    private String type = "message";
    private Long id;
    private String chatId;
    private String userFrom;
    private String textMessage;
    private Date dateMessage;
    private Set<UserTo> usersTo;
    private String uuidFromBrowser;

    public MessageForChat() {
        usersTo = new HashSet<>();
    }

    public MessageForChat(String chatId, String textMessage, Date dateMessage, Set<UserTo> usersTo, String uuidFromBrowser) {
        this.chatId = chatId;
        this.textMessage = textMessage;
        this.dateMessage = new Date(dateMessage.getTime());
        this.usersTo = usersTo;
        this.uuidFromBrowser = uuidFromBrowser;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getUserFrom() {
        return userFrom;
    }

    public void setUserFrom(String userFrom) {
        this.userFrom = userFrom;
    }

    public String getTextMessage() {
        return textMessage;
    }

    public void setTextMessage(String textMessage) {
        this.textMessage = textMessage;
    }

    public Date getDateMessage() {
        return new Date(this.dateMessage.getTime());
    }

    public void setDateMessage(Date dateMessage) {
        this.dateMessage = new Date(dateMessage.getTime());
    }

    public Set<UserTo> getUsersTo() {
        return usersTo;
    }

    public void setUsersTo(Set<UserTo> usersTo) {
        this.usersTo = usersTo;
    }

    public String getUuidFromBrowser() {
        return uuidFromBrowser;
    }

    public void setUuidFromBrowser(String uuidFromBrowser) {
        this.uuidFromBrowser = uuidFromBrowser;
    }

    public void addUserTo(String user, boolean isNewMassage, boolean isDeleted) {
        if (!user.equals(userFrom)) {
            usersTo.add(new UserTo(user, isNewMassage, isDeleted));
        }
    }

    public List<String> takeUsersForMessage() {
        List<String> usersList = new ArrayList<>();
        for (UserTo userTo : usersTo) {
            usersList.add(userTo.getUserTo());
        }
        return usersList;
    }

    public boolean isItNewForUser(String user) {
        boolean isNew = false;
        for (UserTo userTo : usersTo) {
            if (userTo.getUserTo().equals(user)) {
                isNew = userTo.isItNewMessage();
                break;
            }
        }
        return isNew;
    }

    public boolean isItDeletedForUser(String user) {
        boolean isDel = false;
        for (UserTo userTo : usersTo) {
            if (userTo.getUserTo().equals(user)) {
                isDel = userTo.isItDeleted();
                break;
            }
        }
        return isDel;
    }

    public String getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessageForChat messageForChat = (MessageForChat) o;

        if (id != null ? !id.equals(messageForChat.id) : messageForChat.id != null) return false;
        return !(chatId != null ? !chatId.equals(messageForChat.chatId) : messageForChat.chatId != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (chatId != null ? chatId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", chatId=" + chatId +
                ", textMessage='" + textMessage + '\'' +
                ", dateMessage=" + dateMessage +
                ", usersTo=" + usersTo +
                ", uuidFromBrowser='" + uuidFromBrowser + '\'' +
                '}';
    }

    private static class UserTo implements Serializable {
        private String userTo;
        private boolean itNewMessage;
        private boolean itDeleted;

        public UserTo() {
        }

        public UserTo(String userTo, boolean itNewMessage, boolean itDeleted) {
            this.userTo = userTo;
            this.itNewMessage = itNewMessage;
            this.itDeleted = itDeleted;
        }

        public String getUserTo() {
            return userTo;
        }

        public void setUserTo(String userTo) {
            this.userTo = userTo;
        }

        public boolean isItNewMessage() {
            return itNewMessage;
        }

        public void setItNewMessage(boolean itNewMessage) {
            this.itNewMessage = itNewMessage;
        }

        public boolean isItDeleted() {
            return itDeleted;
        }

        public void setItDeleted(boolean itDeleted) {
            this.itDeleted = itDeleted;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            UserTo userTo = (UserTo) o;

            return !(this.userTo != null ? !this.userTo.equals(userTo.userTo) : userTo.userTo != null);

        }

        @Override
        public int hashCode() {
            return userTo != null ? userTo.hashCode() : 0;
        }

        @Override
        public String toString() {
            return "userTo{" +
                    "userTo='" + userTo + '\'' +
                    ", itNewMessage=" + itNewMessage +
                    ", itDeleted=" + itDeleted +
                    '}';
        }
    }
}
