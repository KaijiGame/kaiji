package com.softserveinc.ita.kaiji.newChat.dao;

import com.softserveinc.ita.kaiji.newChat.model.Chat;
import com.softserveinc.ita.kaiji.newChat.model.MessageForChat;


/**
 * Created by Vlad on 31.08.2015.
 */
public interface MessageForChatDao {

    Long save (MessageForChat messageForChat);

    boolean remove(MessageForChat messageForChat);

    boolean removeFromChat(Chat chat);

    boolean removeAll();

    boolean removeAllExceptChat(Chat chat);

    boolean update(MessageForChat messageForChat);

    MessageForChat getById(Long id);


}
