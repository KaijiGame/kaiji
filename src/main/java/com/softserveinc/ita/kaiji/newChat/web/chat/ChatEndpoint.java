package com.softserveinc.ita.kaiji.newChat.web.chat;


import com.softserveinc.ita.kaiji.newChat.model.Chat;
import com.softserveinc.ita.kaiji.newChat.model.MessageForChat;
import com.softserveinc.ita.kaiji.newChat.services.ChatService;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.security.Principal;

/**
 * Created by Bukatin Vlad on 29.08.2015.
 */

@Controller("ChatEndpointController")
public class ChatEndpoint {

    private static final Logger LOG = Logger.getLogger(ChatEndpoint.class);

    @Autowired
    ChatService chatService;


    @MessageMapping("/queue/chat/mess")
    public void onMessage(Principal principal, String msg) {
        if (LOG.isTraceEnabled()) {
            LOG.trace(msg);
        }
        parseJson(msg, principal.getName());
    }

    @SubscribeMapping("/user/queue/chat/sub")
    public void connectHandler(Principal principal
            , @Header(value = "simpSessionId") String simpSessionId) {

        try {
            chatService.addOnlineUser(principal.getName(), simpSessionId);
            LOG.info("Session chat is opened for user: " + principal.getName());
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void disconnectHandler(String simpSessionId) {
        if (LOG.isInfoEnabled()) {
            LOG.info("----x Disconnect session " + simpSessionId);
        }
        chatService.removeOnlineUserBySessionId(simpSessionId);
    }

    private void parseJson(String jsonText, String currentUser) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            if (jsonText.contains("\"type\":\"message\"")) {
                MessageForChat messageForChat = objectMapper.readValue(jsonText, MessageForChat.class);
                chatService.saveMessageAndSend(messageForChat);
            } else if (jsonText.contains("\"type\":\"chat\"")) {
                Chat chat = objectMapper.readValue(jsonText, Chat.class);
                chatService.changingChat(chat, currentUser);
            }
        } catch (JsonMappingException e) {
            LOG.fatal("Error when mapping json", e);
        } catch (JsonParseException e) {
            LOG.fatal("Error when parse json", e);
        } catch (IOException e) {
            LOG.fatal("Something wrong when parsing json", e);
        }
    }

}
