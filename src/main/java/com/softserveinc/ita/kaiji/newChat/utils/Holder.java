package com.softserveinc.ita.kaiji.newChat.utils;

import com.fasterxml.classmate.TypeResolver;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bukatin Vlad on 04.09.2015.
 */
public class Holder<T> {

    List<T> holderList = new ArrayList<>();
    private String type;

    public Holder(String type) {

           this.type = type;
    }

    public List<T> getHolderList() {
        return holderList;
    }

    public void setHolderList(List<T> holderList) {
        this.holderList = holderList;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
