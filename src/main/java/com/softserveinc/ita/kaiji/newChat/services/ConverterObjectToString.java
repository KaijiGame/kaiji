package com.softserveinc.ita.kaiji.newChat.services;

/**
 * Created by Bukatin Vlad on 04.09.2015.
 */
public interface ConverterObjectToString {
        <T> String getMessage(T value);
}
