package com.softserveinc.ita.kaiji.newChat.dto;

/**
 * Created by Bukatin Vlad on 04.09.2015.
 */
public class UserForChatDTO {
    private String type = "user";

    private String nickName;
    private boolean online;
    private boolean currentUser;

    public UserForChatDTO() {
    }

    public UserForChatDTO(String nickName, boolean online) {
        this.nickName = nickName;
        this.online = online;
    }

    public UserForChatDTO(String nickName, boolean online, boolean currentUser) {
        this.nickName = nickName;
        this.online = online;
        this.currentUser = currentUser;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public boolean isCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(boolean currentUser) {
        this.currentUser = currentUser;
    }

    public String getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserForChatDTO that = (UserForChatDTO) o;

        return !(nickName != null ? !nickName.equals(that.nickName) : that.nickName != null);

    }

    @Override
    public int hashCode() {
        return nickName != null ? nickName.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "UserForChatDTO{" +
                "currentNickName='" + nickName + '\'' +
                ", online=" + online +
                ", currentUser=" + currentUser +
                '}';
    }
}
