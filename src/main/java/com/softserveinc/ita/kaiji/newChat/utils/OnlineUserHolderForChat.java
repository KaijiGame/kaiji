package com.softserveinc.ita.kaiji.newChat.utils;

import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Vlad on 02.09.2015.
 */
public class OnlineUserHolderForChat {
    private static OnlineUserHolderForChat onlineHolder = new OnlineUserHolderForChat();
    private Map<String, String> sessionUserMap = new ConcurrentHashMap<>();


    private OnlineUserHolderForChat() {

    }

    public static OnlineUserHolderForChat getOnlineHolder() {
        return onlineHolder;
    }

    public void addUser(String nickName, String sessionId) {
        sessionUserMap.put(sessionId, nickName);
    }

    public String removeUser(String sessionId) {
        String nickName = sessionUserMap.get(sessionId);
        sessionUserMap.remove(sessionId);
        return nickName;
    }

    public List<String> getAllOnlineUsers() {
        List<String> onlineUsers = new ArrayList<>();
        onlineUsers.addAll(sessionUserMap.values());
        return onlineUsers;
    }

}
