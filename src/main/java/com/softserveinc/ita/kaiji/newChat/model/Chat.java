package com.softserveinc.ita.kaiji.newChat.model;

import com.softserveinc.ita.kaiji.newChat.utils.IdForChat;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Bukatin Vlad on 29.08.2015.
 */
public class Chat implements Serializable {
    private static int maxCountMessage = 500;
    private String type = "chat";
    private IdForChat id;
    private String name;
    private List<String> userList;
    private List<MessageForChat> messageForChatList;


    public Chat() {
        this.userList = new CopyOnWriteArrayList<>();
        this.messageForChatList = new CopyOnWriteArrayList<>();
    }

    public Chat(IdForChat id, List<String> userList) {
        this.id = id;
        this.userList = new CopyOnWriteArrayList<>();
        this.messageForChatList = new CopyOnWriteArrayList<>();
        this.userList.addAll(userList);
    }

    public Chat(String id, List<String> userList) {
        this.id = new IdForChat(id);
        this.userList = new CopyOnWriteArrayList<>();
        this.messageForChatList = new CopyOnWriteArrayList<>();
        this.userList.addAll(userList);
    }

    public static int getMaxCountMessage() {
        return maxCountMessage;
    }

    public static void setMaxCountMessage(int maxCountMessage) {
        Chat.maxCountMessage = maxCountMessage;
    }

    @JsonIgnore
    public IdForChat getUnusualId() {
        return id;
    }

    public String getId() {
        return id.getId();
    }

    public void setId(String id) {
        this.id = new IdForChat(id);
    }

    public void setUnusualId(IdForChat id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getUserList() {
        return userList;
    }

    public void setUserList(List<String> userList) {
        this.userList.clear();
        this.userList.addAll(userList);
    }

    public List<MessageForChat> getMessageForChatList() {
        return messageForChatList;
    }

    public void setMessageForChatList(List<MessageForChat> messageForChatList) {
        this.messageForChatList.clear();
        this.messageForChatList.addAll(messageForChatList);
    }

    public void addPlayer(String player) {
        this.userList.add(player);
    }

    public void removePlayer(String player) {
        this.userList.remove(player);
    }

    public void addMessage(MessageForChat messageForChat) {
        this.messageForChatList.add(messageForChat);
    }

    public void removeMessage(MessageForChat messageForChat) {
        this.messageForChatList.remove(messageForChat);
    }

    public String getType() {
        return type;
    }

    public Long getMinMessageId() {
        Long minId = 0L;
        boolean firstIter = true;
        for (MessageForChat mes : messageForChatList) {
            if (firstIter) {
                firstIter = false;
                minId = mes.getId();
            } else if (minId > mes.getId()) {
                minId = mes.getId();
            }
        }
        return minId;
    }

    public void updateMessage(MessageForChat messageForChat) {
        int indexOfMessage = messageForChatList.indexOf(messageForChat);
        messageForChatList.remove(messageForChat);
        messageForChatList.add(indexOfMessage, messageForChat);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Chat chat = (Chat) o;

        return !(id != null ? !id.equals(chat.id) : chat.id != null);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Chat{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", userList=" + userList +
                '}';
    }
}
