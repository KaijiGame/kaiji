package com.softserveinc.ita.kaiji.newChat.utils;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Bukatin Vlad on 31.08.2015.
 */
public class IdForChat implements Serializable {
    private String id;

    public IdForChat() {
    }

    public IdForChat(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IdForChat testChat = (IdForChat) o;
        if (this.id == null
                || testChat.id == null
                || this.id.length() != testChat.id.length()) {
            return false;
        }
        List<String> currentChatUsers = Arrays.asList(this.id.split("_"));
        List<String> checkedChatUsers = Arrays.asList(testChat.id.split("_"));

        if (currentChatUsers.size() != checkedChatUsers.size()) {
            return false;
        }
        int countEquals = 0;
        for (String curUser : currentChatUsers) {
            if (checkedChatUsers.contains(curUser)) {
                countEquals++;
            }
        }
        if (countEquals == currentChatUsers.size()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 0;
        if (id != null) {
            String[] users = id.split("_");
            for (String user : users) {
                hash = hash + user.hashCode();
            }
        }
        return hash;
    }
}
