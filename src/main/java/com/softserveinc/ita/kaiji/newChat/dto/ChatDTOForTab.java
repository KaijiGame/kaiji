package com.softserveinc.ita.kaiji.newChat.dto;

/**
 * Created by bukatinvv on 11.09.2015.
 */
public class ChatDTOForTab {
    private String id;
    private String name;

    public ChatDTOForTab() {
    }

    public ChatDTOForTab(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
