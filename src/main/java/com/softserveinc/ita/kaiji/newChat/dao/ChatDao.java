package com.softserveinc.ita.kaiji.newChat.dao;

import com.softserveinc.ita.kaiji.newChat.model.Chat;
import com.softserveinc.ita.kaiji.newChat.utils.IdForChat;

import java.util.List;

/**
 * Created by Vlad on 31.08.2015.
 */
public interface ChatDao {

    void save(Chat chat);

    boolean remove(Chat chat);

    boolean removeGameChats();

    boolean removeAllChats();

    boolean update(Chat chat);

    Chat getByID(String id);

    Chat getByID(IdForChat id);

    Chat getByUsers(List<String> users);

    List<Chat> getAll();

}
