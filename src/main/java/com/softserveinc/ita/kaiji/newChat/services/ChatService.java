package com.softserveinc.ita.kaiji.newChat.services;

import com.softserveinc.ita.kaiji.newChat.model.Chat;
import com.softserveinc.ita.kaiji.newChat.model.MessageForChat;


/**
 * Created by Vlad on 02.09.2015.
 */
public interface ChatService {

    void  addOnlineUser(String nickName, String sessionId);

    String removeOnlineUserBySessionId(String sessionId);

    Chat getById(String id);

    void saveMessageAndSend(MessageForChat messageForChat);

    void changingChat(Chat chatFromClient, String nickName);


}
