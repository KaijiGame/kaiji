package com.softserveinc.ita.kaiji.newChat.services.implementations;

import com.softserveinc.ita.kaiji.newChat.services.ConverterObjectToString;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created by  Bukatin Vlad on 04.09.2015.
 */
@Component
public class ConverterObjectToStringJSON implements ConverterObjectToString {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConverterObjectToStringJSON.class.getName());
    @Override
    public <T> String getMessage(T value) {
        String message = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            message = mapper.writeValueAsString(value);
        } catch (IOException e) {
            LOGGER.error("exception when create json", e);
        }
        return message;
    }
}
