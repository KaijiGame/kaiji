package com.softserveinc.ita.kaiji.newChat.dto;

import com.softserveinc.ita.kaiji.newChat.model.MessageForChat;

/**
 * Created by bukatinvv on 09.09.2015.
 */
public class MessageForChatDTO {
    private String type = "messageDTO";
    private MessageForChat messageForChat;
    private String chatName;

    public MessageForChatDTO(MessageForChat messageForChat) {
        this.messageForChat = messageForChat;
    }

    public MessageForChat getMessageForChat() {
        return messageForChat;
    }

    public void setMessageForChat(MessageForChat messageForChat) {
        this.messageForChat = messageForChat;
    }

    public String getChatName() {
        return chatName;
    }

    public void setChatName(String chatName) {
        this.chatName = chatName;
    }

    public String getType() {
        return type;
    }
}
