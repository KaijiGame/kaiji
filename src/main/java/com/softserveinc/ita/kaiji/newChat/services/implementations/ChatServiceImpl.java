package com.softserveinc.ita.kaiji.newChat.services.implementations;

import com.softserveinc.ita.kaiji.newChat.dao.ChatDao;
import com.softserveinc.ita.kaiji.newChat.dao.MessageForChatDao;
import com.softserveinc.ita.kaiji.newChat.dto.ChatDTOForTab;
import com.softserveinc.ita.kaiji.newChat.dto.MessageForChatDTO;
import com.softserveinc.ita.kaiji.newChat.dto.UserForChatDTO;
import com.softserveinc.ita.kaiji.newChat.model.Chat;
import com.softserveinc.ita.kaiji.newChat.model.MessageForChat;
import com.softserveinc.ita.kaiji.newChat.services.ChatService;
import com.softserveinc.ita.kaiji.newChat.services.ConverterObjectToString;
import com.softserveinc.ita.kaiji.newChat.utils.Holder;
import com.softserveinc.ita.kaiji.newChat.utils.OnlineUserHolderForChat;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vlad on 02.09.2015.
 */
@Service
public class ChatServiceImpl implements ChatService {
    private OnlineUserHolderForChat userHolderForChat = OnlineUserHolderForChat.getOnlineHolder();
    private static final Logger LOG = Logger.getLogger(ChatServiceImpl.class);
    private static final String GLOBAL_ID = "global";
    @Autowired
    ChatDao chatDao;
    @Autowired
    MessageForChatDao messageForChatDao;

    @Autowired
    private ConverterObjectToString converterObjectToString;

    @Autowired
    public SimpMessagingTemplate template;

    @Override
    public void addOnlineUser(String nickName, String sessionId) {
        changingChat(new Chat(GLOBAL_ID, userHolderForChat.getAllOnlineUsers()), nickName);

        String messageAboutNewUserToAll = converterObjectToString.getMessage(
                new UserForChatDTO(nickName, true, false));
        if (userHolderForChat.getAllOnlineUsers().size() > 0) {
            sendToAll(messageAboutNewUserToAll);
        }
        String messageAboutCurrentUser = converterObjectToString.getMessage(
                new UserForChatDTO(nickName, true, true));

        userHolderForChat.addUser(nickName, sessionId);
        sendMessageToUser(nickName, messageAboutCurrentUser);
        Holder<ChatDTOForTab> holderForTabs = getHolder(nickName);
        String messageAboutGroupChat = converterObjectToString.getMessage(holderForTabs);
        sendMessageToUser(nickName, messageAboutGroupChat);

        if (LOG.isTraceEnabled()) {
            LOG.trace(messageAboutNewUserToAll);
            LOG.trace(messageAboutCurrentUser);
            LOG.trace(messageAboutGroupChat);
        }

    }

    @Override
    public String removeOnlineUserBySessionId(String sessionId) {
        String nickName = userHolderForChat.removeUser(sessionId);
        String messageAboutExitUserToAll = converterObjectToString.getMessage(
                new UserForChatDTO(nickName, false, false));
        if (LOG.isTraceEnabled()) {
            LOG.trace(messageAboutExitUserToAll);
        }
        sendMessageForListUser(
                userHolderForChat.getAllOnlineUsers()
                , messageAboutExitUserToAll);
        return nickName;
    }

    @Override
    public Chat getById(String id) {
        return chatDao.getByID(id);
    }


    @Override
    public void saveMessageAndSend(MessageForChat messageForChat) {
        messageForChatDao.save(messageForChat);
        MessageForChatDTO messageForChatDTO = new MessageForChatDTO(messageForChat);
        messageForChatDTO.setChatName(getChatName(messageForChat.getChatId(), messageForChat.getUserFrom()));
        if (messageForChat.getChatId().equals(GLOBAL_ID)) {
            sendToAll(converterObjectToString.getMessage(messageForChatDTO));
        } else {
            List<String> usersForSend = messageForChat.takeUsersForMessage();
            usersForSend.add(messageForChat.getUserFrom());
            sendMessageForListUser(usersForSend
                    , converterObjectToString.getMessage(messageForChatDTO));
        }
    }

    private Holder getHolder(String nickName){
        List<Chat> chatList = chatDao.getAll();
        List<ChatDTOForTab> tabList = new ArrayList<>();
        for (Chat chat: chatList){
            if (chat.getUserList().contains(nickName)
                    && chat.getUserList().size()>2){
                tabList.add(new ChatDTOForTab(chat.getId(), chat.getName()));
            }
        }
        Holder<ChatDTOForTab> holder = new Holder<>("tabs");
        holder.setHolderList(tabList);
        return holder;
    }

    private String getChatName(String chatId, String userFrom) {
        Chat chat = chatDao.getByID(chatId);
        return getChatName(chat, userFrom);
    }

    private String getChatName(Chat chat, String userFrom) {
        if (chat.getId().equals(GLOBAL_ID)) {
            return "Global";
        } else if (chat.getUserList().size() == 2) {
            for (String user : chat.getUserList()) {
                if (!user.equals(userFrom)) {
                    return user;
                }
            }
        }
        return chat.getName();
    }

    public void changingChat(Chat chatFromClient, String nickName) {
        Chat savingChat = getById(chatFromClient.getId());
        if (savingChat == null) {
            synchronized (ChatServiceImpl.class) {
                savingChat = getById(chatFromClient.getId());
                if (savingChat == null) {
                    chatDao.save(chatFromClient);
                    savingChat = chatFromClient;
                }
            }
        } else if (savingChat.getId().equals(GLOBAL_ID)) {
            savingChat.setUserList(chatFromClient.getUserList());
        }
        savingChat.setName(getChatName(savingChat, nickName));
        String messageAboutChat = converterObjectToString.getMessage(savingChat);
        if (LOG.isTraceEnabled()) {
            LOG.trace(messageAboutChat);
        }
        sendMessageToUser(nickName, messageAboutChat);

    }

    private void sendToAll(String message) {
        sendMessageForListUser(userHolderForChat.getAllOnlineUsers(), message);
    }

    private void sendMessageForListUser(List<String> userList, String message) {
        for (String userName : userList) {
            template.convertAndSendToUser(userName, "/queue/chat/sub", message);
        }
    }

    private void sendMessageToUser(String user, String message) {
        template.convertAndSendToUser(user, "/queue/chat/sub", message);
    }

}
