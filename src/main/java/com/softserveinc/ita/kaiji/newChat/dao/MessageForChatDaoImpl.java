package com.softserveinc.ita.kaiji.newChat.dao;

import com.softserveinc.ita.kaiji.newChat.model.Chat;
import com.softserveinc.ita.kaiji.newChat.model.MessageForChat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by bukatinvv on 01.09.2015.
 */
@Repository
public class MessageForChatDaoImpl implements MessageForChatDao {
    private static Map<Long, MessageForChat> messageMap = new ConcurrentHashMap<>();
    private static Long globalId = 0L;

    @Autowired
    private ChatDao chatDao;

    private static synchronized Long getGlobalId() {
        return ++globalId;
    }

    @Override
    public Long save(MessageForChat messageForChat) {
        if (messageForChat.getId() != null) {
            throw new IllegalArgumentException("this message already exist");
        }

        messageForChat.setId(getGlobalId());
        messageMap.put(messageForChat.getId(), messageForChat);
        Chat chat = chatDao.getByID(messageForChat.getChatId());
        chat.addMessage(messageForChat);
        if (Chat.getMaxCountMessage()<chat.getMessageForChatList().size()){
            remove(
                    getById(
                            chat.getMinMessageId()));
        }
        chatDao.update(chat);
        return messageForChat.getId();
    }

    @Override
    public boolean remove(MessageForChat messageForChat) {
        if (!messageMap.containsKey(messageForChat.getId())) {
            return false;
        }
        Chat chatFromStorage = chatDao.getByID(messageForChat.getChatId());
        chatFromStorage.removeMessage(messageForChat);
        chatDao.update(chatFromStorage);
        messageMap.remove(messageForChat.getId());
        return true;
    }

    @Override
    public boolean removeFromChat(Chat chat) {
        Chat chatFromStorage = chatDao.getByID(chat.getId());
        for (Map.Entry<Long, MessageForChat> pair : messageMap.entrySet()) {
            if (pair.getValue().getChatId().equals(chat.getId())) {
                messageMap.remove(pair.getKey());
                chat.removeMessage(pair.getValue());
                chatFromStorage.removeMessage(pair.getValue());
                chatDao.update(chatFromStorage);
            }
        }
        return true;
    }

    @Override
    public boolean removeAllExceptChat(Chat chat) {

        for (Map.Entry<Long, MessageForChat> pair : messageMap.entrySet()) {
            if (!pair.getValue().getChatId().equals(chat.getId())) {
                messageMap.remove(pair.getKey());
                Chat chatFromStorage = chatDao.getByID(pair.getValue().getChatId());
                chat.removeMessage(pair.getValue());
                chatDao.update(chatFromStorage);
            }
        }
        return true;
    }

    @Override
    public boolean removeAll() {
        messageMap.clear();
        return true;
    }


    @Override
    public boolean update(MessageForChat messageForChat) {
        if (messageForChat.getId() == null) {
            return false;
        }
        messageMap.put(messageForChat.getId(), messageForChat);
        Chat chatFromStorage = chatDao.getByID(messageForChat.getChatId());
        chatFromStorage.updateMessage(messageForChat);
        chatDao.update(chatFromStorage);
        return true;
    }

    @Override
    public MessageForChat getById(Long id) {
        return messageMap.get(id);
    }
}
