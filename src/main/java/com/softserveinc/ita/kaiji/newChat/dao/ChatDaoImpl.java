package com.softserveinc.ita.kaiji.newChat.dao;

import com.softserveinc.ita.kaiji.newChat.model.Chat;
import com.softserveinc.ita.kaiji.newChat.utils.IdForChat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Vlad on 31.08.2015.
 */
@Repository
public class ChatDaoImpl implements ChatDao {
    private static Map<IdForChat, Chat> chatMap = new ConcurrentHashMap<>();
    private static String globalIdChat = "global";

    @Autowired
    private  MessageForChatDao messageForChatDao;


    @Override
    public void save(Chat chat) {
        if (chatMap.containsKey(chat.getUnusualId())) {
            throw new IllegalArgumentException("this chat already exist");
        }
        chatMap.put(chat.getUnusualId(), chat);
    }

    @Override
    public boolean remove(Chat chat) {
        if (!chatMap.containsKey(chat.getUnusualId())) {
            return false;
        }
        chatMap.remove(chat.getUnusualId());
        return true;
    }

    @Override
    public boolean removeGameChats() {
        Chat globalChat = chatMap.get(new IdForChat(globalIdChat));
        chatMap.clear();
        chatMap.put(globalChat.getUnusualId(), globalChat);
        messageForChatDao.removeAllExceptChat(globalChat);
        return false;
    }

    @Override
    public boolean removeAllChats() {
        chatMap.clear();
        messageForChatDao.removeAll();
        return true;
    }

    @Override
    public boolean update(Chat chat) {
        if (!chatMap.containsKey(chat.getUnusualId())) {
            return false;
        }
        chatMap.put(chat.getUnusualId(), chat);
        return true;
    }

    @Override
    public Chat getByID(String id) {
        return chatMap.get(new IdForChat(id));
    }

    @Override
    public Chat getByID(IdForChat id) {
        return chatMap.get(id);
    }

    @Override
    public Chat getByUsers(List<String> users) {
        StringBuilder idFromString = new StringBuilder();
        for (int i = 0; i < users.size(); i++) {
            if (i != 0) {
                idFromString.append("_");
            }
            idFromString.append(users.get(i));

        }
        return chatMap.get(new IdForChat(idFromString.toString()));
    }

    @Override
    public List<Chat> getAll() {
        List<Chat> chatList = new ArrayList<>();
        chatList.addAll(chatMap.values());
        return chatList;
    }
}
