package com.softserveinc.ita.kaiji.web.controller;

import com.softserveinc.ita.kaiji.dto.UserRegistrationDto;
import com.softserveinc.ita.kaiji.dto.game.StatisticsDTO;
import com.softserveinc.ita.kaiji.model.User;
import com.softserveinc.ita.kaiji.service.UserServiceImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.sql.rowset.serial.SerialBlob;
import javax.validation.Valid;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;

/**
 * @author Anton Tulskih
 * @since 25.08.15
 */
@Controller
public class UserProfileController {

    private static final Logger LOG =
            Logger.getLogger(UserProfileController.class);

    String encodedString;

    User user;

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private UserRegistrationDto userDto;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    static public Blob getBlobData(final MultipartFile file) throws IOException,
            SQLException {
        byte[] bytes = file.getBytes();
        return new SerialBlob(bytes);
    }

    @RequestMapping(value = "/userProfile")
    public String showUserProfile(HttpServletRequest request, Model model) {

        if (LOG.isTraceEnabled()) {
            LOG.trace("Showing users' profile");
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug("User nickname is " + request.getSession()
                    .getAttribute("nickname"));
        }

        String nickname = (String) request.getSession().getAttribute
                ("nickname");
        user = userService.findUserByNickname(nickname);

        //receive byte array from the avatar blob object
        byte[] encoded = new byte[0];
        try {
            encoded = user.getAvatar().getBytes(1, (int) user.getAvatar()
                    .length());
        } catch (SQLException sqle) {
            LOG.error("user.getAvatar().getBytes() failure", sqle);
        }
        encodedString = new String(encoded);
        //if received string contains "resources" - it represents path to
        //default avatar image(by default "\resources\img\anonymous_avatar.png")
        //if not - received from blob array of bytes gets encoded to Base64
        //format string to be images' src on JSP page
        if (!encodedString.contains("resources")) {
            encoded =
                   org.apache.commons.codec.binary.Base64.encodeBase64(encoded);
            encodedString = new String(encoded);
            encodedString = "data:image/jpeg;base64," + encodedString;
        }
        System.out.println("encodedString = " + encodedString);
        model.addAttribute("encodedString", encodedString);
        model.addAttribute("user", user);

        StatisticsDTO statisticsDTO = userService.getStatsForUser(nickname);

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("UserDto: win - %d, draw - %d, lose - %d,"
                    + " games won - %d, total games - %d",
                    statisticsDTO.getWin(), statisticsDTO.getDraw(),
                    statisticsDTO.getLose(), statisticsDTO.getGameWins(),
                    statisticsDTO.getTotalGames()));
        }

        model.addAttribute("statisticsDTO", statisticsDTO);

        return "user-profile";
    }

    @RequestMapping(value = "/changePersonalData")
    public ModelAndView initChangePersonalDataForm() {

        ModelAndView model = new ModelAndView();

        if (LOG.isTraceEnabled()) {
            LOG.trace("initializing changePersonalDataForm");
        }

        userDto.setName(user.getName());
        userDto.setNickname(user.getNickname());
        userDto.setEmail(user.getEmail());

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("userDto name is %s, nickname is %s, "
                            + "email is %s", userDto.getName(), userDto.getNickname(),
                    userDto.getEmail()));
        }

        model.addObject("userDto", userDto);
        model.addObject("encodedString", encodedString);
        model.setViewName("changePersonalData");
        return model;
    }

    @RequestMapping(value = "/changePersonalData", method = RequestMethod.POST)
    public ModelAndView
    receiveChangePersonalDataForm(HttpServletRequest request,
                                  @RequestParam("avatarInput") MultipartFile
                                          avatarInput,
                                  @ModelAttribute("userDto")
                                  @Valid UserRegistrationDto userDto,
                                  BindingResult br) {
        ModelAndView model = new ModelAndView();

        if (br.hasErrors()) {

            if (LOG.isInfoEnabled()) {
                LOG.info("errors in the dto object");
            }

            model.setViewName("changePersonalData");

        } else if (userService.findUserByNickname(userDto.getNickname()) != null
                && !userDto.getNickname().equals(user.getNickname())) {

            if (LOG.isInfoEnabled()) {
                LOG.info("User with such nickname already exists. Returning back to 'registration-form'");
            }

            model.addObject("nicknameExistsErr", "ERROR");
            model.setViewName("changePersonalData");

        } else if (userService.findUserByEmail(userDto.getEmail()) != null
                && !userDto.getEmail().equals(user.getEmail())) {

            if (LOG.isInfoEnabled()) {
                LOG.info("User with such email already exists. Returning back "
                        + "to 'registration-form'");
            }

            model.addObject("emailExistsErr", "ERROR");
            model.setViewName("changePersonalData");

        } else {

            user.setName(userDto.getName());
            user.setNickname(userDto.getNickname());
            user.setEmail(userDto.getEmail());
            user.setPassword(passwordEncoder.encode(userDto.getPassword()));
            try {
                if (avatarInput.getSize() == 0) {
                    //doing nothing, leave same avatar
                } else {
                    user.setAvatar(getBlobData(avatarInput));
                }
            } catch (IOException ioe) {
                LOG.error("new File(\"/resources/img/anonymous_avatar.png\") "
                        + "failure", ioe);
            } catch (SQLException sqle) {
                LOG.error("user.setAvatar(getBlobData()) failure", sqle);
            }

            if (LOG.isInfoEnabled()) {
                LOG.info("updating user in the database");
            }

            if (LOG.isDebugEnabled()) {
                LOG.debug(String.format("%n"
                                + "user.name = %s%n"
                                + "user.nickname = %s%n"
                                + "user.email = %s%n"
                                + "user.password = %s%n"
                                + "avatar name = %s",
                        user.getName(), userDto.getNickname(),
                        user.getEmail(), userDto.getPassword(),
                        user.getAvatar()));
            }

            userService.updateUser(user);
            request.getSession().setAttribute("nickname", user.getNickname());
            model.setViewName("redirect:/userProfile");

        }
        return model;
    }
}
