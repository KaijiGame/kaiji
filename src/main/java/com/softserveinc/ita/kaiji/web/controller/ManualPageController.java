package com.softserveinc.ita.kaiji.web.controller;

import com.softserveinc.ita.kaiji.dao.GameStatisticDaoJdbc;
import com.softserveinc.ita.kaiji.dto.game.GameStatisticDto;
import com.softserveinc.ita.kaiji.dto.game.StatisticsDTO;
import com.softserveinc.ita.kaiji.model.User;
import com.softserveinc.ita.kaiji.service.UserServiceImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

/**
 * @author Constantine_Los
 * @since 04.09.2015
 */

@Controller
public class ManualPageController {

    private static final Logger LOG =
            Logger.getLogger(UserProfileController.class);

    String encodedString;

    User user;

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private GameStatisticDto gameStatisticDto;

    @Autowired
    private GameStatisticDaoJdbc gameStatisticDaoJdbc;

    @RequestMapping("/manual")
    public String getManualPage() {
        return "manual-page";
    }
    @RequestMapping("/manual_FAQ")
    public String getManualFAQPage() {
        return "manual-FAQ";
    }
    @RequestMapping("/manual_stats")
    public String getManualStatisticsPage(ModelMap modelMap) {
        List<GameStatisticDto> statList = gameStatisticDaoJdbc.getGameStatistics();
        Collections.sort(statList, new GameStatisticDto.GameStatCompareDate());
        modelMap.addAttribute("statList", statList);
        return "manual-stats";
    }
    @RequestMapping("/manual_cabinet")
    public String getManualCabinetPage(HttpServletRequest request, Model model) {

        if (LOG.isTraceEnabled()) {
            LOG.trace("Showing users' profile");
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug("User nickname is " + request.getSession()
                    .getAttribute("nickname"));
        }

        String nickname = (String) request.getSession().getAttribute
                ("nickname");
        user = userService.findUserByNickname(nickname);

        //receive byte array from the avatar blob object
        byte[] encoded = new byte[0];
        try {
            encoded = user.getAvatar().getBytes(1, (int) user.getAvatar()
                    .length());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        encodedString = new String(encoded);
        //if received string contains "resources" - it represents path to
        //default avatar image(by default "\resources\img\anonymous_avatar.png")
        //if not - received from blob array of bytes gets encoded to Base64
        //format string to be images' src on JSP page
        if (!encodedString.contains("resources")) {
            encoded =
                    org.apache.commons.codec.binary.Base64.encodeBase64(encoded);
            encodedString = new String(encoded);
            encodedString = "data:image/jpeg;base64," + encodedString;
        }
        System.out.println("encodedString = " + encodedString);
        model.addAttribute("encodedString", encodedString);
        model.addAttribute("user", user);

        StatisticsDTO statisticsDTO = userService.getStatsForUser(nickname);

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("UserDto: win - %d, draw - %d, lose - %d,"
                            + " games won - %d, total games - %d",
                    statisticsDTO.getWin(), statisticsDTO.getDraw(),
                    statisticsDTO.getLose(), statisticsDTO.getGameWins(),
                    statisticsDTO.getTotalGames()));
        }

        model.addAttribute("statisticsDTO", statisticsDTO);
        return "manual-cabinet";
    }
    @RequestMapping("/manual_about_lobby")
    public String getManualAboutLobbyPage() {
        return "manual-about-lobby";
    }
    @RequestMapping("/manual_gameroom")
    public String getTutorialGamePage() {
        return "manual-gameroom";
    }
}
