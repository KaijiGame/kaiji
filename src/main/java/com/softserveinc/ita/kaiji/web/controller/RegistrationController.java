package com.softserveinc.ita.kaiji.web.controller;

import com.softserveinc.ita.kaiji.dao.UserDAO;
import com.softserveinc.ita.kaiji.dto.UserRegistrationDto;
import com.softserveinc.ita.kaiji.model.User;
import com.softserveinc.ita.kaiji.model.UserStatus;
import com.softserveinc.ita.kaiji.model.util.email.MailSender;
import com.softserveinc.ita.kaiji.service.UserService;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.sql.rowset.serial.SerialBlob;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;

/**
 * @author Vladyslav Shelest, Anton Tulskih
 * @version 2.0
 * @since 22.04.14.
 */
@Controller
public class RegistrationController {

    private static final Logger LOG = Logger.getLogger(RegistrationController.class);


    static public Blob getBlobData(final MultipartFile file) throws IOException,
            SQLException {
        byte[] bytes = file.getBytes();
        return new SerialBlob(bytes);
    }

    static public Blob getBlobData(final File file) throws IOException,
            SQLException {
        byte[] bytes = IOUtils.toByteArray(String.valueOf(file));
        return new SerialBlob(bytes);
    }

    @Autowired
    private UserRegistrationDto userDto;
    @Autowired
    private MailSender mailSender;
    @Autowired
    private UserService userService;
    @Autowired
    private UserDAO userDAO;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String getRegistrationForm(Model model) {
        if (LOG.isTraceEnabled()) {
            LOG.trace("getRegistrationForm");
        }

        //to prevent filling registration form with data from last user, who
        // changed his personal info
        userDto.setName("");
        userDto.setNickname("");
        userDto.setEmail("");
        userDto.setPassword("");
        userDto.setConfirmPassword("");

        model.addAttribute("userDto", userDto);
        return "registration-form";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ModelAndView
    receiveFormModel(@RequestParam("avatarInput") MultipartFile avatarInput,
                     @ModelAttribute("userDto") @Valid UserRegistrationDto
                             userDto, BindingResult br, HttpServletRequest request) {

        if (LOG.isTraceEnabled()) {
            LOG.trace("receiveFormModel");
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("%n"
                            + "userDto.name = %s%n"
                            + "userDto.nickname = %s%n"
                            + "userDto.email = %s%n"
                            + "userDto.password = %s%n"
                            + "userDto.confirmPassword = %s%n"
                            + "avatarInput name = %s%n"
                            + "avatarInput size = %d",
                    userDto.getName(), userDto.getNickname(),
                    userDto.getEmail(), userDto.getPassword(),
                    userDto.getConfirmPassword(), avatarInput.getOriginalFilename(),
                    avatarInput.getSize()));
        }

        ModelAndView model = new ModelAndView();

        if (br.hasErrors()) {
            if (LOG.isInfoEnabled()) {
                LOG.info("errors in the dto object");
            }
            model.setViewName("registration-form");

        } else if (userDAO.findByEmail(userDto.getEmail()) == null &&
                userDAO.findByNickname(userDto.getNickname()) == null) {

            User user = new User();
            user.setName(userDto.getName());
            user.setEmail(userDto.getEmail());
            user.setNickname(userDto.getNickname());
            user.setPassword(passwordEncoder.encode(userDto.getPassword()));
            user.setUserStatus(UserStatus.INACTIVE);
            //if no file was choose in the <input type="file"> tag on JSP
            //page, then create a new file with path to default avatar image
            //and store it in the blob object. Otherwise store file from <input>
            try {
                if (avatarInput.getSize() == 0) {
                    File defaultAvatar = new File("/resources/img/anonymous_avatar.png");
                    user.setAvatar(getBlobData(defaultAvatar));
                } else {
                    user.setAvatar(getBlobData(avatarInput));
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            if (LOG.isInfoEnabled()) {
                LOG.info("saving user to the database");
            }
            String activationUrl = String.format("%s://%s:%d/activate/%s",request.getScheme(),  request.getServerName(), request.getServerPort(), user.getNickname());
            if (LOG.isDebugEnabled()) {
                LOG.debug(String.format("%n"
                                + "user.name = %s%n"
                                + "user.nickname = %s%n"
                                + "user.email = %s%n"
                                + "user.password = %s%n"
                                + "avatar name = %s",
                        user.getName(), userDto.getNickname(),
                        user.getEmail(), userDto.getPassword(),
                        user.getAvatar()));
            }

            userDAO.save(user);
            mailSender.send(user.getEmail(), activationUrl);
            model.addObject("checkEmailNotif", "SENT");
            model.setViewName("login");

        } else if (userDAO.findByNickname(userDto.getNickname()) != null) {

            if (LOG.isInfoEnabled()) {
                LOG.info("User with such nickname already exists. Returning back to 'registration-form'");
            }

            model.addObject("loginExistsErr", "ERROR");
            model.setViewName("registration-form");

        } else if (userDAO.findByEmail(userDto.getEmail()) != null) {

            if (LOG.isInfoEnabled()) {
                LOG.info("User with such email already exists. Returning back "
                        + "to 'registration-form'");
            }

            model.addObject("emailExistsErr", "ERROR");
            model.setViewName("registration-form");
        }
        return model;
    }

    @RequestMapping(value = "/activate/{nickname}", method = RequestMethod.GET)
    public String activateUser(@PathVariable("nickname") String nickname,
                               Model model) {
        userService.activateUser(nickname);
        model.addAttribute("checkEmailNotif", "ACTIVATED");
        return "login";
    }
}
