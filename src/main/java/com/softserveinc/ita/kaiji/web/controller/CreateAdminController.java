package com.softserveinc.ita.kaiji.web.controller;

import com.softserveinc.ita.kaiji.dao.UserDAO;
import com.softserveinc.ita.kaiji.model.RoleType;
import com.softserveinc.ita.kaiji.model.User;
import com.softserveinc.ita.kaiji.model.UserStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashSet;
import java.util.Set;

@Controller
public class CreateAdminController {

    private final static String EMAIL = "admin@admin.com";

    @Autowired
    private UserDAO userDao;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @RequestMapping(value = "/createAdmin", method = RequestMethod.GET)
    public String createAdmin() {
        if (userDao.findByEmail(EMAIL) == null) {
            User user = new User();
            user.setName("admin");
            user.setEmail(EMAIL);
            user.setNickname("admin");
            Set<RoleType> roles = new HashSet<>();
            roles.add(RoleType.ADMIN_ROLE);
            user.setRoles(roles);
            user.setUserStatus(UserStatus.ACTIVE);
            user.setPassword(passwordEncoder.encode("admin"));
            userDao.save(user);
        }
        return "redirect:/";
    }
}
