package com.softserveinc.ita.kaiji.web.controller;

import com.softserveinc.ita.kaiji.dao.GameStatisticDaoJdbc;
import com.softserveinc.ita.kaiji.dto.game.GameStatisticDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collections;
import java.util.List;

/**
 * @author ievgen_s
 * @since 13.03.14
 */

@Controller
public class StartPageController {

    /* Artur Yakubenko 05.09.15 */
    @Autowired
    private GameStatisticDto gameStatisticDto;
    @Autowired
    private GameStatisticDaoJdbc gameStatisticDaoJdbc;
    /* Artur Yakubenko 05.09.15 */

    @RequestMapping("/rules")
    public String getRulesPage() {
        return "rules-page";
    }

    @RequestMapping("/*")
    public String getStartPage() {
        return "start-page";
    }

    @RequestMapping("/statistic/user")
    public String getStatisticPage() {
        return "statistics";
    }

    /* Artur Yakubenko 05.09.15 */
    @RequestMapping("/gameStatistic")
    public String getGameStatistics(ModelMap modelMap) {
        List<GameStatisticDto> statList = gameStatisticDaoJdbc.getGameStatistics();
        Collections.sort(statList, new GameStatisticDto.GameStatCompareDate());
        modelMap.addAttribute("statList", statList);
        return "gameStatistic";
    }
    /* Artur Yakubenko 05.09.15 */
}
