package com.softserveinc.ita.kaiji.dao;

import com.softserveinc.ita.kaiji.model.GameStatistic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by Artur Yakubenko
 * on 27.08.2015.
 * This class is not used and can be removed
 * or used in implementing statistic collection
 * with Spring data. Currently statistic collection
 * implemented only with GameStatisticDaoJdbc Using JDBCTemplate
 */

public interface GameStatisticDao extends JpaRepository<GameStatistic, Integer> {
    @Query("select s from GameStatistic s where s.playerName = :name")
    GameStatistic findByName(@Param("name") String name);
}