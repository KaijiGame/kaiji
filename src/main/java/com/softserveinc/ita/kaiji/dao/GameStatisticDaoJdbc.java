package com.softserveinc.ita.kaiji.dao;

import com.softserveinc.ita.kaiji.dto.game.GameStatisticDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Artur Yakubenko
 * on 05.09.2015.
 */

@Repository
public class GameStatisticDaoJdbc {

    @Autowired
    private DataSource dataSource;

    public List<GameStatisticDto> getGameStatistics() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        String query =
            "SELECT" +
                " g.id, nickname, number_of_stars, number_of_cards," +
                " start_time game_date," +
                " start_time," +
                " finish_time," +
                " COUNT(IF(duel_result = 'WIN', 1, NULL)) wins," +
                " COUNT(IF(duel_result = 'DRAW', 1, NULL)) draws," +
                " COUNT(IF(duel_result = 'LOSE', 1, NULL)) loses" +
            " FROM" +
                " user u, link_game_to_user l, game g," +
                " game_history h, round r, round_detail d" +
            " WHERE" +
                " u.id = l.user_id AND g.id = l.game_id AND" +
                " g.id = h.game_id AND h.id = r.game_history_id AND r.id = d.round_id" +
            " GROUP BY" +
                " g.id, nickname" +
            " ORDER BY" +
                " game_date, start_time DESC";
        return jdbcTemplate.query(query, new GameStatDtoMapper());
    }

    private class GameStatDtoMapper implements RowMapper<GameStatisticDto> {
        @Override
        public GameStatisticDto mapRow(ResultSet rs, int i) throws SQLException {
            GameStatisticDto gameStatDto = new GameStatisticDto();
            gameStatDto.setPlayerName(rs.getString("nickname"));
            gameStatDto.setStarNumber(rs.getByte("number_of_stars"));
            gameStatDto.setCardNumber(rs.getByte("number_of_cards"));
            gameStatDto.setGameDate(rs.getDate("game_date"));
            gameStatDto.setStartTime(rs.getTime("start_time"));
            gameStatDto.setFinishTime(rs.getTime("finish_time"));
            gameStatDto.setWins(rs.getLong("wins"));
            gameStatDto.setDraws(rs.getLong("draws"));
            gameStatDto.setLoses(rs.getLong("loses"));
            return gameStatDto;
        }
    }
}