package com.softserveinc.ita.kaiji.multiplay.domain.player;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 25.03.2015 1:23.
 */
public enum StateOfPlayer {
    ONLINE, OFFLINE, IN_DUEL, LOSS, WIN
}
