package com.softserveinc.ita.kaiji.multiplay.dto.server.table;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 30.03.2015 21:19.
 */
public class DtoSide {
    private String name;
    private String Card;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCard() {
        return Card;
    }

    public void setCard(String card) {
        Card = card;
    }
}
