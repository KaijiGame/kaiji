package com.softserveinc.ita.kaiji.multiplay;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 30.03.2015 20:26.
 */
public class KaijiException extends RuntimeException {
    public KaijiException() {
    }

    public KaijiException(String message) {
        super(message);
    }

    public KaijiException(String message, Throwable cause) {
        super(message, cause);
    }

    public KaijiException(Throwable cause) {
        super(cause);
    }
}
