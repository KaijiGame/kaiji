package com.softserveinc.ita.kaiji.multiplay.dto.server;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 26.03.2015 22:18.
 */
public class DtoBase {
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
