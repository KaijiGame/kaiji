package com.softserveinc.ita.kaiji.multiplay.dao;

import java.io.Serializable;
import java.util.Collection;

/**
 * Project testFromEmpty
 * Created by mikeldpl
 * 22.03.2015 2:10.
 */
public interface DAO<T, ID extends Serializable> {

    void save(T domain);

    void delete(ID id);

    T findOne(ID id);

    boolean contains(ID domain);

    Collection<T> getAll();

}
