package com.softserveinc.ita.kaiji.multiplay.domain.room;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 25.03.2015 2:17.
 */
public enum StateOfRoom {
    GAME, SELECTING
}
