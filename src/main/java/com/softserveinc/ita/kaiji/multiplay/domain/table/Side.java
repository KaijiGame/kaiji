package com.softserveinc.ita.kaiji.multiplay.domain.table;

import com.softserveinc.ita.kaiji.multiplay.domain.InvalidActionException;
import com.softserveinc.ita.kaiji.multiplay.domain.InvalidDomainStateException;
import com.softserveinc.ita.kaiji.multiplay.domain.card.Card;
import com.softserveinc.ita.kaiji.multiplay.domain.message.InOutMessage;
import com.softserveinc.ita.kaiji.multiplay.domain.player.ShipPlayer;
import com.softserveinc.ita.kaiji.multiplay.domain.player.StateOfPlayer;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 29.03.2015 2:34.
 */
public class Side {
    private final Rate rate = new Rate();
    private ShipPlayer player;
    private Card card;

    public Side() {
    }

    public ShipPlayer getPlayer() {
        return player;
    }

    public void setPlayer(InOutMessage msg, ShipPlayer player) {
        if (player.getState() != StateOfPlayer.ONLINE) {
            throw new InvalidActionException(player.getName() + " player can't be " + player.getState() + " when sets as a side");
        }
        player.setState(msg, StateOfPlayer.IN_DUEL);
        this.player = player;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public Rate getRate() {
        return rate;
    }

    public void reset(InOutMessage msg) {
        player = null;
        card = null;
    }

    public void validateCardsRate() {
        if (!(player != null && card != null && rate != null && getRate().getStars() > 0 &&
                getPlayer().getDeck().getNumberOfCards(getCard()) != 0 && getRate().valid(player)))
            throw new InvalidDomainStateException(getPlayer().getName() + " haven't valid cards or rate");
    }
}
