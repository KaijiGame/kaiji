package com.softserveinc.ita.kaiji.multiplay.domain.table;

import com.softserveinc.ita.kaiji.multiplay.domain.message.InOutMessage;
import com.softserveinc.ita.kaiji.multiplay.domain.player.ShipPlayer;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 29.03.2015 2:36.
 */
public class Rate {

    private int stars = 1;

    public Rate() {
    }

    public int getStars() {
        return stars;
    }

    public boolean valid(ShipPlayer player) {
        return player.getNumberOfStart() >= getStars();
    }

    public void win(InOutMessage msg, ShipPlayer player) {
        player.setNumberOfStart(msg, player.getNumberOfStart() + getStars());
    }

    public void lost(InOutMessage msg, ShipPlayer player) {
        player.setNumberOfStart(msg, player.getNumberOfStart() - getStars());
    }
}