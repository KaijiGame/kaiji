package com.softserveinc.ita.kaiji.multiplay.domain.message;

import com.softserveinc.ita.kaiji.multiplay.KaijiException;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 31.03.2015 21:16.
 */
public class InvalidMessageException extends KaijiException {
    public InvalidMessageException() {
    }

    public InvalidMessageException(String message) {
        super(message);
    }

    public InvalidMessageException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidMessageException(Throwable cause) {
        super(cause);
    }
}
