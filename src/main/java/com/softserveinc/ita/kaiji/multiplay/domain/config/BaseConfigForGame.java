package com.softserveinc.ita.kaiji.multiplay.domain.config;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 25.03.2015 0:48.
 */
public class BaseConfigForGame {
    private long durationConfig = 30_000;
    private int maxNumberOfPlayersConfig = 2;
    private boolean availableSubscribedForPlayerRoomConfig = true;
    private String name = "unnamed";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDurationConfig() {
        return durationConfig;
    }

    public void setDurationConfig(long durationConfig) {
        this.durationConfig = durationConfig;
    }

    public int getMaxNumberOfPlayersConfig() {
        return maxNumberOfPlayersConfig;
    }

    public void setMaxNumberOfPlayersConfig(int maxNumberOfPlayersConfig) {
        this.maxNumberOfPlayersConfig = maxNumberOfPlayersConfig;
    }

    public boolean isAvailableSubscribedForPlayerRoomConfig() {
        return availableSubscribedForPlayerRoomConfig;
    }

    public void setAvailableSubscribedForPlayerRoomConfig(boolean availableSubscribedForPlayerRoomConfig) {
        this.availableSubscribedForPlayerRoomConfig = availableSubscribedForPlayerRoomConfig;
    }

}
