package com.softserveinc.ita.kaiji.multiplay.dto.server.table;

import com.softserveinc.ita.kaiji.multiplay.domain.table.StateOfTable;
import com.softserveinc.ita.kaiji.multiplay.dto.server.DtoBase;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 29.03.2015 0:42.
 */
public class DtoTable extends DtoBase {
    private int id;
    private StateOfTable state;
    private DtoSide left = new DtoSide();
    private DtoSide right = new DtoSide();

    public DtoTable(int id) {
        setType("TABLE");
        this.id = id;
    }

    public DtoSide getLeft() {
        return left;
    }

    public void setLeft(DtoSide left) {
        this.left = left;
    }

    public DtoSide getRight() {
        return right;
    }

    public void setRight(DtoSide right) {
        this.right = right;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public StateOfTable getState() {
        return state;
    }

    public DtoTable setState(StateOfTable state) {
        this.state = state;
        return this;
    }

    public DtoTable setLeftName(String name) {
        left.setName(name);
        return this;
    }

    public DtoTable setRightName(String name) {
        right.setName(name);
        return this;
    }

    public DtoTable setLeftCard(String name) {
        left.setCard(name);
        return this;
    }

    public DtoTable setRightCard(String name) {
        right.setCard(name);
        return this;
    }
}
