package com.softserveinc.ita.kaiji.multiplay.websocket.controller;

import com.softserveinc.ita.kaiji.newChat.web.chat.ChatEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 22.03.2015 18:42.
 */
@Component
public class DisconnectListener implements ApplicationListener<SessionDisconnectEvent> {

    @Autowired
    public SocketGameController socketGameController;

    @Override
    public void onApplicationEvent(SessionDisconnectEvent event) {
        socketGameController.disconnectHandler(event.getSessionId());
    }
}
