package com.softserveinc.ita.kaiji.multiplay.dao;


import com.softserveinc.ita.kaiji.multiplay.domain.player.PlayerOnlineInfo;

/**
 * Project testFromEmpty
 * Created by mikeldpl
 * 22.03.2015 2:13.
 */


public interface PlayerOnlineInfoDAO extends DAO<PlayerOnlineInfo, String> {
}
