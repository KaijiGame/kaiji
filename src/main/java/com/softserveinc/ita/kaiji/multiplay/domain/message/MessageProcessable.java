package com.softserveinc.ita.kaiji.multiplay.domain.message;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 30.03.2015 3:35.
 */
public interface MessageProcessable {
    void processMessage(InOutMessage message);
}
