package com.softserveinc.ita.kaiji.multiplay.dto.server.room;

import java.util.HashMap;
import java.util.Map;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 28.03.2015 19:15.
 */
public class DtoShip extends DtoPlayerRoom {

    private final Map<String, Integer> deck = new HashMap<>();

    public DtoShip(String name) {
        super(name);
    }

    public Map<String, Integer> getDeck() {
        return deck;
    }
}
