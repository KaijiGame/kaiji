package com.softserveinc.ita.kaiji.multiplay.domain.room;

import com.softserveinc.ita.kaiji.multiplay.domain.table.ShipTable;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 30.03.2015 22:49.
 */
public class ShipTables {
    private final ShipTable[] tables;

    public ShipTables(Ship ship, int numberOfTablesConfig) {
        tables = new ShipTable[numberOfTablesConfig];
        for (int i = 0; i < numberOfTablesConfig; i++) {
            tables[i] = new ShipTable(i, ship);
        }
    }

    public ShipTable getFreeTable() {
        for (int i = 0; i < tables.length; i++) {
            if (tables[i].isFree()) {
                return tables[i];
            }
        }
        return null;
    }

    public ShipTable[] getTables() {
        return tables;
    }

    public ShipTable getById(int i) {
        return tables[i];
    }
}
