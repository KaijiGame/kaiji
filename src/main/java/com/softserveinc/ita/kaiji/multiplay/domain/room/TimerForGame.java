package com.softserveinc.ita.kaiji.multiplay.domain.room;


import com.softserveinc.ita.kaiji.multiplay.domain.InvalidDomainStateException;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 25.03.2015 0:26.
 */
public class TimerForGame {

    private long durationConfig;
    private long begin;


    public TimerForGame(long durationConfig) {
        this.durationConfig = durationConfig;
    }


    public long getDurationConfig() {
        return durationConfig;
    }

    public void setDurationConfig(long durationConfig) {
        this.durationConfig = durationConfig;
    }


    public void start() {
        begin = System.currentTimeMillis();
    }

    public long left() {
        return begin == 0 ? 0 : durationConfig - (System.currentTimeMillis() - begin);
    }

    public void setLeftTime(long left) {
        if (left < 0) {
            throw new InvalidDomainStateException("Left time can't be less 0");
        }
        if (left == 0) {
            begin = 0;
        } else {
            begin = left - durationConfig + System.currentTimeMillis();
        }
    }
}