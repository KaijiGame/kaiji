package com.softserveinc.ita.kaiji.multiplay.dao;


import com.softserveinc.ita.kaiji.multiplay.domain.room.Ship;

/**
 * Project testFromEmpty
 * Created by mikeldpl
 * 22.03.2015 2:18.
 */
public interface RoomDAO extends DAO<Ship, String> {
}
