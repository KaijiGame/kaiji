package com.softserveinc.ita.kaiji.multiplay.domain.player;

import com.softserveinc.ita.kaiji.multiplay.domain.InvalidDomainStateException;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 25.03.2015 4:26.
 */
public class InvalidDomainPlayerStateException extends InvalidDomainStateException {
    public InvalidDomainPlayerStateException() {
    }

    public InvalidDomainPlayerStateException(String message) {
        super(message);
    }

    public InvalidDomainPlayerStateException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidDomainPlayerStateException(Throwable cause) {
        super(cause);
    }
}
