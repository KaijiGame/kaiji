package com.softserveinc.ita.kaiji.multiplay.dto.server.player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 26.03.2015 22:31.
 */
public class DtoShipPlayer extends DtoBasePlayer {
    private final Map<String, Integer> deck = new HashMap<>();
    private final List<DtoShipPlayerPublic> whoChallenging = new ArrayList<>();
    private final List<DtoShipPlayerPublic> iChallenged = new ArrayList<>();

    public DtoShipPlayer(String name) {
        super(name);
    }

    public List<DtoShipPlayerPublic> getWhoChallenging() {
        return whoChallenging;
    }

    public List<DtoShipPlayerPublic> getiChallenged() {
        return iChallenged;
    }

    public Map<String, Integer> getDeck() {
        return deck;
    }

    public DtoShipPlayer addWhoChallenging(DtoShipPlayerPublic dtoShipPlayerPublic) {
        whoChallenging.add(dtoShipPlayerPublic);
        return this;
    }

    public DtoShipPlayer addIChallenged(DtoShipPlayerPublic dtoShipPlayerPublic) {
        iChallenged.add(dtoShipPlayerPublic);
        return this;
    }
}
