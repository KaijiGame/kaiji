package com.softserveinc.ita.kaiji.multiplay.dto.server.player;

import com.softserveinc.ita.kaiji.multiplay.dto.server.DtoBase;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 26.03.2015 22:20.
 */
public class DtoBasePlayer extends DtoBase {

    private String name;

    public DtoBasePlayer(String name) {
        setType("PLAYER");
        setName(name);
    }

    public String getName() {
        return name;
    }

    public DtoBasePlayer setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DtoBasePlayer that = (DtoBasePlayer) o;

        if (!name.equals(that.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
