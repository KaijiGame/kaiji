package com.softserveinc.ita.kaiji.multiplay.domain.config;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 25.03.2015 4:14.
 */
public class ShipConfigForGame extends BaseConfigForGame {
    private int playerNumberOfStart = 3;
    private int numberOfCardsTypesConfig = 3;
    private int numberOfCardsOneTypeConfig = 4;
    private int numberOfTablesConfig = 3;

    public int getNumberOfTablesConfig() {
        return numberOfTablesConfig;
    }

    public void setNumberOfTablesConfig(int numberOfTablesConfig) {
        this.numberOfTablesConfig = numberOfTablesConfig;
    }

    public int getNumberOfCardsOneTypeConfig() {
        return numberOfCardsOneTypeConfig;
    }

    public void setNumberOfCardsOneTypeConfig(int numberOfCardsOneTypeConfig) {
        this.numberOfCardsOneTypeConfig = numberOfCardsOneTypeConfig;
    }

    public int getNumberOfCardsTypesConfig() {
        return numberOfCardsTypesConfig;
    }

    public void setNumberOfCardsTypesConfig(int numberOfCardsTypesConfig) {
        this.numberOfCardsTypesConfig = numberOfCardsTypesConfig;
    }

    public int getPlayerNumberOfStart() {
        return playerNumberOfStart;
    }

    public void setPlayerNumberOfStart(int playerNumberOfStart) {
        this.playerNumberOfStart = playerNumberOfStart;
    }
}
