package com.softserveinc.ita.kaiji.multiplay.dto.server.room;

import com.softserveinc.ita.kaiji.multiplay.domain.room.StateOfRoom;
import com.softserveinc.ita.kaiji.multiplay.dto.server.DtoBase;
import com.softserveinc.ita.kaiji.multiplay.dto.server.player.DtoBasePlayerPublic;

import java.util.ArrayList;
import java.util.List;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 27.03.2015 1:45.
 */
public class DtoPlayerRoom extends DtoBase {

    private String name;
    private Long time;
    private List<DtoBasePlayerPublic> players = new ArrayList<>();
    private StateOfRoom state;
    private String message;

    public DtoPlayerRoom(String name) {
        setType("SHIP");
        setName(name);
    }

    public String getMessage() {
        return message;
    }

    public DtoPlayerRoom setMessage(String message) {
        this.message = message;
        return this;
    }

    public StateOfRoom getState() {
        return state;
    }

    public DtoPlayerRoom setState(StateOfRoom state) {
        this.state = state;
        return this;
    }

    public List<DtoBasePlayerPublic> getPlayers() {
        return players;
    }

    public void setPlayers(List<DtoBasePlayerPublic> players) {
        this.players = players;
    }

    public DtoPlayerRoom addPlayer(DtoBasePlayerPublic player) {
        players.add(player);
        return this;
    }

    public Long getTime() {
        return time;
    }

    public DtoPlayerRoom setTime(Long time) {
        this.time = time;
        return this;
    }

    public String getName() {
        return name;
    }

    public DtoPlayerRoom setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DtoPlayerRoom that = (DtoPlayerRoom) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
