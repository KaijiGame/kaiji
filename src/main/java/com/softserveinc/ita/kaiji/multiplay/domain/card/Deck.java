package com.softserveinc.ita.kaiji.multiplay.domain.card;

import java.util.HashMap;
import java.util.Map;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 28.03.2015 17:29.
 */
public class Deck {

    private final Map<Card, Integer> deck = new HashMap<>();
    private final int numberOfCardsTypesConfig;

    public Deck(int numberOfCardsOneTypeConfig, int numberOfCardsTypesConfig) {
        for (int i = 0; i < numberOfCardsTypesConfig; i++) {
            deck.put(new Card(i, numberOfCardsTypesConfig), numberOfCardsOneTypeConfig);
        }
        this.numberOfCardsTypesConfig = numberOfCardsTypesConfig;
    }

    public Map<Card, Integer> getDeck() {
        return deck;
    }

    public int getNumberOfCardsTypesConfig() {
        return numberOfCardsTypesConfig;
    }

    public int getNumberOfCards(Card card) {
        return deck.get(card);
    }

    public void setNumberOfCards(Card card, int number) {
        if (number < 0)
            throw new InvalidDomainCardStateException("Number of cards can't be less then 0");
        deck.put(card, number);
    }

    public int decreaseCard(Card card) {
        int res;
        setNumberOfCards(card, res = getNumberOfCards(card) - 1);
        return res;
    }

    public Map<String, Integer> getMapStToInt() {
        Map<String, Integer> res = new HashMap<>();
        for (Map.Entry<Card, Integer> cards : deck.entrySet()) {
            if (res.put(cards.getKey().getName(), cards.getValue()) != null) {
                throw new InvalidDomainCardStateException("Deck have illegal cards.");
            }
        }
        return res;
    }

    public boolean isNotEmpty() {
        boolean res = false;
        for (Integer val : deck.values()) {
            res |= val != 0;
        }
        return res;
    }
}
