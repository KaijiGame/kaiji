package com.softserveinc.ita.kaiji.multiplay.domain;

import com.softserveinc.ita.kaiji.multiplay.KaijiException;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 30.03.2015 20:26.
 */
public class InvalidActionException extends KaijiException {
    public InvalidActionException() {
    }

    public InvalidActionException(String message) {
        super(message);
    }

    public InvalidActionException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidActionException(Throwable cause) {
        super(cause);
    }
}
