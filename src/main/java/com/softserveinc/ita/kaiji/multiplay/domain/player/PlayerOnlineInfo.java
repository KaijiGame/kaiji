package com.softserveinc.ita.kaiji.multiplay.domain.player;

import com.softserveinc.ita.kaiji.multiplay.domain.room.PlayRoom;
import com.softserveinc.ita.kaiji.multiplay.service.SendMessageByWebSocketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Project testFromEmpty
 * Created by mikeldpl
 * 21.03.2015 23:37.
 */
@Entity
@Component
@Scope("prototype")
public class PlayerOnlineInfo {

    private static SendMessageByWebSocketService sendMessageByWebSocketService;
    @Id
    private final String idSession;
    private final String naem;
    private final PlayRoom playRoom;

    public PlayerOnlineInfo(String idSession, String naem, PlayRoom playRoom) {
        this.idSession = idSession;
        this.naem = naem;
        this.playRoom = playRoom;
    }

    @Autowired
    public void setSendMessageByWebSocketService(SendMessageByWebSocketService sendMessageByWebSocketService) {
        PlayerOnlineInfo.sendMessageByWebSocketService = sendMessageByWebSocketService;
    }

    public void send(Object msg) {
        sendMessageByWebSocketService.sendMessageToPlayerOnShip(this, msg);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        return getClass().isInstance(o) &&
                getIdSession().equals(((PlayerOnlineInfo) o).getIdSession());
    }

    @Override
    public int hashCode() {
        return getIdSession().hashCode();
    }

    public String getIdSession() {
        return idSession;
    }

    public String getName() {
        return naem;
    }

    public PlayRoom getPlayRoom() {
        return playRoom;
    }

    public boolean goOffLina() {
        return this.getPlayRoom().removePlayer(this);
    }

    public boolean goOnLina() {
        if (this.getPlayRoom() != null)
            return this.getPlayRoom().addPlayer(this);
        return false;
    }
}
