package com.softserveinc.ita.kaiji.multiplay.domain.table;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 29.03.2015 0:41.
 */
public enum StateOfTable {
    IN_GAME, EMPTY
}
