package com.softserveinc.ita.kaiji.multiplay.domain;

import com.softserveinc.ita.kaiji.multiplay.domain.player.PlayerOnlineInfo;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 28.03.2015 16:20.
 */
public interface GameElement extends TranslateToDto {
    void addSubscriber(PlayerOnlineInfo info);

    void sendMessageToSubscribers(Object msg);

    void removeSubscriber(PlayerOnlineInfo info);

    void removeAllSubscribers();
}
