package com.softserveinc.ita.kaiji.multiplay.dto.server.player;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 28.03.2015 14:02.
 */
public class DtoShipPlayerPublic extends DtoBasePlayerPublic {

    private Integer stars;

    public DtoShipPlayerPublic(String name) {
        super(name);
    }

    public Integer getStars() {
        return stars;
    }

    public DtoShipPlayerPublic setStars(Integer stars) {
        this.stars = stars;
        return this;
    }

    @Override
    public DtoShipPlayerPublic setDelet(boolean delete) {
        return (DtoShipPlayerPublic) super.setDelet(delete);
    }
}
