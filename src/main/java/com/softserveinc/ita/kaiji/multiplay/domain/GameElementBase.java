package com.softserveinc.ita.kaiji.multiplay.domain;

import com.softserveinc.ita.kaiji.multiplay.domain.player.PlayerOnlineInfo;

import java.util.HashSet;
import java.util.Set;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 22.03.2015 23:16.
 */

public abstract class GameElementBase implements GameElement {

    private Set<PlayerOnlineInfo> subscribers = new HashSet<>();

    public GameElementBase() {
    }

    @Override
    public void sendMessageToSubscribers(Object msg) {
        for (PlayerOnlineInfo player : subscribers) {
            player.send(msg);
        }
    }

    @Override
    public void addSubscriber(PlayerOnlineInfo info) {
        subscribers.add(info);
    }

    @Override
    public void removeSubscriber(PlayerOnlineInfo info) {
        subscribers.remove(info);
    }

    @Override
    public void removeAllSubscribers() {
        subscribers.clear();
    }
}
