package com.softserveinc.ita.kaiji.multiplay.dto.client;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 31.03.2015 19:37.
 */
public class DtoClient {

    private Types type;


    /**
     * values="0", "1"... - ids of tables, if table type
     * values="name" - name of client, if player type
     */
    private String name;


    /**
     * values="ROCK", "PAPER", "SCISSORS" , if table type
     * values="challenge.add", "challenge.remove", if player type
     */
    private String action;

    public DtoClient() {
    }

    public Types getType() {
        return type;
    }

    public void setType(Types type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public static enum Types {
        TABLE, PLAYER
    }
}
