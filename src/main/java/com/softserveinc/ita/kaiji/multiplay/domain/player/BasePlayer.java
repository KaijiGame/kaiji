package com.softserveinc.ita.kaiji.multiplay.domain.player;


import com.softserveinc.ita.kaiji.multiplay.domain.GameElementWithPublisherOwnerBase;
import com.softserveinc.ita.kaiji.multiplay.domain.message.InOutMessage;
import com.softserveinc.ita.kaiji.multiplay.domain.message.MessageProcessable;
import com.softserveinc.ita.kaiji.multiplay.domain.room.PlayRoom;
import com.softserveinc.ita.kaiji.multiplay.dto.server.player.DtoBasePlayer;
import com.softserveinc.ita.kaiji.multiplay.dto.server.player.DtoBasePlayerPublic;

/**
 * Project testFromEmpty
 * Created by mikeldpl
 * 21.03.2015 23:37.
 */

public abstract class BasePlayer extends GameElementWithPublisherOwnerBase implements MessageProcessable {

    private final String name;
    private final PlayRoom room;
    private StateOfPlayer state = StateOfPlayer.ONLINE;

    public BasePlayer(String name, PlayRoom room) {
        this.name = name;
        this.room = room;
    }

    public StateOfPlayer getState() {
        return state;
    }

    public void setState(InOutMessage msg, StateOfPlayer state) {
        msg.addDomainThatMustBeSanded(getPublisher());
        this.state = state;

    }


    public String getName() {
        return name;
    }

    public PlayRoom getRoom() {
        return room;
    }

    @Override
    public DtoBasePlayer getDto() {
        return new DtoBasePlayer(getName());
    }

    @Override
    public DtoBasePlayer getEntireDto() {
        return getDto().setName(getName());
    }

    @Override
    public PlayRoom getPublisher() {
        return getRoom();
    }

    @Override
    public DtoBasePlayerPublic getPublicDto() {
        return new DtoBasePlayerPublic(getName());
    }

    @Override
    public DtoBasePlayerPublic getEntirePublicDto() {
        return getPublicDto().setState(getState());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        return getClass().isInstance(o) &&
                getName().equals(((BasePlayer) o).getName());
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }
}
