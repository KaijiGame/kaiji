package com.softserveinc.ita.kaiji.multiplay.dto.server;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 31.03.2015 23:16.
 */
public class DtoMessage extends DtoBase {
    private String msg;

    public DtoMessage() {
        setType("MESSAGE");
    }

    public DtoMessage(String message) {
        this();
        this.msg = message;
    }

    public String getMsg() {
        return msg;
    }

    public DtoMessage setMsg(String msg) {
        this.msg = msg;
        return this;
    }
}
