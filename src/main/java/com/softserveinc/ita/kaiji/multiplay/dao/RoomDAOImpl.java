package com.softserveinc.ita.kaiji.multiplay.dao;

import com.softserveinc.ita.kaiji.multiplay.domain.room.Ship;
import org.springframework.stereotype.Repository;

/**
 * Project testFromEmpty
 * Created by mikeldpl
 * 22.03.2015 0:04.
 */
@Repository
public class RoomDAOImpl extends InMemoryDAO<Ship, String> implements RoomDAO {
    public RoomDAOImpl() {
        super(Ship.class);
    }
}
