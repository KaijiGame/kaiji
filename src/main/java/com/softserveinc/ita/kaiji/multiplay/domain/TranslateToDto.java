package com.softserveinc.ita.kaiji.multiplay.domain;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 30.03.2015 21:45.
 */
public interface TranslateToDto {
    Object getDto();

    Object getEntireDto();
}
