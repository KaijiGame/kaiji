package com.softserveinc.ita.kaiji.multiplay.service;

import com.softserveinc.ita.kaiji.multiplay.domain.player.PlayerOnlineInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 22.03.2015 19:00.
 */
@Service
public class SendMessageByWebSocketService {
    @Autowired
    public SimpMessagingTemplate template;


    public void sendMessageToPlayerOnShip(PlayerOnlineInfo playerOnlineInfo, Object msg) {
        template.convertAndSendToUser(playerOnlineInfo.getName(),
                "/queue/game/" + playerOnlineInfo.getPlayRoom().getName(), msg);
    }

}
