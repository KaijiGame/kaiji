package com.softserveinc.ita.kaiji.multiplay.domain.room;

import com.softserveinc.ita.kaiji.multiplay.domain.player.BasePlayer;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 24.03.2015 23:29.
 */
public class Players {
    private final int maxNumberOfPlayersConfig;
    private final Set<BasePlayer> players = new LinkedHashSet<>();

    public Players(int maxNumberOfPlayersConfig) {
        this.maxNumberOfPlayersConfig = maxNumberOfPlayersConfig;
    }

    public Set<BasePlayer> getPlayers() {
        return players;
    }

    public int getMaxNumberOfPlayersConfig() {
        return maxNumberOfPlayersConfig;
    }

    public boolean addPlayer(BasePlayer player) {
        if (players.contains(player))
            return true;
        if (players.size() < maxNumberOfPlayersConfig) {
            players.add(player);
            return true;
        }
        return false;
    }

    public boolean removePlayer(BasePlayer player) {
        return players.remove(player);
    }

    public BasePlayer getPlayerByName(String name) {
        for (BasePlayer player : players) {
            if (player.getName().equals(name)) {
                return player;
            }
        }
        return null;
    }

    public boolean isFull() {
        return players.size() >= maxNumberOfPlayersConfig;
    }

}