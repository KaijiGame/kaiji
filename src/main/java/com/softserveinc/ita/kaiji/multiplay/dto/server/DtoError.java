package com.softserveinc.ita.kaiji.multiplay.dto.server;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 28.03.2015 14:27.
 */
public class DtoError extends DtoBase {
    private String err;

    public DtoError() {
        setType("sys.error");
    }

    public String getErr() {
        return err;
    }

    public DtoError setErr(String err) {
        this.err = err;
        return this;
    }
}
