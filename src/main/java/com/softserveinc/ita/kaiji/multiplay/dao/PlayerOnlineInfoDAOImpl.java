package com.softserveinc.ita.kaiji.multiplay.dao;


import com.softserveinc.ita.kaiji.multiplay.domain.player.PlayerOnlineInfo;
import org.springframework.stereotype.Repository;

/**
 * Project testFromEmpty
 * Created by mikeldpl
 * 22.03.2015 0:01.
 */
@Repository
public class PlayerOnlineInfoDAOImpl
        extends InMemoryDAO<PlayerOnlineInfo, String> implements PlayerOnlineInfoDAO {
    public PlayerOnlineInfoDAOImpl() {
        super(PlayerOnlineInfo.class);
    }
}
