package com.softserveinc.ita.kaiji.multiplay.domain.message;

import com.softserveinc.ita.kaiji.multiplay.domain.GameElement;
import com.softserveinc.ita.kaiji.multiplay.domain.player.BasePlayer;
import com.softserveinc.ita.kaiji.multiplay.dto.client.DtoClient;
import org.apache.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 25.03.2015 0:15.
 */
public class InOutMessage {
    private static final Logger LOG = Logger.getLogger(InOutMessage.class);

    public static boolean sendEntireDto = true;
    private final BasePlayer player;
    private final DtoClient in;
    private final Map<GameElement, Object> out = new LinkedHashMap<>();

    public InOutMessage(BasePlayer player, DtoClient in) {
        this.player = player;
        this.in = in;
    }

    public BasePlayer getPlayer() {
        return player;
    }

    public DtoClient getIn() {
        return in;
    }

    public Map<GameElement, Object> getOut() {
        return out;
    }

//	public Object getDtoByDomain(GameElement domain) {
//		Object res;
//		if (!out.containsKey(domain)) {
//			out.put(domain, res = sendEntireDto ? domain.getEntireDto() : domain.getDto());
//		} else {
//			res = out.get(domain);
//		}
//		return res;
//	}

    public void addDomainThatMustBeSanded(GameElement domain) {
        out.put(domain, null);
    }

    public void setDtoByDomain(GameElement domain, Object dto) {
        out.put(domain, dto);
    }

    public void sendResponse() {
        for (Map.Entry<GameElement, Object> elem : out.entrySet()) {
            if (LOG.isInfoEnabled()) {
                LOG.info("SEND   " + elem.getKey());
            }
            elem.getKey().sendMessageToSubscribers(elem.getKey().getEntireDto());
        }
        out.clear();
    }
}
