package com.softserveinc.ita.kaiji.multiplay.domain;

import com.softserveinc.ita.kaiji.multiplay.domain.player.PlayerOnlineInfo;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 28.03.2015 15:12.
 */
public abstract class GameElementWithPublisherOwnerBase implements GameElementWithPublisherOwner {

    private PlayerOnlineInfo info;

    @Override
    public void sendMessageToSubscribers(Object msg) {
        info.send(msg);
    }

    @Override
    public void addSubscriber(PlayerOnlineInfo info) {
        this.info = info;
    }

    public PlayerOnlineInfo getInfo() {
        return info;
    }

    @Override
    public void removeSubscriber(PlayerOnlineInfo info) {
        if (info.equals(this.info)) {
            this.info = null;
        }
    }

    @Override
    public void removeAllSubscribers() {
        this.info = null;
    }
}
