package com.softserveinc.ita.kaiji.multiplay.domain.room;

import com.softserveinc.ita.kaiji.multiplay.domain.message.InOutMessage;
import com.softserveinc.ita.kaiji.multiplay.domain.message.MessageProcessable;
import com.softserveinc.ita.kaiji.multiplay.domain.player.PlayerOnlineInfo;
import com.softserveinc.ita.kaiji.multiplay.dto.client.DtoClient;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 30.03.2015 3:32.
 */
public interface Room extends MessageProcessable {
    boolean addPlayer(PlayerOnlineInfo info);

    boolean removePlayer(PlayerOnlineInfo info);

    void inComeMessage(PlayerOnlineInfo info, DtoClient msg);

    void start(InOutMessage msg);

    void stop();
}
