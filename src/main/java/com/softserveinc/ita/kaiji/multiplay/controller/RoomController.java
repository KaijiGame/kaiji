package com.softserveinc.ita.kaiji.multiplay.controller;

import com.softserveinc.ita.kaiji.multiplay.dao.RoomDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 01.04.2015 22:22.
 */
@Controller
@RequestMapping(value = "/game/room")
public class RoomController {

    @Autowired
    private RoomDAO roomDAO;

    @RequestMapping(value = "{shipName}", method = RequestMethod.GET)
    public String getRoomPage(@PathVariable("shipName") String shipName, Model model, Principal principal) {
        //ships info
        model.addAttribute("shipName", shipName);
        model.addAttribute("numberOfTables", roomDAO.findOne(shipName).getTables().getTables().length);
        return "room";
    }

}
