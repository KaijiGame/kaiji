package com.softserveinc.ita.kaiji.multiplay.dto.server.player;

import com.softserveinc.ita.kaiji.multiplay.domain.player.StateOfPlayer;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 27.03.2015 2:20.
 */
public class DtoBasePlayerPublic {
    private String name;
    private StateOfPlayer state;
    private boolean delet = false;

    public DtoBasePlayerPublic(String name) {
        this.name = name;
    }

    public boolean isDelet() {
        return delet;
    }

    public DtoBasePlayerPublic setDelet(boolean delet) {
        this.delet = delet;
        return this;
    }

    public String getName() {
        return name;
    }

    public DtoBasePlayerPublic setName(String name) {
        this.name = name;
        return this;
    }

    public StateOfPlayer getState() {
        return state;
    }

    public DtoBasePlayerPublic setState(StateOfPlayer state) {
        this.state = state;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DtoBasePlayerPublic that = (DtoBasePlayerPublic) o;

        if (!name.equals(that.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
