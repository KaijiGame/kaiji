package com.softserveinc.ita.kaiji.multiplay.domain;

import com.softserveinc.ita.kaiji.multiplay.KaijiException;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 25.03.2015 4:23.
 */
public class InvalidDomainStateException extends KaijiException {
    public InvalidDomainStateException() {
    }

    public InvalidDomainStateException(String message) {
        super(message);
    }

    public InvalidDomainStateException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidDomainStateException(Throwable cause) {
        super(cause);
    }
}
