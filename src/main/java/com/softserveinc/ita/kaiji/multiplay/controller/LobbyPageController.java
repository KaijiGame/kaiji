package com.softserveinc.ita.kaiji.multiplay.controller;

import com.softserveinc.ita.kaiji.multiplay.dao.RoomDAO;
import com.softserveinc.ita.kaiji.multiplay.domain.config.ShipConfigForGame;
import com.softserveinc.ita.kaiji.multiplay.domain.room.Ship;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

/**
 * @author Nazarenko Alexandra
 * @version 2.0
 * @since 1.04.2014
 */
@Controller
@RequestMapping(value = "/game/lobby-page")
public class LobbyPageController {


    private RoomDAO roomDAO;


    //creating of one ship in the future we must do it in form.
    private String nameOfShip = "Espoir";

    @Autowired
    public LobbyPageController(RoomDAO room) {
        this.roomDAO = room;
        ShipConfigForGame conf = new ShipConfigForGame();
        conf.setName(nameOfShip);
        roomDAO.save(new Ship(conf));
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getLobbyPage(Model model, Principal principal) {
        //ships info
        model.addAttribute("ship", roomDAO.findOne(nameOfShip));
        return "lobby-page";
    }
}
