package com.softserveinc.ita.kaiji.multiplay.domain;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 28.03.2015 16:48.
 */
public interface GameElementWithPublisherOwner extends GameElement {

    GameElement getPublisher();

    Object getPublicDto();

    Object getEntirePublicDto();
}
