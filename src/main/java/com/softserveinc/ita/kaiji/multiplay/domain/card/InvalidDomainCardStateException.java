package com.softserveinc.ita.kaiji.multiplay.domain.card;

import com.softserveinc.ita.kaiji.multiplay.domain.InvalidDomainStateException;

/**
 * Project kaiji-game
 * Created by mikeldpl
 * 28.03.2015 17:41.
 */
public class InvalidDomainCardStateException extends InvalidDomainStateException {
    public InvalidDomainCardStateException() {
    }

    public InvalidDomainCardStateException(String message) {
        super(message);
    }

    public InvalidDomainCardStateException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidDomainCardStateException(Throwable cause) {
        super(cause);
    }
}
