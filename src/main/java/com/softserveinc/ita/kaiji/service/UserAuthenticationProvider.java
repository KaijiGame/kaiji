package com.softserveinc.ita.kaiji.service;

import com.softserveinc.ita.kaiji.model.User;
import com.softserveinc.ita.kaiji.model.UserStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class UserAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private UserService userService;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    /**
     * Performs authentication
     * @param authentication - the authentication request object.
     * @return a fully authenticated object including credentials, cannot be null
     */
    @Override
    public Authentication authenticate(final Authentication authentication) {
        final String username = authentication.getName();
        final String password = (String) authentication.getCredentials();

        if (username == null || username.trim().isEmpty()) {
            throw new BadCredentialsException("Email cannot be empty");
        }

        if (password == null || password.trim().isEmpty()) {
            throw new BadCredentialsException("Password cannot be empty");
        }

        final User user = userService.findUserByNickname(username);

        if (user == null) {
            throw new BadCredentialsException("Username not found.");
        }

        if (user.getUserStatus() != UserStatus.ACTIVE) {
            throw new BadCredentialsException("Activate your profile");
        }

        if (!passwordEncoder.matches(password, user.getPassword())) {
            throw new BadCredentialsException("Wrong password.");
        }

        final Collection<? extends GrantedAuthority> authorities
                = user.getAuthorities();

        return new UsernamePasswordAuthenticationToken(user, password,
                authorities);
    }

    /**
     * Check if AuthenticationProvider supports the indicated Authentication object.
     * @param aClass - the authentication object.
     * @return true if supporting
     */
    @Override
    public boolean supports(final Class<?> aClass) {
        return true;
    }
}