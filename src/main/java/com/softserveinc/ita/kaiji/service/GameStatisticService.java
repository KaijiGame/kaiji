package com.softserveinc.ita.kaiji.service;

import com.softserveinc.ita.kaiji.dao.GameStatisticDao;
import com.softserveinc.ita.kaiji.model.GameStatistic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Artur Yakubenko
 * on 27.08.2015.
 */

@Service
public class GameStatisticService {
    @Autowired
    private GameStatisticDao statisticsRepository;
    public GameStatistic add (GameStatistic stat) {
        return statisticsRepository.save(stat);
    }

    public void delete(Integer id) {
        statisticsRepository.delete(id);
    }

    public GameStatistic getByName(String name) {
        return statisticsRepository.findByName(name);
    }

    public GameStatistic edit(GameStatistic stat) {
        return statisticsRepository.saveAndFlush(stat);
    }

    public List<GameStatistic> getAll() {
        return statisticsRepository.findAll();
    }
}
