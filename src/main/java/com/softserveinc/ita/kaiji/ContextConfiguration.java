package com.softserveinc.ita.kaiji;

import com.softserveinc.ita.kaiji.dao.GameStatisticDaoJdbc;
import com.softserveinc.ita.kaiji.newChat.WebSocketConfigForChat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesView;

import java.util.Locale;
import java.util.Properties;

/**
 * Root configuration
 *
 * @author Sydorenko Oleksandra
 * @version 1.0
 * @since 20.08.14
 */

@Configuration
@Import({DBConfiguration.class, SecurityConfiguration.class
        , WebSocketConfiguration.class, WebSocketConfigForChat.class, SocialContext.class })
@PropertySource("classpath:email.properties")
@ComponentScan({"com.softserveinc.ita.kaiji.rest", "com.softserveinc.ita.kaiji.web",
        "com.softserveinc.ita.kaiji.service", "com.softserveinc.ita.kaiji.model", "com.softserveinc.ita.kaiji.dto",
        "com.softserveinc.ita.kaiji.ajax", "com.softserveinc.ita.kaiji.sse",
        "com.softserveinc.ita.kaiji.multiplay.dao",
        "com.softserveinc.ita.kaiji.multiplay.domain",
        "com.softserveinc.ita.kaiji.multiplay.dto",
        "com.softserveinc.ita.kaiji.multiplay.service",
        "com.softserveinc.ita.kaiji.newChat"})
public class ContextConfiguration {

    @Autowired
    Environment env;

    @Bean
    public LocalValidatorFactoryBean validatorFactoryBean() {
        LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();
        validator.setValidationMessageSource(messageSourceBean());
        return validator;
    }

    @Bean(name = "messageSource")
    public ReloadableResourceBundleMessageSource messageSourceBean() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames("classpath:i18n/common", "classpath:i18n/errors", "classpath:i18n/pages/profile",
                "classpath:i18n/pages/registration-form", "classpath:i18n/pages/start-page", "classpath:i18n/pages/header", "classpath:i18n/pages/create-game",
                "classpath:i18n/pages/system-configuration", "classpath:i18n/pages/play-game", "classpath:i18n/pages/join-game",
                "classpath:i18n/pages/statistics", "classpath:i18n/pages/login", "classpath:i18n/pages/footer",
                "classpath:i18n/pages/admin-page", "classpath:i18n/pages/manual-page", "classpath:i18n/pages/FAQ",
                "classpath:i18n/pages/manual_statistics", "classpath:i18n/pages/manual_cabinet",
                "classpath:i18n/pages/manual_about_lobby", "classpath:i18n/pages/manual_gameroom", "classpath:i18n/pages/gameStatistic");
        messageSource.setFallbackToSystemLocale(false);
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Bean(name = "localeResolver")
    public CookieLocaleResolver localeResolverBean() {
        CookieLocaleResolver resolver = new CookieLocaleResolver();
        resolver.setDefaultLocale(new Locale("en"));
        return resolver;
    }

    @Bean
    public Validator mvcValidator() {
        return validatorFactoryBean();
    }


    @Bean
    public TilesConfigurer tilesConfigurer() {
        TilesConfigurer tilesConfigurer = new TilesConfigurer();
        tilesConfigurer.setDefinitions("/WEB-INF/defs/definitions.xml");
        return tilesConfigurer;
    }

    @Bean
    public UrlBasedViewResolver tilesViewResolver() {
        UrlBasedViewResolver viewResolver = new UrlBasedViewResolver();
        viewResolver.setViewClass(TilesView.class);
        viewResolver.setOrder(0);
        return viewResolver;
    }

    @Bean
    public JavaMailSenderImpl javaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(env.getProperty("mail.smtp.host"));
        mailSender.setPort(Integer.valueOf(env.getProperty("mail.smtp.port")));
        mailSender.setUsername(env.getProperty("email.address"));
        mailSender.setPassword(env.getProperty("email.password"));
        mailSender.setJavaMailProperties(mailProperties());
        return mailSender;
    }

    @Bean
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver =
                new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(20971520); //20MB
        multipartResolver.setMaxInMemorySize(1048576); //1MB
        return multipartResolver;
    }

    private Properties mailProperties() {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");
        return properties;
    }

    /* Artur Yakubenko 05.09.15 */
    @Bean
    public GameStatisticDaoJdbc gameStatisticDaoJdbc() {
        return new GameStatisticDaoJdbc();
    }
    /* Artur Yakubenko 05.09.15 */
}
