package com.softserveinc.ita.kaiji.rest.dto;

import com.softserveinc.ita.kaiji.model.RoleType;

import java.util.Set;

/**
 * @author Konstantin Shevchuk
 * @version 1.3
 * @since 15.08.14.
 */

public class UserRestDto {

    private Integer id;
    private String name;
    private String nickname;
    private String email;
    private Set<RoleType> roles;
    private String registrationDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<RoleType> getRoles() {
        return roles;
    }

    public void setRoles(Set<RoleType> roles) {
        this.roles = roles;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }
}
